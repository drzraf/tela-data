--
-- Structure de la table `liste_rouge_meta`
--
CREATE TABLE IF NOT EXISTS `liste_rouge_meta` (
  `guid` varchar(255) NOT NULL DEFAULT 'urn:lsid:tela-botanica.org:#projet:#version',
  `langue_meta` varchar(2) NOT NULL DEFAULT 'fr',
  `code` varchar(20) NOT NULL,
  `version` varchar(20) NOT NULL,
  `titre` varchar(255) DEFAULT NULL,
  `description` text,
  `mots_cles` varchar(510) DEFAULT NULL,
  `citation` varchar(500) DEFAULT NULL,
  `url_tech` varchar(510) DEFAULT NULL,
  `url_projet` varchar(510) DEFAULT NULL,
  `source` text,
  `createurs` text,
  `editeur` varchar(1000) NOT NULL DEFAULT 'nom=Tela Botanica,guid=urn:lsid:tela-botanica.org,courriel=accueil@tela-botanica.org,telephone=+334 67 52 41 22,url.info=http://www.tela-botanica.org/page:association_tela_botanica,url.logo=http://www.tela-botanica.org/sites/accueil/generique/images/graphisme/logo_tela_fond_blanc.png,type=organisation,acronyme=TB,adresse=Association Tela Botanica - Institut de Botanique - 167 rue Auguste Broussonnet 34090 MONTPELLIER FRANCE,latitude.wgs84=43.615892,longitude.wgs84=3.871736,contact.prenom=Jean-Pascal,contact.nom=MILCENT,contact.courriel=jpm@tela-botanica.org,contact.role=administrateur des données;',
  `contributeurs` text,
  `droits` text,
  `url_droits` varchar(510) DEFAULT NULL,
  `langue` varchar(255) NOT NULL DEFAULT 'fr',
  `date_creation` varchar(30) NOT NULL DEFAULT 'YYYY-MM-DD',
  `date_validite` varchar(255) DEFAULT NULL,
  `couverture_spatiale` varchar(510) NOT NULL DEFAULT 'iso-3166-1.id=FX;',
  `couverture_temporelle` varchar(510) DEFAULT NULL,
  `web_services` varchar(510) DEFAULT NULL COMMENT 'ontologies,meta-donnees,noms,taxons,aide,textes,images,observations,noms-vernaculaires',
  PRIMARY KEY (`guid`,`langue_meta`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `liste_rouge_meta`
--

INSERT INTO `liste_rouge_meta` (`guid`, `langue_meta`, `code`, `version`, 
`titre`, `description`, 
`mots_cles`, `citation`, 
`url_tech`, `url_projet`, 
`source`, 
`createurs`, `editeur`, 
`contributeurs`, 
`droits`, `url_droits`, `langue`, `date_creation`, `date_validite`, `couverture_spatiale`, `couverture_temporelle`, `web_services`) VALUES
('urn:lsid:tela-botanica.org:liste_rouge:2012', 'fr', 'liste_rouge', '2012',
 'Liste rouge des espèces menacées en France', 'Pour cette nouvelle étape dans la réalisation de la Liste rouge des espèces menacées en France, le risque de disparition de plus d’un millier de plantes vasculaires, parmi les plus rares du territoire, a été examiné. Cet état des lieux a été mené avec l’expertise d’une vingtaine de botanistes spécialisés. Il est le fruit d’une collaboration associant le Comité français de l’UICN, la Fédération des conservatoires botaniques nationaux (FCBN) et le Muséum national d’Histoire naturelle.',
 'menacé,liste, rouge, france', 'UICN France, FCBN & MNHN (2012). La Liste rouge des espèces menacées en France - Chapitre Flore vasculaire de France métropolitaine : premiers résultats pour 1 000 espèces, sous-espèces et variétés.', 
 '', 'http://www.uicn.fr/', 
 'Bilz M., Kell S.P., Maxted N. & Lansdown R.V. (2011). European Red List of vascular plants. Publications Office of the European Union, Luxembourg.\n\r\nOlivier L., Galland J-P., Maurin H. (coord.) & Roux J-P. (réd.)\r\n(1995). Livre rouge de la flore menacée de France, Tome I : espèces prioritaires. Col. Patrimoines naturels, Volume 20.\r\n\r\nSPN/IEGB/MNHN, CBN de Porquerolles, Ministère de l''Environnement, Paris.\r\nRoux J-P. (coord.) (citation provisoire). Livre rouge de la flore\r\nmenacée de France (flore métropolitaine), Tome II : espèces à\r\nsurveiller.\r\nCBN méditerranéen de Porquerolles, FCBN,\r\nMinistère en charge de l''Ecologie.\r\nUICN France & MNHN (2009). La Liste rouge des espèces\r\nmenacées en France - Contexte, enjeux et démarche\r\nd’élaboration. Paris, France.\r\n\r\nUICN France, MNHN, FCBN & SFO (2010). La Liste rouge des\r\nespèces menacées en France - Chapitre Orchidées de France\r\nmétropolitaine. Paris, France.\r\n\r\nValentin B., Toussaint B. & Valet J.M. (2010). Bilan de la\r\nconservation des taxons du tome 1 du livre rouge de la flore\r\nmenacée de France. Centre régional de phytosociologie / CBN\r\nde Bailleul, pour la FCBN, Bailleul.\r\n',
 'o.nom=UICN', '',
 'p.prenom=Jean-Pierre,p.nom=Roux,p.fonction=Compilation des données,o.nom=CBN-MED;p.prenom=Bertille,p.nom=Valentin,p.fonction=Compilation des données,o.nom=CBN-BL;p.prenom=Nicolas,p.nom=Juillet,p.fonction=Pré-évaluations,o.nom=FCBN;p.prenom=Frédéric,p.nom=Andrieu,p.fonction=Comité d’évaluation/experts,o.nom=CBN-MED;p.prenom=Philippe,p.nom=Antonetti,p.fonction=Comité d’évaluation/experts,o.nom=CBN-MC;p.prenom=Grégory,p.nom=Caze,p.fonction=Comité d’évaluation/experts,o.nom=CBN-SA;p.prenom=Bruno,p.nom=de Foucault,p.fonction=Comité d’évaluation/experts,o.nom=SBF;p.prenom=Sébastien,p.nom=Filoche,p.fonction=Comité d’évaluation/experts,o.nom=CBN-BP;p.prenom=Luc,p.nom=Garraud,p.fonction=Comité d’évaluation/experts,o.nom=CBN-A;p.prenom=Julien,p.nom=Geslin,p.fonction=Comité d’évaluation/experts,o.nom=CBN-B;p.prenom=Lionel,p.nom=Gire,p.fonction=Comité d’évaluation/experts,o.nom=CBN-PMP;p.prenom=Michel,p.nom=Hoff,p.fonction=Comité d’évaluation/experts,o.nom=Univ. Strasbourg;p.prenom=Laetitia,p.nom=Hugot,p.fonction=Comité d’évaluation/experts,o.nom=CBN-C;p.prenom=Denis,p.nom=Jordan,p.fonction=Comité d’évaluation/experts,o.nom=indépendant;p.prenom=Jacques,p.nom=Lambinon,p.fonction=Comité d’évaluation/experts,o.nom=indépendant;p.prenom=Nicolas,p.nom=Leblond,p.fonction=Comité d’évaluation/experts,o.nom=CBN-PMP;p.prenom=Joël,p.nom=Mathez,p.fonction=Comité d’évaluation/experts,o.nom=Univ. Montpellier;p.prenom=Frédéric,p.nom=Médail,p.fonction=Comité d’évaluation/experts,o.nom=IMBE Univ. Aix-Marseille;p.prenom=Henri,p.nom=Michaud,p.fonction=Comité d’évaluation/experts,o.nom=CBN-MED;p.prenom=James,p.nom=Molina,p.fonction=Comité d’évaluation/experts,o.nom=CBN-MED;p.prenom=Serge,p.nom=Muller,p.fonction=Comité d’évaluation/experts,o.nom=Univ. Metz;p.prenom=Jean-Marc,p.nom=Tison,p.fonction=Comité d’évaluation/experts,o.nom=SBF;p.prenom=Benoît,p.nom=Toussaint,p.fonction=Comité d’évaluation/experts,o.nom=CBN-BL;p.prenom=Jérémie,p.nom=Van Es,p.fonction=Comité d’évaluation/experts,o.nom=CBN-A;p.prenom=Mathias,p.nom=Voirin,p.fonction=Comité d’évaluation/experts,o.nom=Esope;p.prenom=Guillaume,p.nom=Gigot,p.fonction=Comité d’évaluation/évaluateurs Liste rouge,o.nom=FCBN;p.prenom=Patrick,p.nom=Haffner,p.fonction=Comité d’évaluation/évaluateurs Liste rouge,o.nom=MNHN;p.prenom=Florian,p.nom=Kirchner,p.fonction=Comité d’évaluation/évaluateurs Liste rouge,o.nom=UICN France;p.prenom=Sophie,p.nom=Auvert,p.fonction=Contributeurs,o.nom=CBN-BP;p.prenom=Alain,p.nom=Delage,p.fonction=Contributeurs,o.nom=CBN-C;p.prenom=Yorick,p.nom=Ferrez,p.fonction=Contributeurs,o.nom=CBN-FC-ORI;p.prenom=Jacques,p.nom=Gamisans,p.fonction=Contributeurs,o.nom=indépendant;p.prenom=Georges,p.nom=Guende,p.fonction=Contributeurs,o.nom=PNR du Lubéron;p.prenom=Pascal,p.nom=Holveck,p.fonction=Contributeurs,o.nom=ONF;p.prenom=Sylvie,p.nom=Magnanon,p.fonction=Contributeurs,o.nom=CBN-B;p.prenom=Virgile,p.nom=Noble,p.fonction=Contributeurs,o.nom=CBN-MED;p.prenom=Arne,p.nom=Saatkamp,p.fonction=Contributeurs,o.nom=IMBE Univ. Aix-Marseille;p.prenom=AuroreCavrois,p.nom=,p.fonction=Elaboration des documents,o.nom=UICN France;p.prenom=Johan,p.nom=Gourvil,p.fonction=Elaboration des documents,o.nom=FCBN;',
  NULL, NULL, 'fr', '2012-01-01', NULL, '', NULL, 'protections:0.1;meta-donnees:0.1');
 
  


--
-- Structure de la table `liste_rouge_v2012`
--

CREATE TABLE IF NOT EXISTS `liste_rouge_v2012` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `nom_sci_orig` varchar(200) NOT NULL,
  `num_categorie` varchar(2) NOT NULL,
  `categorie` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


