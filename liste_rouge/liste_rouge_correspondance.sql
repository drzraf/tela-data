UPDATE liste_rouge_v2012 SET num_nom_retenu = 84927, nom_sci = "Astragalus genargenteus Moris" WHERE id = 213;
UPDATE liste_rouge_v2012 SET num_nom_retenu = 75188, nom_sci = "Iberis spathulata J.P.Bergeret ex DC." WHERE id = 270;
UPDATE liste_rouge_v2012 SET num_nom_retenu = 16200, nom_sci = "Cerinthe glabra subsp. pyrenaica (Arv.-Touv.) Kerguélen" WHERE id = 356;
UPDATE liste_rouge_v2012 SET num_nom_retenu = 55848, nom_sci = "Rhaponticum heleniifolium Godr. & Gren. subsp. heleniifolium" WHERE id = 368;
UPDATE liste_rouge_v2012 SET num_nom_retenu = 30796, nom_sci = "Lappula deflexa (Wahlenb.) Garcke" WHERE id = 448;
UPDATE liste_rouge_v2012 SET num_nom_retenu = 638, nom_sci = "Aconitum variegatum subsp. pyrenaicum Vivant & Delay" WHERE id = 491;
UPDATE liste_rouge_v2012 SET num_nom_retenu = 55352, nom_sci = "Ranunculus revelieri Boreau var. revelieri" WHERE id = 507;
UPDATE liste_rouge_v2012 SET num_nom_retenu = 20961, nom_sci = "Cytisus ardoinoi E.Fourn." WHERE id = 632;
UPDATE liste_rouge_v2012 SET num_nom_retenu = 28640, nom_sci = "Gagea luberonensis J.M.Tison" WHERE id = 669;
UPDATE liste_rouge_v2012 SET num_nom_retenu = 8189, nom_sci = "Astragalus vesicarius subsp. pastellianus (Pollini) Arcang." WHERE id = 899;
