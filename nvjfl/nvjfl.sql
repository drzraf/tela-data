--
-- Structure de la table `nvjfl_meta`
--
CREATE TABLE IF NOT EXISTS `nvjfl_meta` (
  `guid` varchar(255) NOT NULL DEFAULT '',
  `langue_meta` varchar(2) NOT NULL DEFAULT '',
  `code` varchar(20) DEFAULT NULL,
  `version` varchar(20) DEFAULT NULL,
  `titre` varchar(255) DEFAULT NULL,
  `description` text,
  `mots_cles` varchar(510) DEFAULT NULL,
  `citation` varchar(255) DEFAULT NULL,
  `url_tech` varchar(510) DEFAULT NULL,
  `url_projet` varchar(510) DEFAULT NULL,
  `source` text,
  `createurs` text,
  `editeur` text,
  `contributeurs` text,
  `droits` text,
  `url_droits` varchar(510) DEFAULT NULL,
  `langue` varchar(255) DEFAULT NULL,
  `date_creation` varchar(30) DEFAULT NULL,
  `date_validite` varchar(255) DEFAULT NULL,
  `couverture_spatiale` varchar(510) DEFAULT NULL,
  `couverture_temporelle` varchar(510) DEFAULT NULL,
  `web_services` varchar(255) NOT NULL,
  PRIMARY KEY (`guid`,`langue_meta`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `nvjfl_meta`
--
INSERT INTO `nvjfl_meta` (`guid`, `langue_meta`, `code`, `version`, `titre`, `description`, `mots_cles`, `citation`, `url_tech`, `url_projet`, `source`, `createurs`, `editeur`, `contributeurs`, `droits`, `url_droits`, `langue`, `date_creation`, `date_validite`, `couverture_spatiale`, `couverture_temporelle`, `web_services`) VALUES
('urn:lsid:tela-botanica.org:nvjls:2007', 'fr', 'nvjls', '2007', 'Noms vernaculaires des taxons de la BDNFF par Jean-François LÉGER', '', 'eFlore, ontologies, flore, france', NULL, '', '', '', 'p.nom=LEGER,p.prenom=Jean-François', 'nom=Tela Botanica,guid=urn:lsid:tela-botanica.org,courriel=accueil@tela-botanica.org,telephone=+334 67 52 41 22,url.info=http://www.tela-botanica.org/page:association_tela_botanica,url.logo=http://www.tela-botanica.org/sites/accueil/generique/images/graphisme/logo_tela_fond_blanc.png,type=organisation,acronyme=TB,adresse=Association Tela Botanica - Institut de Botanique - 167 rue Auguste Broussonnet 34090 MONTPELLIER FRANCE,latitude.wgs84=43.615892,longitude.wgs84=3.871736,contact.prenom=Jean-Pascal,contact.nom=MILCENT,contact.courriel=jpm@tela-botanica.org,contact.role=administrateur des données', NULL, NULL, NULL, 'fr', '2007-10-29', NULL, NULL, NULL, 'noms-vernaculaires:0.1;Meta-donnees:0.1');

--
-- Structure de la table `nvjfl_biblio_v2007`
--
CREATE TABLE IF NOT EXISTS `nvjfl_biblio_v2007` (
  `num_ref` int(15) NOT NULL,
  `titre` text NOT NULL,
  `auteurs` text NOT NULL,
  `debut_parution` varchar(4) NOT NULL,
  `fin_parution` varchar(4) NOT NULL,
  `num_type_source` int(3) NOT NULL,
  `editions_ou_periodique` text NOT NULL,
  `collection_ou_nom_numero` varchar(50) NOT NULL,
  `num_periodique` varchar(5) NOT NULL,
  `page_debut` int(3) NOT NULL,
  `page_fin` int(5) NOT NULL,
  PRIMARY KEY (`num_ref`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Structure de la table `nvjfl_lien_biblio_v2007`
--
CREATE TABLE IF NOT EXISTS `nvjfl_lien_biblio_v2007` (
  `num_nom` int(25) NOT NULL,
  `num_ref` int(25) NOT NULL,
  `notes` text NOT NULL,
  PRIMARY KEY (`num_nom`,`num_ref`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Structure de la table `nvjfl_ontologies_v2007`
--
CREATE TABLE IF NOT EXISTS `nvjfl_ontologies_v2007` (
  `id` int(1) NOT NULL DEFAULT '0',
  `classe_id` int(1) DEFAULT NULL,
  `nom` varchar(26) DEFAULT NULL,
  `description` text,
  `code` varchar(100) DEFAULT NULL,
  `complements` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `nom` (`nom`),
  KEY `code` (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Structure de la table `nvjfl_v2007`
--
CREATE TABLE IF NOT EXISTS `nvjfl_v2007` (
  `id` int(11) NOT NULL,
  `num_nom_vernaculaire` int(25) NOT NULL,
  `num_taxon` int(10) NOT NULL,
  `nom_vernaculaire` text NOT NULL,
  `code_langue` varchar(3) NOT NULL,
  `num_genre` text NOT NULL,
  `num_statut` text NOT NULL,
  `zone_usage` text NOT NULL,
  `notes` text NOT NULL,
  PRIMARY KEY (`id`,`num_taxon`,`code_langue`),
  KEY `num_taxon` (`num_taxon`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
