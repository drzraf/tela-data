 CREATE  TABLE  `tb_eflore`.`sptba_meta` (  `guid` varchar( 255  )  NOT  NULL DEFAULT  '',
 `langue_meta` varchar( 2  )  NOT  NULL DEFAULT  '',
 `code` varchar( 20  )  DEFAULT NULL ,
 `version` varchar( 20  )  DEFAULT NULL ,
 `titre` varchar( 255  )  DEFAULT NULL ,
 `description` text,
 `mots_cles` varchar( 510  )  DEFAULT NULL ,
 `citation` varchar( 255  )  DEFAULT NULL ,
 `url_tech` varchar( 510  )  DEFAULT NULL ,
 `url_projet` varchar( 510  )  DEFAULT NULL ,
 `source` text,
 `createurs` text,
 `editeur` text,
 `contributeurs` text,
 `droits` text,
 `url_droits` varchar( 510  )  DEFAULT NULL ,
 `langue` varchar( 255  )  DEFAULT NULL ,
 `date_creation` varchar( 30  )  DEFAULT NULL ,
 `date_validite` varchar( 255  )  DEFAULT NULL ,
 `couverture_spatiale` varchar( 510  )  DEFAULT NULL ,
 `couverture_temporelle` varchar( 510  )  DEFAULT NULL ,
 `web_services` varchar( 255  )  NOT  NULL ,
 PRIMARY  KEY (  `guid` ,  `langue_meta`  )  ) ENGINE  =  MyISAM  DEFAULT CHARSET  = utf8;
 
 INSERT INTO `sptba_meta` (`guid`, `langue_meta`, `code`, `version`, `titre`, `description`, `mots_cles`, `citation`, `url_tech`, `url_projet`, `source`, `createurs`, `editeur`, `contributeurs`, `droits`, `url_droits`, `langue`, `date_creation`, `date_validite`, `couverture_spatiale`, `couverture_temporelle`, `web_services`) VALUES
('urn:lsid:tela-botanica.org:sptba:2012', 'fr', 'sptba', '2012', 'Statuts de protection des Antilles synthétisés par Tela botanica', '', 'eFlore, statut, protection, france, Antilles', 'Statuts de protection des Antilles synthétisés par Tela botanica', '', '', '', 'p.prenom=Jean-Pascal,p.nom=Milcent,p.courriel=jpm@tela-botanica.org', 'nom=Tela Botanica,guid=urn:lsid:tela-botanica.org,courriel=accueil@tela-botanica.org,telephone=+334 67 52 41 22,url.info=http://www.tela-botanica.org/page:association_tela_botanica,url.logo=http://www.tela-botanica.org/sites/accueil/generique/images/graphisme/logo_tela_fond_blanc.png,type=organisation,acronyme=TB,adresse=Association Tela Botanica - Institut de Botanique - 167 rue Auguste Broussonnet 34090 MONTPELLIER FRANCE,latitude.wgs84=43.615892,longitude.wgs84=3.871736,contact.prenom=Jean-Pascal,contact.nom=MILCENT,contact.courriel=jpm@tela-botanica.org,contact.role=administrateur des données', NULL, NULL, NULL, 'fr', '2012-07-10', NULL, 'iso-3166-1.id=XA;', NULL, 'Ontologies:0.1;Meta-donnees:0.1;Statuts:0.1');


CREATE TABLE `tb_eflore`.`sptba_lois_v2012` (
`id` int( 10 ) NOT NULL ,
`texte` text NOT NULL ,
`date_mise_a_jour` varchar( 50 ) NOT NULL ,
`hyperlien_legifrance` text NOT NULL ,
`statut` text NOT NULL COMMENT 'sous section = « statut »',
`regle` text NOT NULL ,
`zone_application` text NOT NULL ,
`code_zone_application` varchar( 20 ) NOT NULL ,
PRIMARY KEY ( `id` )
) ENGINE = InnoDB DEFAULT CHARSET = utf8;

CREATE TABLE `tb_eflore`.`sptba_especes_v2012` (
`nom_complet` text NOT NULL ,
`nom_sci` text NOT NULL ,
`auteur` varchar(50) NOT NULL ,
`nom_verna` text NOT NULL ,
`num_nom` int( 10 ) NOT NULL ,
`1` int( 1 ) NOT NULL ,
`2` int( 1 ) NOT NULL, 
KEY `num_nom` ( `num_nom` )
) ENGINE = InnoDB DEFAULT CHARSET = utf8;
