--
-- Structure de la table `nvps_meta`
--

CREATE TABLE IF NOT EXISTS `nvps_meta` (
  `guid` varchar(255) NOT NULL DEFAULT '',
  `langue_meta` varchar(2) NOT NULL DEFAULT '',
  `code` varchar(20) DEFAULT NULL,
  `version` varchar(20) DEFAULT NULL,
  `titre` varchar(255) DEFAULT NULL,
  `description` text,
  `mots_cles` varchar(510) DEFAULT NULL,
  `citation` varchar(255) DEFAULT NULL,
  `url_tech` varchar(510) DEFAULT NULL,
  `url_projet` varchar(510) DEFAULT NULL,
  `source` text,
  `createurs` text,
  `editeur` text,
  `contributeurs` text,
  `droits` text,
  `url_droits` varchar(510) DEFAULT NULL,
  `langue` varchar(255) DEFAULT NULL,
  `date_creation` varchar(30) DEFAULT NULL,
  `date_validite` varchar(255) DEFAULT NULL,
  `couverture_spatiale` varchar(510) DEFAULT NULL,
  `couverture_temporelle` varchar(510) DEFAULT NULL,
  `web_services` varchar(255) NOT NULL,
  PRIMARY KEY (`guid`,`langue_meta`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `nvps_meta`
--

INSERT INTO `nvps_meta` (`guid`, `langue_meta`, `code`, `version`, `titre`, `description`, `mots_cles`, `citation`, `url_tech`, `url_projet`, `source`, `createurs`, `editeur`, `contributeurs`, `droits`, `url_droits`, `langue`, `date_creation`, `date_validite`, `couverture_spatiale`, `couverture_temporelle`, `web_services`) VALUES
('urn:lsid:tela-botanica.org:nvps:2012', 'fr', 'nvps', '2012', 'Noms vernaculaires grecs des taxons de la BDTFX par Pierre SEBA', '', 'eFlore, ontologies, flore, france', 'Noms vernaculaires grecs des taxons de la BDTFX par Pierre SEBA. version 2012.', '', '', '', 'p.nom=SEBA,p.prenom=Pierre', 'nom=Tela Botanica,guid=urn:lsid:tela-botanica.org,courriel=accueil@tela-botanica.org,telephone=+334 67 52 41 22,url.info=http://www.tela-botanica.org/page:association_tela_botanica,url.logo=http://www.tela-botanica.org/sites/accueil/generique/images/graphisme/logo_tela_fond_blanc.png,type=organisation,acronyme=TB,adresse=Association Tela Botanica - Institut de Botanique - 167 rue Auguste Broussonnet 34090 MONTPELLIER FRANCE,latitude.wgs84=43.615892,longitude.wgs84=3.871736,contact.prenom=Jean-Pascal,contact.nom=MILCENT,contact.courriel=jpm@tela-botanica.org,contact.role=administrateur des données', NULL, NULL, NULL, 'fr', '2012-10-29', NULL, NULL, NULL, 'noms-vernaculaires:0.1;Meta-donnees:0.1');

--
-- Structure de la table `nvps_v2012`
--

CREATE TABLE nvps_v2012 (
  id int( 11 ) NOT NULL AUTO_INCREMENT,
  code_langue varchar( 3 ) NOT NULL DEFAULT 'ell',
  code_referentiel varchar( 5 ) NOT NULL DEFAULT 'BDNFF',
  num_taxon int( 10 ) NOT NULL ,
  nom_vernaculaire text NOT NULL ,
  genre text NOT NULL ,
  notes text NOT NULL ,
PRIMARY KEY ( id , num_taxon , code_langue ) ,
KEY num_taxon ( num_taxon )
) ENGINE = MYISAM DEFAULT CHARSET = utf8;