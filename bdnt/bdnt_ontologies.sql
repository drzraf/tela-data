CREATE TABLE IF NOT EXISTS `bdnt_meta` (
  `guid` varchar(255) NOT NULL DEFAULT '',
  `langue_meta` varchar(2) NOT NULL DEFAULT '',
  `code` varchar(20) DEFAULT NULL,
  `version` varchar(20) DEFAULT NULL,
  `titre` varchar(255) DEFAULT NULL,
  `description` text,
  `mots_cles` varchar(510) DEFAULT NULL,
  `citation` varchar(255) DEFAULT NULL,
  `url_tech` varchar(510) DEFAULT NULL,
  `url_projet` varchar(510) DEFAULT NULL,
  `source` text,
  `createurs` text,
  `editeur` text,
  `contributeurs` text,
  `droits` text,
  `url_droits` varchar(510) DEFAULT NULL,
  `langue` varchar(255) DEFAULT NULL,
  `date_creation` varchar(30) DEFAULT NULL,
  `date_validite` varchar(255) DEFAULT NULL,
  `couverture_spatiale` varchar(510) DEFAULT NULL,
  `couverture_temporelle` varchar(510) DEFAULT NULL,
  `web_services` varchar(255) NOT NULL,
  PRIMARY KEY (`guid`,`langue_meta`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `bdnt_meta` (`guid`, `langue_meta`, `code`, `version`, `titre`, `description`, `mots_cles`, `citation`, `url_tech`, `url_projet`, `source`, `createurs`, `editeur`, `contributeurs`, `droits`, `url_droits`, `langue`, `date_creation`, `date_validite`, `couverture_spatiale`, `couverture_temporelle`, `web_services`) VALUES
('urn:lsid:tela-botanica.org:bdnt:4.40', 'fr', 'bdnt', '4.40', 'Ontologies du manuel technique du projet référentiel', 
'À partir des tableaux du manuel technique', 'référentiel, ontologies, flore, france', NULL, 'http://www.tela-botanica.org/wikini/eflore/wakka.php?wiki=IntegrationOntologies', 
'http://www.tela-botanica.org/wikini/RTaxMethodo/wakka.php?wiki=MaNuel', 
'Bisby F.A. 1994. Plant Names in Botanical Databases; Plant Taxonomic Database Standards No.3 published for the TDWG\r\nhttp://www.tdwg.org/standards/113/download/113-528-1-RV.pdf;\r\nHyam R. 2006. TDWG Taxon Name LSID Ontology; TDWG Biodiversity Information Standards; vers.0.2 Modified: 2007-05-04\r\nhttp://rs.tdwg.org/ontology/voc/TaxonName;\r\nHyam R. 2006. TDWG Taxon Rank LSID Ontology; TDWG Biodiversity Information Standards; vers. 0.2 Modified: 2007-11-19\r\nhttp://rs.tdwg.org/ontology/voc/TaxonRank;\r\nHyam R. and Kennedy, J. 2005. Taxon Concept Schema – User Guide ,TDWG\r\nhttp://www.tdwg.org/standards/;\r\nPereira R. 2007. Plant occurrence and status scheme. A Standard for Recording the Relationship between a Plant and a Place; World Conservation Monitoring Centre, An International Working Group on Taxonomic Databases Standard, TDWG\r\nhttp://www.tdwg.org/standards/106/download/106-522-1-RV.pdf\r\n', 'p.prenom=Mareva,p.nom=Noel,p.courriel=mareva@tela-botanica.org;p.prenom=Jean-Pascal,p.nom=Milcent,p.courriel=jpm@tela-botanica.org', 'nom=Tela Botanica,guid=urn:lsid:tela-botanica.org,courriel=accueil@tela-botanica.org,telephone=+334 67 52 41 22,url.info=http://www.tela-botanica.org/page:association_tela_botanica,url.logo=http://www.tela-botanica.org/sites/accueil/generique/images/graphisme/logo_tela_fond_blanc.png,type=organisation,acronyme=TB,adresse=Association Tela Botanica - Institut de Botanique - 167 rue Auguste Broussonnet 34090 MONTPELLIER FRANCE,latitude.wgs84=43.615892,longitude.wgs84=3.871736,contact.prenom=Jean-Pascal,contact.nom=MILCENT,contact.courriel=jpm@tela-botanica.org,contact.role=administrateur des données', NULL, NULL, NULL, 'fr', '2013-07-01', NULL, 'iso-3166-1.id=FX;', NULL, 'Ontologies:0.1;Meta-donnees:0.1');

CREATE TABLE IF NOT EXISTS `bdnt_ontologies_v4_40` (
  `id` int(5) NOT NULL,
  `classe_id` int(5) NOT NULL,
  `nom` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `code` varchar(100) NOT NULL,
  `complements` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
