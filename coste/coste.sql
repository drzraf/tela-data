--
-- Structure de la table `coste_meta`
--
CREATE TABLE IF NOT EXISTS `coste_meta` (
  `guid` varchar(255) NOT NULL DEFAULT 'urn:lsid:tela-botanica.org:chorodep:#version',
  `langue_meta` varchar(2) NOT NULL DEFAULT 'fr',
  `code` varchar(20) NOT NULL,
  `version` varchar(20) NOT NULL,
  `titre` varchar(255) DEFAULT NULL,
  `description` text,
  `mots_cles` varchar(510) DEFAULT NULL,
  `citation` varchar(255) DEFAULT NULL,
  `url_tech` varchar(510) DEFAULT NULL,
  `url_projet` varchar(510) DEFAULT NULL,
  `source` text,
  `createurs` text,
  `editeur` varchar(1000) NOT NULL DEFAULT 'nom=Tela Botanica;guid=urn:lsid:tela-botanica.org;courriel=accueil@tela-botanica.org;telephone=+334 67 52 41 22;url.info=http://www.tela-botanica.org/page:association_tela_botanica;url.logo=http://www.tela-botanica.org/sites/accueil/generique/images/graphisme/logo_tela_fond_blanc.png;type=organisation;acronyme=TB;adresse=Association Tela Botanica - Institut de Botanique - 167 rue Auguste Broussonnet 34090 MONTPELLIER, FRANCE;latitude.wgs84=43.615892;longitude.wgs84=3.871736;contact.prenom=Jean-Pascal;contact.nom=MILCENT;contact.courriel=jpm@tela-botanica.org;contact.role=administrateur des données;',
  `contributeurs` text,
  `droits` text,
  `url_droits` varchar(510) DEFAULT NULL,
  `langue` varchar(255) NOT NULL DEFAULT 'fr',
  `date_creation` varchar(30) NOT NULL DEFAULT 'YYYY-MM-DD',
  `date_validite` varchar(255) DEFAULT NULL,
  `couverture_spatiale` varchar(510) NOT NULL DEFAULT 'iso-3166-1.id=FX;',
  `couverture_temporelle` varchar(510) DEFAULT NULL,
  `web_services` varchar(510) DEFAULT NULL COMMENT 'ontologies,meta-donnees,noms,taxons,aide,textes,images,observations,noms-vernaculaires',
  PRIMARY KEY (`guid`,`langue_meta`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Données de la table coste_meta
--

INSERT INTO `coste_meta` (`guid`, `langue_meta`, `code`, `version`, `titre`, `description`, `mots_cles`, `citation`, `url_tech`, `url_projet`, `source`, `createurs`, `editeur`, `contributeurs`, `droits`, `url_droits`, `langue`, `date_creation`, `date_validite`, `couverture_spatiale`, `couverture_temporelle`, `web_services`) VALUES 
('urn:lsid:tela-botanica.org:coste:2.00', 'fr', 'coste', '2.00', 'Flore descriptive et illustrée de la France par L''abbé H. COSTE', 'Au 07 février 2011, les documents réunis dans le cadre de le projet de numérisation de la flore de Coste sont :\r\n - l''ensemble des images au format PNG et JPEG des 4354 taxons (Christophe BONNET, Frédéric LEGENS et Jean ZISSLER)\r\n - l''ensemble des descriptions au format HTML et RTF des 4354 taxons (Jean ZISSLER)\r\n - l''ensemble des clés (avec reprise des images et descriptions) du tome 01 au format DOC (Daniel LE QUÉRÉ).\r\n\r\nLe travail restant à réaliser est le suivant :\r\n - compléter la famille des Papilionacées du tome 01\r\n - numériser les descriptions et clés des familles et genres du tome 02\r\n - numériser les descriptions et clés des familles et genres du tome 03\r\n\r\nPour la numérisation des tome 2 et 3, l''idée est de compléter les documents manquant en utilisant ce wiki. Le tome 03 est disponible en PDF image avec OCR sur le site biodiversitylibrary.org.', 'index, flore, image, description, clé', NULL, 'http://www.tela-botanica.org/wikini/eflore/wakka.php?wiki=IntegrationCoste', 'http://www.tela-botanica.org/wikini/florecoste/', NULL, 'p.prenom=Jean-Pascal,p.nom=MILCENT,p.courriel=jpm@tela-botanica.org', 'nom=Tela Botanica,guid=urn:lsid:tela-botanica.or,courriel=accueil@tela-botanica.org,telephone=+334 67 52 41 22,url.info=http://www.tela-botanica.org/page:association_tela_botanica,url.logo=http://www.tela-botanica.org/sites/accueil/generique/images/graphisme/logo_tela_fond_blanc.png,type=organisation,acronyme=TB,adresse=Association Tela Botanica - Institut de Botanique - 167 rue Auguste Broussonnet 34090 MONTPELLIER FRANCE,latitude.wgs84=43.615892,longitude.wgs84=3.871736,contact.prenom=Jean-Pascal,contact.nom=MILCENT,contact.courriel=jpm@tela-botanica.org,contact.role=administrateur des données', NULL, NULL, NULL, 'fr', '2012-05-16', '', 'iso-3166-1.id=FX', NULL, 'metaDonnees:0.1;noms:0.1;taxons:0.1;Textes:0.1;Images:0.1');

--
-- Structure de la table `coste_v2_00`
--

CREATE TABLE IF NOT EXISTS `coste_v2_00` (
  `num_nom` int(9) NOT NULL DEFAULT '0',
  `num_nom_retenu` varchar(9) DEFAULT NULL,
  `num_tax_sup` varchar(9) DEFAULT NULL,
  `rang` int(3) DEFAULT NULL,
  `nom_sci` varchar(500) DEFAULT NULL,
  `nom_supra_generique` varchar(500) DEFAULT NULL,
  `genre` varchar(500) DEFAULT NULL,
  `epithete_infra_generique` varchar(500) DEFAULT NULL,
  `epithete_sp` varchar(500) DEFAULT NULL,
  `type_epithete` varchar(500) DEFAULT NULL,
  `epithete_infra_sp` varchar(500) DEFAULT NULL,
  `cultivar_groupe` varchar(500) DEFAULT NULL,
  `cultivar` varchar(500) DEFAULT NULL,
  `nom_commercial` varchar(500) DEFAULT NULL,
  `auteur` varchar(100) DEFAULT NULL,
  `annee` int(4) DEFAULT NULL,
  `biblio_origine` varchar(500) DEFAULT NULL,
  `notes` varchar(500) DEFAULT NULL,
  `nom_addendum` varchar(500) DEFAULT NULL,
  `basionyme` varchar(9) DEFAULT NULL,
  `nom_francais` text,
  `num_nom_coste` varchar(9) DEFAULT NULL,
  `num_nom_retenu_coste` varchar(9) DEFAULT NULL,
  `num_tax_sup_coste` varchar(9) DEFAULT NULL,
  `nom_coste` varchar(500) DEFAULT NULL,
  `auteur_coste` varchar(100) DEFAULT NULL,
  `biblio_coste` varchar(500) DEFAULT NULL,
  `synonymie_coste` varchar(500) DEFAULT NULL,
  `tome` int(1) DEFAULT NULL,
  `page` int(4) DEFAULT NULL,
  `nbre_taxons` varchar(9) DEFAULT NULL,
  `flore_bdtfx_nn` varchar(9) DEFAULT NULL,
  `flore_bdtfx_nt` varchar(9) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `page_wiki_dsc` varchar(100) DEFAULT NULL,
  `page_wiki_cle` varchar(100) DEFAULT NULL,
  `nom_sci_html` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`num_nom`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
