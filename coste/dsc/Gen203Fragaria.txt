Genre **203**. — **FRAGARIA** L. — //Fraisier//.
(Du latin //fragrans//, odorant : allusion au parfum du fruit.)

Calice persistant, à 10 lobes sur 2 rangs, les 5 extérieurs plus petits en calicule ; 5 pétales, largement obovales ; étamines nombreuses ; styles latéraux, courts, marcescents ; fruit bacciforme, formé de nombreux carpelles secs, lisses, disposés sur un réceptacle ovoïde ou globuleux, charnu-succulent, odorant et caduc à la maturité.
Fleurs blanches ou un peu rosées, en cymes terminales peu fournies ; pédicelles fructifères recourbés ; tiges et pétioles couverts de poils étalés ; feuilles à 3 folioles dentées tout autour ; stipules soudées au pétiole ; plantes vivaces, stolonifères.
5 ou 6 espèces habitant tout l'hémisphère boréal et l'Amérique méridionale. Toutes sont cultivées pour leurs fruits ou fraises, dont les usages et les propriétés sont bien connus. Les racines des fraisiers sont astringentes.