Genre **48**. — **CARDAMINE** L. — //Cardamine//.
(Du grec //cardamon//, cresson : les Cardamines ressemblent beaucoup au Cresson.)

Sépales dressés ou un peu étalés, égaux à la base ; stigmate entier ou échancré ; silique linéaire, mince, comprimée ; valves planes, sans nervures, s'ouvrant avec élasticité et se roulant de bas en haut à la maturité ; bec court ou long ; graines ovales, comprimées, rarement ailées, sur 1 rang.
Fleurs blanches, lilacées ou purpurines ; feuilles simples ou découpées ; plantes glabres ou un peu hérissées à la base.
Environ 75 espèces habitant les régions tempérées et arctiques de tout le globe.