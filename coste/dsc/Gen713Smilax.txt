Genre **713**. — **SMILAX** L.
(Du grec smilê, grattoir : plante très rude.)

Environ 200 espèces habitant les régions chaudes et tropicales des deux mondes.