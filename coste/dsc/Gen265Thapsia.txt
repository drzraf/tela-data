Genre **265**. — **THAPSIA** L.
(Plante nommée ainsi par Pline en souvenir de l'île de //Thapsos//.)

Calice à 5 dents ; pétales entiers ou émarginés, avec une pointe courbée en dedans, les extérieurs non ou à peine plus grands ; fruit ovale, comprimé par le dos, a coupe transversale sublinéaire, à 4 ailes très larges ; méricarpes a 9 côtes, 5 primaires filiformes, et 4 secondaires, dont 2 dorsales filiformes ou obscurément ailées, et 2 marginales largement ailées ; vallécules a 1 bandelette ; carpophore libre, bifide.
Fleurs jaunes ou blanches, en ombelles à 10-30 rayons, la centrale grande et fertile, les latérales petites et ordinairement stériles ; un involucre ou non ; feuilles intérieures 2-5 fois pennatiséquées, les supérieures souvent réduites a la gaine ; plantes vivaces.
Environ 10 espèces habitant la région méditerranéenne.