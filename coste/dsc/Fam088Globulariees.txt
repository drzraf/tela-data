Famille **88**. — **GLOBULARIEES** (SÉLAGINÉES)
Dessins de Mlle Kastner.

Environ 140 espèces habitant les régions extratropicales de l'ancien monde.