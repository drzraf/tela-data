FAMILLE **2**. — **BERBÉRIDÉES**.
Dessins de M. DELPY.

Environ 105 espèces croissant surtout dans les régions tempérées de l'hémisphère boréal et de l'Amérique méridionale.