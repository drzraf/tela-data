FAMILLE **87**. — **PLOMBAGINEES**.
Dessins de Mlle Kastneu.

Fleurs régulières. Calice persistant, tubuleux, souvent scarieux, à 5-10 lobes ; corolle délicate, tordue dans le bouton, tantôt polypétale, tantôt monopétale, à 5 divisions ; 5 étamines, opposées aux divisions de la corolle, insérées à sa base ou sur le réceptacle ; 5 styles, parfois soudés en un seul, alternes avec les étamines ; ovaire libre ; fruit sec, membraneux, renfermé dans le calice, à 1 loge et à 1 graine, indéhiscent ou s'ouvrant irrégulièrement vers la base.
Fleurs purpurines, bleues, violettes ou blanches, en grappes ou en têtes munies de bradées ; feuilles simples, alternes ou toutes radicales, sans stipules ; plantes herbacées ou ligneuses à la base.
Près de 300 espèces répandues dans une grande partie du globe.