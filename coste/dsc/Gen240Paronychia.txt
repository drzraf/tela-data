Genre 240. — PARONYCHIA Juss. — //Paronyque//.
(Du grec //paronychia//, panaris : plante jadis employée pour guérir les panaris.)

Calice à 5 sépales mucronés, herbacés ou scarieux, voûtés ou dressés, non épaissis-spongieux ; pétales nuls ou rudimentaires ; 2-5 étamines ; style bifide, 2 stigmates ; capsule recouverte par le calice, indéhiscente ou s'ouvrant par la base en 5 valves soudées au sommet, contenant 1 seule graine.
Fleurs blanchâtres ou verdâtres, petites, en têtes ou en cymes munies de bractées scarieuses ; feuilles opposées ou verticillées, munies de stipules scarieuses-argentées ; plantes annuelles ou vivaces.
Environ 45 espèces habitant l'Europe méridionale, l'Orient, l'Afrique et l'Amérique.