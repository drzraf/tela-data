Genre **742**. — **CYPRIPEDIUM** L. — //Sabot de Vénus//.
(Du grec Cypris, Vénus, pedilon, chaussure : l'orme du iaholle.)

Environ 50 espèces habitant l'Europe, l'Asie, l'Amérique.