Genre **556**. — **SIBTHORPIA** L.
(Dédié à Sibthorp, botaniste anglais.) 

6 espèces habitant l'Europe, l'Afrique, l'Asie centrale et l'Amérique australe.