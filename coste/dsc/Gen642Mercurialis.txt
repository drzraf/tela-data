Genre **642**. — **MERCURIALIS** L. — //Mercuriale//.
(Dédié à Mercure qui en lit connaître, dit-on, les propriétés.)

Fleurs dioïques ou accidentellement monoïques. Périanthe herbacé, à 3 divisions soudées à la base ; 8-lS étamines libres ; 2 styles indivis, divergents, papilleux sur toute leur longueur ; ovaire muni entre les loges de 2 filets stériles ; capsule didyme, dressée, à 2 coques monospermes ; graines réticulées-rugueuses, caronculées.
Fleurs verdâtres, axillaires, les mâles en épis grêles interrompus longuement pédonculés, les femelles solitaires ou fasciculées ; feuilles opposées, munies de petites stipules ; plantes à suc aqueux .
Environ S espèces habitant l'Europe, la région méditerranéenne, l'Asie orientale.