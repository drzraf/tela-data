Genre **615**. — **BETA** L. — //Bette//.
(Du celtique belt, rouge : allusion k la racine rouge de la betterave.)

Fleurs hermaphrodites, sans bractées. Périanthe urcéolé, à 5 divisions soudées à la base, à tube coriace à la maturité ; S étamines, insérées sur un disque charnu qui unit le périanthe à l'ovaire ; style court, 2-3 stigmates ; fruit subglobuleux-déprimé, renfermé dans le périanthe ligneux et accrescent ; graine horizontale, lenticulaire.
Fleurs verdâtres ou rougeâtres, sessiles, en petits glomérules axillaires formant de longs épis lâches ; feuilles alternes, entières, ovales ou rhomboïdales ; plantes herbacées, plus ou moins charnues.
Environ 12 espèces habitant l'Europe, l'Orient, l'Afrique septentrionale.