Genre **155**. — **SAROTHAMNUS** Wimm.
(Du grec //saros//, balai, //thamnos//, buisson : les rameaux sont employés à la campagne à faire des balais.)

Calice scarieux, court, en cloche, à 2 lèvres divariquées, la supérieure à 2 dents superficielles, l'inférieure à 3 ; étendard grand, suborbiculaire, redressé ; carène obtuse ; étamines monadelphes ; style allongé, enroulé ou fortement courbé en cercle ; stigmate en tête ; gousse longuement saillante, oblongue, très comprimée, à faces glabres, noire à la maturité, à graines nombreuses et caronculées.
Fleurs jaunes, solitaires ou géminées aux noeuds, en grappes lâches et feuillées ; feuilles inférieures ou toutes trifoliolées, à folioles entières ; arbrisseaux non épineux, très rameux, verts, noircissant par la dessiccation.