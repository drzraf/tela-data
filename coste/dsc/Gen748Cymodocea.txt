Genre **748**. — **CYMODOCEA** Kœn.
(CA'modocée, nymphe des eaux.)

3 espèces habitant les mers de l'ancien monde et l'Australie.