Genre **769**. — **ERIOPHORUM** L. — //Linaigrelte, Jonc à coton//.
(Du grec erion, laine, pherein, porter : les épillets après la floraison ressemblent à des houppes de laine blanche.)

Fleurs hermaphrodites, en épillets ovoïdes ou oblongs, multiflores ; écailles florales peu nombreuses, imbriquées tout autour, presque égales, les inférieures parfois stériles ; style filiforme, caduc ; 3 stigmates ; akènes trigones, mutiques ou mucronés par la base non renflée du style, entourés de nombreuses soies blanches longuement saillantes et formant une aigrette cotonneuse.
Epillets brunâtres ou fauves, tantôt solitaires en tète terminale, tantôt en ombelle à pédoncules inégaux ; feuilles trigones au moins au sommet, engainant la tige ; plantes des marais ou des tourbières.
13 espèces habitant les régions tempérées et froides de l'hémisphère boréal.