Genre 209. — ALCHEMILLA L. — //Alchémille//.
(De l'arabe //alkemelieh//, alchimie : ces plantes ont été l'objet de divers essais de la part des alchimistes.)

Calice à 8 lobes persistants, sur 2 rangs, les 4 extérieurs en calicule et parfois peu apparents ; corolle nulle ; 1-4 étamines ; 1-2 styles latéraux, partant de la base du carpelle ; stigmate en tête ; ovaire infère; fruit sec, très petit, à 1-2 graines renfermées dans le tube du calice.
Fleurs verdâtres ou jaunâtres, petites, en cymes corymbiformes ou en fascicules ; feuilles palmatilobées ou palmatipartites, munies de stipules persistantes ; plantes herbacées.
Environ 50 espèces habitant les régions tempérées des deux mondes et l'Afrique australe. Plantes astringentes et toniques, recherchées par les animaux dans les pâturages.