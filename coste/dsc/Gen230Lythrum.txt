Genre 230. — LYTHRUM L. — //Salicaire//.
(Du grec //lythron//, sang des blessures : allusion à la couleur des fleurs.)

Calice tubuleux, cylindrique, muni de côtes externes plus longues, à 8-12 dents ; 4-6 pétales, dépassant les dents du calice ; style allongé, filiforme ; stigmate en tête ; capsule oblongue ou cylindrique, complètement enveloppée par le calice.
Environ 25 espèces répandues dans presque tout le globe.