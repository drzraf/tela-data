Genre **236**. — **CLAYTONIA** L.
(Genre dédié à Clayton, médecin de Virginie, décédé en 1773.)

Environ 20 espèces habitant l'Asie boréale, l'Amérique, la Nouvelle-Zélande.