Genre **650**. — **FICUS** L. — //Figuier//.
(Nom latin du Figuier, dérivé lui-même du grec sykê.)

Environ CSG espèces habitant les régions tropicales et subtropicales des deux mondes.