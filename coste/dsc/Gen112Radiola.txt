Genre **112**. — **RADIOLA** L. — //Radiole//.
(Du latin //radius//, rayon : allusion aux lobes du calice et aux valves de la capsule rayonnants en étoile à la maturité.)

1 seule espèce.