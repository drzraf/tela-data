Genre 222. — ISNARDIA L. (Ludwigia L.)
(Dédié à Isnard, professeur de botanique au jardin du roi au XVIIIe siècle.)

Environ 20 espèces habitant l'Europe, l'Asie, l'Afrique, 1'Amérique boréale.