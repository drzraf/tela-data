Genre **602**. — **LIPPIA** L.
(Dédié à Lippi, botaniste français, mort en 1705.)

Environ 90 espèces habitant les régions chaudes surtout de l'Amérique.