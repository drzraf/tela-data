Genre **267**. — **SILER** Crantz.
(Nom formé de //Sium// et de //Selinum//.)

2 espèces habitant l'Europe et l'Asie.