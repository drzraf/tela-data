Genre **595**. — **SCUTELLARIA** L. — //Scutellaire//.
(Du latin scuta, écuelle, tasse : forme du calice.)

Calice en cloche courte, à 2 lèvres entières, arrondies, rapprochées après la floraison, la supérieure à la fin caduque, portant sur le dos une écaille saillante arrondie et concave, l'inférieure persistante ; corolle bilabiée, à tube longuement saillant, nu en dedans, courbé inférieurement ; lèvre supérieure voûtée en casque, trilobée, l'inférieure entière ; 4 étamines, rapprochées-parallèles sous le casque ; anthères ciliées, celles des étamines extérieures à 1 loge, celles des élamines intérieures à 2 loges presque parallèles ; style à lobes presque égaux ; carpelles subglobuleux, tuberculeux.
Fleurs bleues, violacées ou rosées, pédicellées 2 à 2, axillaires ou en grappe terminale ; feuilles dentées au moins à la base ; plantes vivaces, vertes, pubescentes ou glabrescentes.
Environ 100 espèces répandues dans presque tout le globe.