Genre **572**. — **ORIGANUM** L. — //Origan//.
(Du grec //oros//, montagne, //ganos//, ornement : plante ornementale.)

Environ 30 espèces habitant l'Europe, l'Asie extratropicale, l'Afrique septentrionale.