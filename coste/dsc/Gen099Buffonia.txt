Genre **99**. — **BUFFONIA** Sauv.
(Genre dédié à //Buffon// par Sauvages.)

4 sépales scarieux ; 4 pétales, entiers ou bidentés ; 2‑4 ou 8 étamines ; 2 styles, très courts, opposés aux sépales externes ; capsule membraneuse, comprimée‑lenticulaire, à 2 valves entières et opposées aux sépales internes, à 2 graines obovales, tuberculeuses.
Fleurs blanches, petites, en fascicules formant une panicule allongée ; feuilles linéaires en alène, dilatées à la base et connées ; plantes grêles, étalées‑rameuses, glabres.
7 espèces habitant la région méditerranéenne et l'Asie occidentale.