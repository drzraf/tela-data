Genre **96**. — **DIANTHUS** L. — //Oeillet//.
(Du grec //Dios//, Jupiter, //anthos//, fleur : l'Œillet était consacré au dieu Jupiter à cause de sa beauté.)

Calice tubuleux, entouré à la base d'écailles en forme de calicule, sans nervures commissurales, à 5 dents ; 5 pétales, à onglet ordinairement long, muni de bandelettes ailées, sans écailles à la gorge ; 10 étamines ; 2 styles filiformes ; capsule à 1 loge, s'ouvrant au sommet par 4 dents, à carpophore court ; graine en bouclier, ombiliquées au centre d'une des faces.
Fleurs rouges ou roses, rarement blanches, solitaires ou agglomérées au sommet des tiges ; feuilles linéaires ou lancéolées, les caulinaires connées à la base ; plantes ordinairement vivaces.
Environ 240 espèces habitant l'Europe, l'Asie, l'Afrique et l'Amérique du Nord, souvent cultivées comme ornement.