Genre **670**. — **ABIES** DC — //Sapin//.
(Du grec abin, abios : qui vit longtemps.)

Fleurs monoïques. Chatons mâles solitaires, axillaires ou terminaux ; cônes allongés, oblongs-cylindriques, à écailles imbriquées-spiralées, coriaces, minces et tranchantes sur les bords, non épaissies au sommet, à maturation annuelle ; graines géminées à la base de chaque écaille, terminées par une aile large ; 4-10 cotylédons.
Chatons tous insérés sur les rameaux de l'année précédante ; feuilles toutes solitaires, naissant isolément, en spirale, linéaires, raides, persistantes ; arbres élevés, droits, à branches verticillées et étalées horizontalement.
Environ 30 espèces habitant les régions exlratropicales de l'hémisphère boréal.
Toutes fournissent des bois de construction et de travail de premier ordre.