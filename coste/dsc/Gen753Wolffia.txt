Genre 753.— WOLFFIA Horkel.
(Dédié à Wolff, botaniste allemand.)

Environ 12 espèces habitant presque tout le globe.