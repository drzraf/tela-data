Genre **201**. — **POTENTILLA** L. — //Potentille//.
(Du latin //potens//, puissant, fortifiant : allusion aux propriétés toniques et astringentes de ces plantes.)

Calice persistant, à 10-8 lobes sur 2 rangs, les 5-4 extérieurs en calicule ; 5-4 pétales, obovales ou orbiculaires, entiers ou échancrés ; étamines nombreuses ; styles latéraux, courts, caducs ; ovaire libre et supère ; fruit sec, composé de nombreux carpelles monospermes, indéhiscents, sans arête, réunis sur un réceptacle poilu, convexe, persistant.
Fleurs blanches, roses ou jaunes, solitaires et axillaires ou en cymes terminales ; feuilles trifoliolées, digitées ou imparipennées, à folioles ordinairement dentées ; stipules soudées au pétiole ; plantes herbacées, rarement un peu ligueuses.
Environ 160 espèces habitant les régions tempérées et froides de l'hémisphère boréal.