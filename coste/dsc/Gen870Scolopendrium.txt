Genre **870**. — **SCOLOPENDRIUM** Sm. — //Scolopendre//.
(Du grec scolopendra, scolopendre, mille-pieds : la disposition des sores rappelle les pattes de cet insecte.)

Sores linéaires, allongés, espacés, parallèles entre eux et disposés obliquement entre la nervure médiane et le bord de la fronde ; indusie linéaire, d'abord connivente sur le sore, puis s'ouvrant en long par le milieu et se repliant comme en 2 valves.
Fructifications disposées en groupes sur la face inférieure des feuilles ; feuilles simples, échancrées en cœur à la base, à pétiole et rachis poilus-écailleux ; plantes vivaces. à souche épaisse gazonnante.
Environ 14 espèces habitant les lieux humides des deux mondes.