Genre **690**. — **GAGEA** Salisl).
(Hédii' à Thomas Gage, botaniste anglais, f en 1820.)

Périanthe persistant, étalé pendant la floraison, à G divisions libres ou un peu soudées à la base, oblongues ou lancéolées, égales, planes ; G étamines, bien plus courtes que le périanthe, à filets filiformes, insérés à la base des pétales ; anthères dressées, fixées au filet par leur base creuse ; style filiforme, trigone, astigmate en tète subtrilobée ; capsule ovoïde-trigone, à loges renfermant plusieurs graines subglobuleuses ou anguleuses.
Fleurs jaunes en dedans, verdâtres en dehors, assez petites, solitaires ou encorymbe terminal muni de bractées ; feuilles peu nombreuses, linéaires, les radicales i-3, plus longues que la tige, les caulinaires alternes ou opiiosées sous l'inflorescence ; plantes peu élevées, à bulbe tunique petit.
Environ 2b espèces habitant l'Europe, l'Asie tempérée, l'Afrique septentrionale.