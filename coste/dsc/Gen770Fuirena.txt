Genre **770**. — **FUIRENA** Rottbôll.
(Dédié à Fuiren, botaniste danois.)

Environ âo espèces habitant les régions chaudes des deux mondes.