Genre **31**. — **FUMARIA** L. — //Fumeterre//.
(Du latin //fumus//, fumée : plantes ayant l'amertume de la fumée.)

Sépales pétaloïdes ou très petits, plus courts que la corolle ; pétale supérieur prolongé en éperon ou talon court et obtus ; fruit en forme de silicule, court, globuleux ou ovoïde, à graine unique, ombiliquée.
Fleurs rouges, roses ou blanches, toujours brunâtres au sommet, en grappes multiflores, opposées aux feuilles ; feuilles bi-tripennatiséquées, à segments ovales, lancéolés ou linéaires ; plantes annuelles, à tiges anguleuses, faibles, souvent grimpantes au moyen des pétioles contournés en vrilles.
Environ 10 espèces habitant l'Europe, l'Asie centrale et l'Afrique extratropicale.