Genre **111**. — **LINUM** L. — //Lin//.
(Du grec //linon//, fil : l'écorce du Lin est filamenteuse et textile.)

5 sépales, libres, entiers ; 5 pétales, plus longs que les sépales ; 5 étamines fertiles ; 5 styles ; capsule globuleuse ou ovoïde‑conique, à 10 loges monospermes.
Fleurs jaunes, bleues, roses ou blanches ; feuilles entières, opposées, alternes ou éparses ; plantes herbacées ou un peu ligneuses à la base, à tiges sans noeuds, à écorce résistante et filamenteuse.
Environ 100 espèces habitant les deux mondes. Plusieurs sont cultivées dans les parterres, à cause de la beauté de leurs fleurs.