Genre **195**. — **PRUNUS** L. — //Prunier//.
(Nom latin du Prunier, dérivé du grec //proumnon//.)

Calice caduc, en cloche, à 5 lobes ; 5 pétales ; 15-30 étamines ; 1 style ; ovaire libre ; drupe globuleuse ou oblongue, glabre mais couverte d'une poussière cireuse glauque,
charnue-succulente, colorée, indéhiscente ; noyau ovoïde ou oblong, plus ou moins comprimé, a faces lisses ou rugueuses, à bords sillonnés, à 1-2 amandes amères.
Fleurs blanches, naissant avant les feuilles, solitaires, géminées ou fasciculées, pédonculées ; pédoncules fructifères ordinairement plus courts que le fruit ; feuilles enroulées dans,leur jeunesse, ovales ou oblongues, dentées ou crénelées ; stipules caduques ; arbres ou arbrisseaux souvent épineux.