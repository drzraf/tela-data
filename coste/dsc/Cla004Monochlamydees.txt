Quatrième classe. — MONOGHLAMYDÉES ou APÉTALES.

Une seule enveloppe florale (périanthe ou périgone), herbacée en forme de calice ou colorée en forme de corolle, parfois nulle ; ovules contenus dans un ovaire fermé, et recevant l'action du pollen au moyen d'un stigmate.