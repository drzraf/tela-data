Genre 233. — ECBALLIUM Rich. — //Momordique//.
(Du grec //ecballô//, je lance au dehors : le fruit projette ses graines par le pédoncule à la maturité.)

1 seule espèce.