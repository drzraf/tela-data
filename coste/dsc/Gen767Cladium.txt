Genre **767**. — **CLADIUM** Browne.
(Du grec clados, rameau : inllorescence très rameuse.)

Environ 33 espèces habitant les régions tempérées et tropicales des deux mondes.