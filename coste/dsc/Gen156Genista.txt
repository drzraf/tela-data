Genre **156**. — **GENISTA** L. — //Genêt//.
(Du celtique //gen//, petit buisson : allusion au port de ces arbrisseaux,)

Calice à 2 lèvres dressées, rarement divariquées, la supérieure à 2 lobes presque toujours profonds, l'inférieure à 3 dents ; étendard peu élargi, le plus souvent non redressé ; carène obtuse, droite ou à peine courbée ; étamines monadelphes ; style ascendant, stigmate oblique ; gousse saillante, allongée, comprimée, à graines ordinairement non caronculées.
Fleurs jaunes, axillaires ou terminales ; feuilles simples ou trifoliolées, à folioles entières ; stipules petites ou nulles ; arbrisseaux ou sous‑arbrisseaux épineux ou sans épines.
Environ 70 espèces habitant l'Europe, l'Asie occidentale, l'Afrique septentrionale. Amères et diurétiques, elles servent surtout à la nourriture des bestiaux.