Genre **637**. — **ASARUM** L. — //Asaret//.
(Du grec a, non, sairein, orner : (leur sans éclat.)

13 espèces habitant l'Europe, l'Asie tempérée, l'Amérique boréale.