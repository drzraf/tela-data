Genre **688**. — **ERYTHRONIUM** L.
(Du grec erylhros, rouge : couleur des fleurs et des taches des feuilles.)

7 espèces habitant les régions tempérées de l'hémisphère boréal.