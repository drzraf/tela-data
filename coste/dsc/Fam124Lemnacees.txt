FAMILLE **124**. — **LEMNACEES**.
Dessins de Mlle Kastner.

Fleurs monoïques, sans périanthe. Fleurs mâles réduites à 1-2 étamines à filets filiformes ou nuls, anthère à 1-2 loges ; fleurs femelles solitaires, composées d'un ovaire libre, sessile, uniloculaire, et d'un style court à stigmate concave-pelté ; fruit utriculaire,un peu charnu, à 1 graine et indéhiscent ou à plusieurs graines et déhiscent.
Fleurs minuscules, sortant du bord d'une fronde ou naissant parfois à sa surface ; très petites plantes aquatiques, flottantes, non fixées au sol, réduites à des frondes lenticulaires, toujours rassemblées en grand nombre, se reproduisant surtout par bourgeonnement .
Environ 20 espèces dispersées sur tout le globe. Elles sont mangées par les canards et purifient les mares et les eaux stagnantes qu'elles couvrent parfois complètement.