Genre **759**. — **TYPHA** L. — //Massette, Quenouilles//.
(Du grec tiphos, marais : plantes des marais.)

Deux épis terminant la hampe, le supérieur mâle formé d'étamines et de filaments stériles, l'inférieur femelle formé d'ovaires fertiles entremêlées de soies et d'ovaires stériles en forme de massues. Périanthe constitué par de nombreuses soies très fines ; étamines à filets simples ou bi-trifides, anthère à 4 loges ; style long, capillaire, à stigmate en languette linéaire ou obovale ; fruit très petit, en fuseau, porté sur un pédicelle capillaire, indéhiscent ou s'ouvrant par une fente longitudinale.
Fleurs roussâtres, très nombreuses, en 2 épis cylindracés, très compacts, superposés, le supérieur mâle caduc, sortant chacun d'une spathe très caduque ; feuilles engainant longuement la tige raide et sans nœuds, toutes radicales, dressées, coriaces, très allongées ; souche rampante-stolonifère.
Environ 10 espèces habitant les régions tempérées et tropicales des deux mondes .