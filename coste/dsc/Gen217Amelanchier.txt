Genre 217. — AMELANCHIER Medik. — //Amélanchier//.
(Du grec //a//, privatif, //mêlea// pommier, //anchein// étrangler : nom donné par antiphrase à cause de la douceur des fruits.)

4 espèces habitant les régions tempérées de l'hémisphère boréal.