Genre **679**. — **ALISMA** L. — //Fluteau//.
(Du celtique alis, eau : plantes aquatiques.)

Fleurs hermaphrodites ; sépales herbacés ; pétales caducs, beaucoup plus longs que les sépales ; 6 étamines, opposées par paires aux pétales ; carpelles petits, nombreux, arrondis ou à peine mucronés au sommet, verticillés ou en têtes globuleuses, monospermes, indéhiscents.
Fleurs en verticillés ou en ombelles, rarement solitaires et axillaires ; feuilles longuement pétiolées, nervées ; plantes vivaces.
Environ 12 espèces habitant presque tout le globe.