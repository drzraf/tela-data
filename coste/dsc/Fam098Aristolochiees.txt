FAMILLE **98**. — **ARISTOLOCHIEES**.
Dessins de Mlle Kastner.

Fleurs hermaphrodites. Périanthe coloré, régulier ou irrégulier, tubuleux en languette ou en cloche trifide ; 6 ou 12 élamines, insérées sur l'ovaire, à anthères libres ou soudées, à 2 loges s'ouvrant en long ; style court, en colonne, à 6 stigmates rayonnants ; ovaire adhérent ou demi-adhérent ; fruit capsulaire, coriace, à G loges polyspermes, s'ouvrant irrégulièrement ou par 6 valves.
Fleurs jaunâtres ou brunâtres, solitaires ou fasciculées, pédonculées ; feuilles alternes ou opposées, largement en cœur à la base, entières, palminervées, sans stipules ; liantes vivaces herbacées, à souche ordinairement rampante ou tubéreuse.
Environ 223 espèces répandues dans les régions tempérées et chaudes de presque tout le globe.