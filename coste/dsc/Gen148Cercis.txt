Genre **148**. — **CERCIS** L. — //Gainier//.
(Du grec //cercis//, navette : allusion à la forme de la corolle ou de la gousse.)

3 ou 4 espèces habitant les régions tempérées de l'Europe, de l'Asie, de l'Amérique.