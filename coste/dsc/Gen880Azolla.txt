Genre **880**. — **AZOLLA** Lamk.
(Du grec a, sans, zoé, vie: tué par la sécheresse.)

Sporocarpes globuleux, membraneux, uniloculaires, presque indéhiscents ; sporanges les plus grandes (macrosporanges) 1-2 placées au fond du sporocarpe, ovoïdes-apiculées, subsessiles ; les plus petites (microsporanges) très nombreuses dans la partie supérieure du sporocarpe, globuleuses, pédicellées.
Fruits naissant en dessous des tiges à la base des rameaux inférieurs, petits, sessiles, groupés par 2-4 ; feuilles enroulées par le côté dans leur jeunesse, très petites, alternes, densément imbriquées sur 2 rangs, sessiles ; petites plantes annuelles nageantes, à tiges capillaires radicantes.
5 espèces habitant les deux mondes.