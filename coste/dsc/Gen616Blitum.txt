Genre **616**. — **BLITUM** L.
(Du grec blilon, fade, insipide : plante sans saveur.)

Fleurs hermaphrodites, sans bractées. Périanthe herbacé, à 3-S divisions soudées inférieuremenl, d'abord planes, puis gonflées-charnues, succulentes et d'un rouge vif à la maturité ; 1-5 étamines, à filets capillaires insérés sur le réceptacle ; 2 styles et stigmates ; fruit subglobuleux-comprimé, membraneux, libre, recouvert par le périanthe devenu charnu en forme de baie ; graine verticale, lenticulaire.
Fleurs verdâtres puis rougcâtres, sessiles, en glomérules axillaires ou terminaux ; feuilles alternes, pétiolées, triangulaires ou haslées ; plantes annuelles, vertes, inodores.
2 espèces habitant l'Europe, l'Asie tempérée, l'Afrique septentrionale.