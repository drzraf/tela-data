Genre **97**. — **VELEZIA** L.
(Genre dédié à Velez, botaniste espagnol.)

4 espèces habitant la région méditerranéenne.