Genre **851**. — **NARDURUS** Reichb.
(Du groc nardos, nard, oui^a, queue : épi grolc et ressemblant à celui du Nardus.)

Epillets comprimés par le côté, à 3-9 fleurs ; glumes inégales ou presque égales, mutiques, carénées, à 1-3 nervures, plus courtes que les fleurs ; glumelles égales ou un peu inégales, l'inférieure oblongue, convexe et arrondie sur le dos, mutique ou aristée ; la supérieure bicarénée, ciliée, bidentée ; 3 étamines ; stigmates sessiles, latéraux ; caryopse glabre, oblong on linéaire, canaliculé à la face interne, adhérent aux glumelles ou libre.
Epillets subsessiles, solitaires dans les excavations de l'axe, appliqués contre lui par une de leurs faces, formant un épi grêle, distique ou unilatéral, mutique ou aristé ; feuilles enroulées-sétacées, à ligule courte tronquée ; plantes annuelles grêles.
7 ou 8 espèces habitant l'Europe, l'Asie, l'Afrique.