Genre **59**. — **CAMELINA** Crantz. — //Cameline//.
(Du grec //camaï//, à terre, //linon//, lin : ces plantes croissent dans les champs de lin.)

Sépales dressés, presque égaux à la base ; silicules obovales ou en poire, renflées, déhiscentes ; valves coriaces, convexes, munies d'une nervure dorsale, terminées en appendice appliqué sur la hase du style court ; graines ovoïdes, non ailées, plusieurs dans chaque loge, sur 2 rangs.
Fleurs jaunes, petites ; feuilles entières, dentées ou sinuées‑pennatifides, les caulinaires embrassantes‑auriculées ; plantes annuelles, rudes et plus ou moins velues.
Environ 5 espèces habitant l'Europe, l'Asie et l'Afrique septentrionale.