Genre **1**. **—** **RANUNCULUS** L. **—** //Renoncule//.
(//Rana//, grenouille, //colere//, habiter : plusieurs espèces sont aquatiques et servent ; de retraite aux grenouilles.)

5 sépales ; 5 pétales ou plus, à onglet presque toujours muni d’une écaille on d’une fossette nectarifère ; carpelles non bossus à la base, mucronés ou terminés en bec court, disposés en têtes globuleuses ou oblongues.
Fleurs solitaires, jaunes ou blanches ; feuilles alternes, rarement toutes radicales ; plantes herbacées.
Environ 200 espèces croissant dans tout le globe.