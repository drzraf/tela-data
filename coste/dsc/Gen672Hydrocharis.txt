Genre **672**. — **HYDROCHARIS** L. ~ Morène.
(Du grec hyclôr, eau, charis, ornement : ornement des eaux.)

1 seule espèce.