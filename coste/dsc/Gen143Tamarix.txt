Genre **143**. — **TAMARIX** L.

Calice à 4‑5 lobes ; 4‑5 pétales ; 5 étamines, insérées sur un disque glanduleux, à filets libres ou à peine soudés à la base ; 3 styles, à stigmates élargis au sommet ; graines dressées, munies au sommet d'une aigrette de poils sessile.
Environ 25 espèces habitant l'hémisphère boréal de l'ancien monde et l'Afrique australe.