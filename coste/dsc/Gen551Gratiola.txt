Genre **551**. — **GRATIOLA** L. — //Gratiole//.
(Du latin //gratia//, grâce : plante élégante et pleine de charme.)

Environ 20 espèces habitant les régions chaudes et tempérées des deux mondes.