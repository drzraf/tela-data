Genre **801**. — **SORGHUM** Pers. — //Sorgho//.
(De sorghi, nom indien du sorgho.)

Epillets comprimés par le dos, polygames, à 2 fleurs dont 1 seule fertile, l'autre ou toutes 2 stériles ; glumes presque égales, coriaces, luisantes, mutiques, tridentées ou rongées au sommet, égalant ou dépassant les fleurs ; glumelles membraneuses, inégales, l'inférieure mutique ou munie dans l'échancrure d'une arête genouillée ; glumellules longuement ciliées ; 3 étamines ; stigmates terminaux ; caryopse glabre, ovale ou arrondi, un peu comprimé par le dos .
Epillets géminés ou ternes, de forme différente, l'un sessile et fertile, l'autre ou les 
2 autres pédicellés et stériles, tous DES à la base, assez nombreux au sommet des rameaux et formant ensemble une grande panicule rameuse ; feuilles planes, larges de 
1-3 cm., à nervure médiane forte et blanche, à ligule poilue ; plantes robustes, à nœuds des chaumes pubescents.
4 ou S espèces habitant les régions chaudes ou tempérées des deux mondes. La plupart sont cultivées comme plantes fourragères. Les graines du sorgho commun servent à la nourriture de l'homme et des animaux domestiques, et ses panicules, séchées avec soin, à la confection des balais de jonc.