Genre **100**. — **ALSINE** Wahl. — //Alsine//.
(Du grec //alsos//, bois sombre : plusieurs espèces croissent dans les bois ombragés.)

5 sépales ; 5 pétales, entiers ou à peine émarginés, parfois nuls ; 10 étamines, rarement 3‑5 ; 3 styles, opposés aux sépales externes ; capsule membraneuse, ovoïde ou cylindracée, s'ouvrant jusqu'à la base en 3 valves entières et opposées aux sépales internes ; graines nombreuses, petites, en rein, ordinairement tuberculeuses.
Fleurs blanches, en cyme ou en panicule, rarement solitaires ; feuilles linéaires ou lancéolées ; plantes herbacées, peu élevées.
Environ 80 espèces répandues dans presque tout le globe.