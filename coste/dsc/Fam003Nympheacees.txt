FAMILLE **3**. — **NYMPHÉACÉES**.
Dessins de Mme HERINCQ.

Fleurs régulières ; sépales 4-5, grands, colorés en dedans ; pétales nombreux, souvent sur plusieurs rangs ; étamines nombreuses, à filets pétaloïdes ; stigmates sessiles, rayonnante, persistants ; ovaire libre ; fruit charnu-herbacé, indéhiscent, à plusieurs loges contenant plusieurs graines.
Fleurs blanches ou jaunes, grandes, solitaires sur de longs pédoncules ; feuilles flottantes, toutes radicales, longuement pétiolées, arrondies et échancrées en coeur ; herbes aquatiques, vivaces, à rhizome gros et charnu.
Environ 35 espèces croissant dans les eaux de presque tout le globe.