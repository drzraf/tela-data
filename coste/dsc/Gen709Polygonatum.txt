Genre **709**. — **POLYGONATUM** Ail. — //Sceau-de-Salomon//.
(Du grec polys, beaucoup, gonu, genou : tige formée d'articulations nombreuses et renflées comme un genou.)

Fleurs hermaphrodites. Périanthe caduc, tubuleux-cylindrique, un peu dilaté au sommet, à 6 dents courtes, vertes, dressées ou peu étalées ; 6 étamines incluses, insérées vers le milieu du périanthe, à anthères linéaires fixées par le dos ; 1 style filiforme, à stigmate obtus et trigone ; baie globuleuse, violacée ou noirâtre, à 3 loges contenant chacune 2 graines globuleuses jaunâtres.
Fleurs blanches, vertes au sommet, penchées, 1-6 articulées sur des pédoncules axillaires courts ; feuilles toutes caulinaires, alternes ou verticillées, sessiles ou subsessiles ; plantes herbacées, à tige feuillée, simple, à souche horizontale épaisse et noueuse.
13 espèces habitant les régions tempérées de l'hémisphère boréal.