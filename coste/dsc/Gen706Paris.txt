Genre **706**. — **PARIS** L. — //Pariselle//.
(Dédié au célèbre berger Pàrh, ou du latin par, paire : feuilles disposées par paires égales.)

Environ 9 espèces habitant l'Europe et l'Asie tempérée.