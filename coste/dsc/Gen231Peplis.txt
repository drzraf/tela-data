Genre 231. — PEPLIS L.
(Du grec //peplis//, pourpier ?)

Calice en cloche, sans côtes saillantes, à 10-12 dents égales ou les extérieures plus longues ; 5-6 pétales, petits, ovales-arrondis, caducs parfois nuls ; 6 étamines ; style très court ou assez long ; stigmate orbiculaire ; capsule globuleuse ou ovoïde.
4 espèces habitant l'Europe, l'Asie tempérée, l'Afrique septentrionale.