Genre **866**. — **POLYSTICHUM** Roth.
(Du grec polys, beaucoup, slichos, rangée : allusion aux nombreuses rangées de fructifications.)

Sores arrondis, distincts ou à la fin confluents, disposés sur 2 rangs à peu près réguliers et parallèles, rapprochés tantôt de la nervure médiane, tantôt du bord des lobes ; indusie orbiculaire en rein, ombiliquée, sessile, fixée par le centre et par un pli déprimé allant du centre à la circonférence, libre du reste et s'ouvrant sur la plus grande partie de son pourtour.
Fructifications disposées par groupes sur la face inférieure des feuilles ; feuilles 
2-4 fois peunatiséquées, toutes de même forme et fertiles, à pétiole écailleux ou nu ; plantes vivaces grandes, à souche épaisse gazonnante, rarement rampante.
Environ 73 espèces habitant presque tout le globe.