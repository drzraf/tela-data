Genre **622**. — **SUÆDA** Forsk. — //Soude//.
(De suœda, soude : plantes riches en soude.)

Fleurs hermaphrodites, munies de 2-3 bractéoles scarieuses. Périanthe urcéolé, à 
S lobes charnus, s'accroissant après la floraison et simulant une baie ; S étamines, insérées au fond du périanthe, libres ; 3-S stigmates lancéolés en alêne ; fruit comprimé, membraneux, enfermé dans le périanthe accru et non soudé avec lui ; graine verticale ou horizontale.
Fleurs verdâtres, solitaires ou en glomérules axillaires, disposées en longs épis feuilles ; feuilles alternes, charnues, linéaires, subcylindriques ; plantes très rameuses, à rameaux non articulés.
Environ 45 espèces habitant les rivages et les lieux salés de presque tout le globe.
Leurs cendres fournissent de la soude.