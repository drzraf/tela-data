Genre 868. ~ ATHYRIUM Roth.
(Du grec a privatif, (hyrion, petite porte : l'indusie a été considérée comme indéhiscente.)

Sores brièvement oblongs ou arrondis, distincts ou parfois à la fin confluents, sur 
2 rangs parallèles à la nervure médiane des lobes ; indusie oblongue ou arrondie^ conforme au sore, fixée par le côté à une nervure, s'ouvrant par son bord interne frangé ou cilié, persistante ou très caduque.
Fructifications disposées par groupes sur la face inférieure des feuilles ; feuilles bitripennatiséquées, toutes de même forme et fertiles, à pétiole assez court et écailleux ; plantes vivaces, assez robustes, à souche épaisse gazonnante.
Environ 2S espèces habitant presque tout le globe.