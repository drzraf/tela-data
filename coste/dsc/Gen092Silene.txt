Genre **92**. — **SILENE** L. — //Silène//.
(Le calice de l'espèce principale est ballonné comme le ventre du dieu Silène,)

Calice tubuleux, resserré ou mi peu évasé au sommet, muni de nervures commissurales, à 5 dents ; 5 pétales, à onglet long et sans bandelettes ailées, à gorge ordinairement couronnée d'écailles ; 10 étamines ; 3 styles ; capsule à 3 loges à la base ou à 1 seule, s'ouvrant au sommet par 6 dents ; graines nombreuses, en rein, chagrinées ou tuberculeuses.
Environ 250 espèces habitant l'Europe, l'Asie et l'Afrique extratropicales, et l'Amérique du Nord.