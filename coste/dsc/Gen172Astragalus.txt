Genre **172**. — **ASTRAGALUS** L. — //Astragale//.
(Du grec //astragalos//, vertèbre, os du talon: allusion à la forme de la gousse de quelques espèces.)

Calice tubuleux en cloche, à 5 dents presque égales ; étendard ovale ou oblong, non redressé, plus long que les ailes ; carène obtuse, non apiculée ; étamines diadelphes ; gousse saillante ou incluse, souvent subtrigone, peu ou point renflée, non ou très brièvement stipitée, à 2 loges complètes ou incomplètes, formées par la suture inférieure repliée en dedans ; graines ordinairement nombreuses.
Fleurs rouges, violettes, bleues, blanchâtres ou jaunâtres, en grappes ou en têtes axillaires ; feuilles imparipennées, rarement paripennées, sans vrilles à folioles entières.