Genre **817**. — **CORYNEPHORUS** P. Beauv.
(Du grec corynê, massue, j)herô, je porte : arête épaissie en massue au sommet.)

Epillets comprimés par le côté, ù2 fleurs, l'inférieure sessile et la supérieure stipitée ; glumes presque égales, membraneuses, carénées, aiguës, à 1-3 nervures, dépassant beaucoup les fleurs ; glumelles un peu inégales, membraneuses, l'inférieure barbue à la base, aiguë, entière, sans nervures, portant sur le clos au-dessus de la base une arête courte, droite, articulée et munie d'une très petite collerette de poils vers son milieu, un peu renflée en massue au sommet ; la supérieure un peu plus courte bicarénée, à 
3-4 dents ; 3 étamines ; stigmates latéraux ; caryopse glabre, oblong, sillonné à la face interne, adhérent aux glumelles.
Epillets pédicellés, fascicules, en panicule étalée à la floraison puis resserrée, verte ou violacée ; feuilles enroulées-sétacées, à ligule allongée ; plantes assez grêles, glabres.
3 espèces habitant l'Europe et la région méditerranéenne.