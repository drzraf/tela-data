Genre **145**. — **EMPETRUM** L. — //Camarine//.
(Du grec //empetros//, qui croit parmi les rochers.)

1 seule espèce habitant les régions montagneuses et froides des deux mondes.