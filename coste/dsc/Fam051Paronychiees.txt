FAMILLE 51. — PARONYCHIÉES.
Dessins de M. DENISE

Fleurs régulières ; calice persistant, à 5 sépales ; 5 pétales, souvent rudimentaires ou nuls, alternes avec les sépales ; 2-10 étamines, opposées aux sépales ; 2-3 styles ou stigmates ; ovaire libre ; fruit capsulaire, enveloppé par le calice, à 1 seule loge, indéhiscent ou à 3-5 valves, à 1 ou plusieurs graines.
Fleurs blanches ou verdâtres, petites, en fascicules axillaires ou en cymes terminales ; feuilles entières, le plus souvent munies de stipules scarieuses ; plantes herbacées, couchées-étalées.
Environ 100 espèces répandues dans presque tout le globe.