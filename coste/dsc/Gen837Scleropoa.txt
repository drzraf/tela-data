Genre **837**. — **SCLEROPOA** Gris.
(Du grec scieras, dur, poa, gazon : panicule raide et dure.)

Épiliets comprimés par le côté, à 5-11 fleurs persistantes ou caduques ; glumes presque égales, obtuses ou mucronées, carénées, à 1-3 nervures, bien plus courtes que les fleurs ; glumelles presque égales, l'inférieure carénée, entière, mutique ou mucronulée, à 3-5 nervures ; la supérieure bidentée ; 3 étamines ; stigmates subsessiles, presque terminaux ; caryopse glabre, oblong obtus, déprimé ou canaliculé à la face interne, adhérent aux glumelles, 
Epiliets oblongs, à pédicellés et rameaux trigones courts et épais, disposés en panicule ou en grappes spiciformes raides, unilatérales, vertes ou violacées ; feuilles planes ou enroulées, à ligule membraneuse déchirée ; plantes annuelles, glabres, feuillées souvent jusqu' à la panicule.
Environ 12 espèces habitant l'Europe et la région méditerranéenne. Plantes dures et sans importance.