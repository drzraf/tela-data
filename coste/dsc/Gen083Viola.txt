Genre **83**. — **VIOLA** L. — //Violette//.
(Du grec //ion//, violet : allusion à la couleur de la fleur de la Violette.)

Fleurs irrégulières ; calice persistant, à 5 sépales inégaux, prolongés à la base en appendices ; 5 pétales inégaux, l'inférieur terminé en éperon ; 5 étamines, à filets courts et élargis, à anthères dressées‑conniventes ; 1 style et 1 stigmate ; ovaire libre ; capsule à 1 loge, s'ouvrant par 3 valves, à plusieurs graines ovoïdes‑globuleuses.
Fleurs violettes, bleues, blanches, jaunes ou mélangées de ces diverses couleurs, solitaires sur un pédoncule ordinairement bractéolé, recourbé au sommet ; feuilles toutes radicales ou alternes, simples, pétiolées, stipulées ; plantes herbacées, rarement sous‑ligneuses à la base.
Environ 150 espèces répandues dans presque tout le globe.