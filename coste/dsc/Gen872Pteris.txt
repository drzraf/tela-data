Genre **872**. — **PTERIS** L. — //Fougère//.
(Nom grec de la fougère, dérivé de pleron, aile : feuilles en forme d'ailes.)

Sores linéaires, formant une ligne continue sous le bord enroulé des lobes ou des segments ; indusie linéaire, continue avec le bord des lobes, recouvrant les sores, s'ouvrant du côté interne et se renversant en dehors.
Fructifications disposées en lignes sous la marge inférieure des feuilles ; feuilles coriaces, 1-4 fois pennatiséquées, très longuement pétiolées ; plantes élevées, à souche rampante.
Environ 60 espèces, habitant surtout les régions chaudes.