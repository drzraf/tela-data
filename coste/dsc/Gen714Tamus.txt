Genre **714**. — **TAMUS** L. — //Tamier//.
(Nom donné par les Latins à une plante sarmentouse analogue au Tamier.)

2 espèces habitant l'Europe, l'Asie, l'Afrique septentrionale.