Genre **871**. — **BLECHNUM** L.
(Du grec blechnon, fougère, dérivé de blax, sans vertu.)

50 à 60 espèces habitant une grande partie du globe.