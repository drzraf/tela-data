Genre **708**. — **CONVALLARIA** L. — //Muguet//.
(Du latin convaUis, vallée, et du grec leirion, lis : Lis des vallées.)

Une seule espèce.