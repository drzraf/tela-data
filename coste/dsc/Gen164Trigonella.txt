Genre **164**. — **TRIGONELLA** L. — //Trigonelle//.
(Du grec //treis//, trois, //gônia//, angle : allusion à la forme triangulaire de la corolle.)

Calice en cloche, à 5 dents égales ou inégales ; corolle caduque ; ailes écartées de l'étendard, libres en avant ; carène obtuse, très courte, en sorte que la corolle paraît n'avoir que 3 pétales ; étamines diadelphes, à filets non dilatés au sommet ; gousse saillante, allongée, linéaire ou oblongue, un peu arquée, jamais contournée en hélice, ni épineuse, à graines nombreuses.
Fleurs jaunes, blanches ou rosées, solitaires, géminées, en ombelles ou en grappes courtes ; feuilles trifoliolées, à folioles dentées au sommet ; stipules soudées au pétiole par leur base ; plantes annuelles.
Environ 60 espèces habitant l'Europe, l'Asie, l'Afrique, l'Australie. Quelques‑unes ont cultivées comme fourragères.