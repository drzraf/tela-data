Genre **79**. — **CAPPARIS** L. — //Câprier//.
(De //capparis//, nom donné par les Grecs au Câprier.)

Environ 135 espèces habitant les régions chaudes et tropicales de tout le globe.