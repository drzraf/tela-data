Genre **78**. — **CRAMBE** L.
(Nom donné par les Grecs à diverses espèces de choux.)

Environ 20 espèces habitant l'Europe, l'Asie, l'Afrique.