Genre **798**. — **STENOTAPHRUM** Trin.
(Du grec slanos, étroite, (aphros, excavation.)

3 ou 4 espèces habitant les régions chaudes des deux mondes.