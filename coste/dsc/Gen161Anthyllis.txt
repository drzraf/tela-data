Genre **161**. — **ANTHYLLIS** L. — //Anthyllide//.
(Du grec //anthos//, fleur, //ioulos//, duvet : plantes velues‑soyeuses.)

Calice tubuleux, à dents plus courtes que le tube, rarement l'égalant ; étendard redressé ; ailes non connées ; carène presque toujours obtuse ; étamines monadelphes, à filets épaissis au sommet ; style ascendant, stigmate en tête ; gousse petite, renfermée dans le calice, stipitée, ordinairement glabre, à 1‑2 graines non caronculées.
Fleurs jaunes, rouges ou blanches, en têtes ou en grappes ; feuilles simples, trifoliolées ou imparipennées, sans stipules, à folioles entières ; plantes annuelles, vivaces ou ligneuses.
Environ 20 espèces habitant l'Europe, l'Asie occidentale, l'Afrique septentrionale.