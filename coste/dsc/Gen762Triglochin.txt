Genre **762**. — **TRIGLOCHIN** L. — //Troscarl//.
(Du grec, Ireis, trois, glôchis, pointe : fruit à trois pointes à la base.)

Périanthe à 6 divisions libres, ovales, concaves, presque égales ; étamines à filets très courts, à anthères fixées au filet par le milieu du dos ; 3 ou 6 stigmates subsessiles, courts, plumeux ; fruit ovoïde ou linéaire, formé de 3-0 carpelles monospermes, soudés dans toute leur longueur à un axe filiforme dont ils se séparent de bas en haut à la maturité.
Fleurs d'un vert blanchâtre, petites, brièvement pédonculées, en longue grappe spiciforme ; feuilles toutes radicales à la base de la tige sans nœuds ; plantes à souche plus ou moins bulbeuse.
Environ 10 espèces habitant les régions tempérées des deux mondes.