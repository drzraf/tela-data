FAMILLE **122**. — **POTAMEES**.
Dessins (.le M. Dknise.

Fleurs hermaphrodites, monoïques ou dioïques. Périanthe herbacé à 4 divisions, ou en forme de petite coupe, ou nul et remplacé par des spalhes ou bractées ; 1 ou plusieurs élamines libres, bypogyncs, anthère à 1-4 loges ; 1 style très court ou filiforme, à sligmale simple ou bifide ; ovaire libre ; fruit simple ou composé de 2-8 carpelles libres ou soudés à la base, sessiles ou pédicellés, ordinairement à 1 seule graine.
Fleurs vcrdàtres ou rougeâtres, petites, sans apparence, axillaires, solilaircs, en épi ou en spadice ; feuilles alternes ou opposées, entières ou ondulécs-denticulées, souvent munies de stipules ; plantes aquatiques vivaces, submergées ou flotlantes.
Environ 100 espèces ré[)andues dans les eaux douces ou salées de tout le globe. Très abondantes dans certaines eaux, elles sont utilisées surtout comme engrais.