Genre **128**. — **IMPATIENS** L. — //Impatiente//.
(Du latin //impatiens//, impatient : allusion à la déhiscence précipitée de la capsule à la maturité.)