Genre 228. — CALLITRICHE L.
(Du grec //callos//, beau, //thrix//, cheveu : plantes ayant l'aspect d'une belle chevelure.)

Fleurs monoïques ou polygames ; calice et corolle nuls, remplacés par 2 bractées membraneuses, atténuées aux 2 bouts, arquées en dedans, conniventes au sommet, manquant souvent ; 1 étamine, à filet allongé portant une anthère en rein; 2 styles, filiformes, papilleux dans presque toute leur longueur ; ovaire libre, supère ; fruit capsulaire, à 4 loges, à 4 coques plus ou moins carénées, monospermes, se séparant à la maturité.
Fleurs très petites, à peine visibles, solitaires à l'aisselle des feuilles ; feuilles opposées, étroitement connées à la base, très entières ou échancrées au sommet, sans stipules ; plantes aquatiques, pérennantes, grêles, allongées, molles, submergées ou flottantes.
Environ 10 espèces habitant les régions tempérées et froides de tout le globe.