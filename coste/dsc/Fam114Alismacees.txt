FAMILLE **114**. — **ALISMACÉES**.
Dessins de Mlle Kastner.

Fleurs hermaphrodites, rarement monoïques ou dioïques, régulières. Périanthe double, à 6 divisions libres ou à peine soudées à la base, les 3 extérieures persistantes ordinairement herbacées, les 3 intérieures plus grandes, pétaloïdes, ordinairement caduques ; étamines 6-9 ou indéfinies, insérées à la base ou autour du pistil ; anthères à 2 loges ; styles terminaux, à stigmate simple ; ovaire libre ; fruit formé de carpelles nombreux 
(6 au moins), libres ou soudés à la base, à une ou plusieurs graines.
Fleurs blanches ou rosées, presque toujours en verticilles ou en ombelle ; feuilles engainantes, toutes radicales ou fasciculées, entières ; herbes aquatiques ou des lieux marécageux, glabres.
Environ 55 espèces répandues dans les eaux douces de presque tout le globe.