FAMILLE **34**. — **FRAXINÉES**.
Dessins de M. DENISE.

Environ 20 espèces répandues dans les régions tempérées et subtropicales de l'hémisphère boréal.