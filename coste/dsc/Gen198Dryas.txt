Genre **198**. — **DRYAS** L.
(Du grec //dryas//, dryade, nymphe des bois et des pâturages.)

2 espèces habitant l'Europe, l'Asie, l'Amérique.