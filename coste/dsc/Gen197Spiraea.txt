Genre **197**. — **SPIRÆA** L. — //Spirée//.
(Du grec //speira//, spirale : allusion aux carpelles contournés de la Reine des prés.)

Calice persistant, à 5 lobes, sans calicule ; 5 pétales, rarement 6-8 ; étamines nombreuses ; 3 ou plusieurs styles, terminaux, marcescents ; ovaire libre et supère ; fruit sec, composé de 3-9 carpelles disposés en un seul verticille, indéhiscents ou s'ouvrant en dedans, renfermant chacun 2-5 graines.
Fleurs blanches, en panicules ou en corymbes terminaux ; feuilles simples ou composées, stipulées ou non ; plantes herbacées ou ligneuses, sans épines.
Environ 50 espèces habitant l'Europe, l'Asie, l'Amérique.