FAMILLE 43. — ONAGRARIÉES.
Dessins de M. DENISE.
Fleurs hermaphrodites, régulières ou presque régulières ; calice à tube soudé à l'ovaire, à 2, 4 ou 5 lobes caducs ou persistants ; pétales 2, 4 ou 5, rarement nuls ; 4, 8 ou 10 étamines, insérées avec les pétales au sommet du tube du calice ; 1 style filiforme, à 1, 4 ou 5 stigmates ; ovaire infère ; fruit capsulaire à 4 loges polyspermes ou à 1-2 loges monospermes.
Fleurs roses, blanches, jaunes ou verdâtres, en grappes terminales ou solitaires et
axillaires ; feuilles ordinairement simples, opposées ou alternes, souvent pourvues de stipules très petites ; plantes herbacées.
Environ 350 espèces répandues dans les régions tempérées et tropicales du globe.