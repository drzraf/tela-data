Genre **114**. — **MALOPE** L.
(Du grec //matos//, couvert de poils blancs : la plante est hispide.)

Trois espèces habitant la région méditerranéenne.