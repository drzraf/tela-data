Genre **677**. — **BDTOMUS** L. — //Butôme //.
(Du grec bom, bœuf, temn(j,]e coupe : les feuilles tranchantes font saigner la bouche des jjœufs qui en mangent.)

1 seule espèce.