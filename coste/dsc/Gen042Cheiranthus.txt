Genre **42**. — **CHEIRANTHUS** L. — //Giroflée//.
(Du grec //cheir//, main, //anthos//, fleur: fleur qu'on tient à la main pour respirer sa bonne odeur.)

Environ 12 espèces habitant l'Europe, l'Asie, l'Afrique et l'Amérique septentrionales.