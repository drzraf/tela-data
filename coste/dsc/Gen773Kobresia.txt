Genre **773**. — **KOBRESIA** Willd.
(Dédié à Kobi'os, botaniste allemand d'Ausbourg.)

Fleurs monoïques, en épillets androgyns ou unisexuels, chacun à 1-2 fleurs, la supérieure mâle, l'inférieure femelle ; écailles florales imbriquées tout autour, brunes bordées de blanc ; 3 étamines ; style filiforme caduc, à 3 stigmates ; ovaire nu, non renfermé dans un utricule ; akène trigone, mucroné par la base du style.
Epillets réunis en épi ou en panicule spiciforme terminale, chacun placé à l'aisselle d'une bractée scarieuse qui le dépasse ; feuilles raides, étroitement linéaires, canaliculées ; plantes vivaces gazonnantes.
S ou 6 espèces habitant les régions froides de l'hémisphère boréal.