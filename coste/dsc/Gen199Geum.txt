Genre **199**. — **GEUM** L. — //Benoîte//.
(Du grec //geuô//, j'assaisonne : allusion à l'odeur de clou de girofle qu'exhale la racine de la Benoîte.)

Calice persistant, à 10 lobes sur 2 rangs, les 5 extérieurs plus petits formant calicule ; 5 pétales, rarement 6-7, obovales ou en cœur renversé ; étamines nombreuses ; styles terminaux, accrescents après la floraison ; ovaire libre et supère ; fruit sec, composé de nombreux carpelles velus, monospermes, indéhiscents, terminés par une arête souvent plumeuse, réunis en tête globuleuse sur un réceptacle poilu, conique, persistant.
Fleurs jaunes, blanchâtres ou rougeâtres, solitaires ou en cymes terminales ; feuilles inférieures lyrées-pennatiséquées, à segments inégaux, le terminal plus grand ; plantes vivaces, velues, à souche épaisse.
Environ 30 espèces habitant les régions tempérées et froides de tout le globe.