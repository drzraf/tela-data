Famille 132. — RHIZOCARPEES
(Dessins de M. Denise.)

Fructifications (sporocarpes) globuleuses ou ovoïdes, coriaces ou membraneuses, à la fin déhiscentes, à une ou plusieurs loges contenant des sporanges ; ceux-ci de deux sortes : les uns plus gros (macrosporanges), les autres plus petits (microsporanges), renfermés dans un même sporocarpe ou dans des sporocarpes différents.
Fructifications naissant à la base des feuilles, sur les rhizomes ou entre les racines ; feuilles alternes ou toutes radicales, enroulées ou non dans leur jeunesse ; plantes aquatiques ou des lieux humides, vivaces ou annuelles.
Environ 130 espèces répandues dans presque tout le globe. Quelques-unes, flottantes à la surface des eaux, font l'ornement des étangs et des bassins.