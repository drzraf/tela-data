Genre **634**. — **HIPPOPHAE** L. — //Argousier//.
(Du grec hippos, cheval, pliaô, je tue : les fruits sont employés en décoction pour détruire la vermine des animaux.)

1 seule espèce.