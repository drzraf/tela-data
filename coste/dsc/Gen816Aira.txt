Genre **816**. — **AIRA** L. — //Candie//.
(Du grec aira, ivraie, dérivé de airô, jo fais mourir.)

Epillets comprimés par le côté, à 2 fleurs sessiles ou subsessiles ; glumes presque égales, membraneuses, carénées, uninervées, dépassant les fleurs ; glumelles presque égales, membraneuses, l'inférieure ponctuée-scabre, acuminée et bifide au sommet, portant sur le dos, vers le tiers inférieur, une arête droite ou genouillée non articulée et non épaissie au sommet, rarement entière et sans arête ; la supérieure bicarénée et émarginée ; 3 étamines ; stigmates latéraux ; caryopse glabre, oblong en fuseau, sillonné à la face interne, adhérent aux glumelles.
Epillets petits, pédicellés, en panicule étalée ou spiciforme, verte ou violacée ; feuilles courtes, enroulées-sétacées, à ligule allongée ; plantes annuelles, grêles, élégantes, glabres, à racine très courte.
Environ 8 espèces habitant l'Europe, l'Asie, l'Afrique. Quçlque§-une§ sorI cultivée^ comme ornement dans Içs parterreg ,