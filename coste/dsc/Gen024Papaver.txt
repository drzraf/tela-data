Genre **24**. — **PAPAVER** L. — //Pavot//.

Pétales chiffonnés dans le bouton, à onglet souvent taché de noir ; stigmates 4-15, sessiles et rayonnants sur un disque couronnant l'ovaire ; capsule renflée, arrondie ou oblongue, à 1 seule loge subdivisée en 4-15 compartiments par autant de cloisons, s'ouvrant sous le disque des stigmates par 4-15 trous.
Fleurs rouges, jaunes ou blanches, grandes, solitaires, penchées avant l'épanouissement ; feuilles 1, 2 ou 3 fois pennatipartites ou incisées-dentées ; herbes à suc laiteux.
Environ 26 espèces répandues dans les régions extratropicales de tout le globe.