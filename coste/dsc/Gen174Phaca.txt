Genre **174**. – **PHACA** L.
(Du grec //phacos//, lentille : allusion à la forme des graines.)

Calice tubuleux ou en cloche, à 5 dents courtes, presque égales ; étendard orbiculaire ou ovale, non redressé, égalant ou dépassant les ailes ; carène obtuse, non apiculée ; étamines diadelphes ; gousse saillante, étalée ou réfléchie, longuement stipitée, renflée en vessie membraneuse, à 1 loge sans cloison ou avec une demi-cloison formée par la suture inférieure repliée en dedans.
Fleurs jaunes, blanchâtres, bleuâtres ou violacées, en grappes axillaires pédonculées ; feuilles imparipennées, à folioles entières ; stipules libres ; plantes vivaces, herbacées, habitant les hautes montagnes.