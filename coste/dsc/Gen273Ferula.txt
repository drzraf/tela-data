Genre **273**. — **FERULA** L.
(Du latin //ferire//, frapper : allusion aux longues tiges servant de verges.)

Calice à 5 dents ; pétales ovales, acuminés, entiers, à pointe dressée ou courbée en dedans ; styles étalés ou réfléchis ; fruit ovale ou oblong, comprimé par le dos, glabre, entouré d'un rebord plan ; méricarpes à 5 côtes, les trois dorsales filiformes, égales, les deux marginales dilatées en aile aplanie ; vallécules à plusieurs bandelettes ; carpophore libre, bifide ; graine à face commissurale plane.
Fleurs jaunes, en ombelles à 5-40 rayons, les latérales plus petites que la centrale et souvent stériles ; involucre et involucelle nuls ou à plusieurs folioles ; feuilles décomposées en lanières linéaires ; plantes vivaces, glabres, robustes.
Environ 80 espèces habitant l'Europe méridionale, l'Asie occidentale, l'Afrique septentrionale.