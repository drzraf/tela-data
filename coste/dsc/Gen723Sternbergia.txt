Genre **723**. — **STERNBERGIA** Waldst. et Kit.
(Dédié à Sternberg.)

Environ 12 espèces habitant l'Europe et la région méditerranéenne.