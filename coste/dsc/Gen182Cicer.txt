Genre **182**. – **CICER** L. – //Cicérole//.
(Du grec //cicus//, force, vigueur: allusion aux qualités alimentaires des graines.)

7 espèces habitant la région méditerranéenne et l'Asie occidentale.