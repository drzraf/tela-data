Genre **718**. — **SISYRINCHIUM** L.
(Du grec sys, porc, rynchos, grouin: bulbes de quelques espèces recherchés des porcs.)

Environ oO espèces habitant l'Amérique, naturalisées en divers pays.