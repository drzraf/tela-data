FAMILLE 49. — CUCURBITACÉES.
Dessins de Mlle KASTNER.

Fleurs monoïques ou dioïques ; calice à tube soudé à l'ovaire, à 5 lobes ; corolle en cloche ou en roue, insérée au sommet du tube calicinal, à 5 lobes plus ou moins soudés entre eux et avec le calice ; 5 étamines triadelphes, 4 soudées 2 à 2, la 5ème libre ; 1 style court, 3-5 stigmates épais et bifides ; ovaire infère ; fruit charnu, à plusieurs graines comprimées.
Fleurs verdâtres ou jaunâtres, en fascicules ou solitaires, axillaires ; feuilles alternes, simples, palmatinervées, pétiolées, sans stipules ; plantes vivaces, herbacées, hérissées, grimpantes ou rampantes.
Environ 640 espèces, la plupart habitant les régions chaudes, surtout tropicales, des deux mondes.