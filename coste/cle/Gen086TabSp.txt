TABLEAU DES ESPÈCES

{1} Tiges ligneuses à la base ; fleurs axillaires, très peu nombreuses.
	Tiges longuement nues à la base ; feuilles coriaces, ovales ; fleurs grandes, jaunâtres ou rosées.	**P. Chamaebuxus 406**
	Tiges feuillées jusqu'à la base ; feuilles minces, linéaires‑lancéolées ; fleurs petites, blanchâtres.	**P. rupestris 407**
{1} Tiges entièrement herbacées ; fleurs en grappe terminale.
	{2} Plantes annuelles, à racine très grêle ; grappe égalant au moins la moitié de la tige.
		Fleurs assez grandes ; ailes à 3 nervures ramifiées ; feuilles lancéolées‑linéaires, aiguës.	**P. monspeliaca 408**
		Fleurs très petites ; ailes à 1 nervure non ramifiée ; feuilles supérieures linéaires, obtuses.	**P. exilis 409**
	{2} Plantes vivaces ou pérennantes ; grappe ordinairement plus courte que la moitié de la tige.
		{3} Tiges sans rosettes de feuilles ; feuilles inférieures plus petites que celles de la tige.
			Feuilles inférieures et des rameaux stériles opposées ; grappes paraissant latérales ; bractées plus courtes que le pédicelle.	**P. serpyllacea 410**
			Feuilles toutes alternes ou éparses ; grappes terminales ; bractée moyenne égalant ou dépassant le pédicelle.	**P. vulgaris 411**
		{3} Tiges munies de rosettes de feuilles ; feuilles inférieures plus grandes que celles de la tige.
			Rosettes à axe central terminé par une pousse stérile ; grappes serrées, courtes, ainsi que les rameaux.	**P. alpina 412**
			Rosettes sans pousse stérile au centre ; grappes lâches, allongées, ainsi que les rameaux.
				Bractées plus courtes que le pédicelle ; capsules petites, presque sessiles ; plante à saveur amère.	**P. amara 413**
				Bractée moyenne dépassant le pédicelle ; capsules assez grandes, pédicellées ; plante à saveur herbacée.	**P. calcarea 414**