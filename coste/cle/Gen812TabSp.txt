TABLEAU DES ESPÈCES

Arêtes fortement plumeuses dans les deux tiers supérieurs, très allongées (15-30 cm.), flexueuses et arquées en dehors ; épillets d'un vert jaunâtre, longs de 4-6 cm.	**S. pennata 4022**
Arêtes glabres ou pubescentcs, jamais plumeuses, dressées ou flexueuses -recourbées ; épillets verts ou blanchâtres, longs de 1-2 cm.
Ligule allongée, lancéolée ; panicule lâche.
Arêtes glabres, longues de 10-15 cm., irrégulièrement contournées et recourbées en dessus ; glumes atténuées en pointe plus courte qu'elles ; panicule engaînée à la base par la feuille supérieure dilatée	**S. capillata 4023**
Arêtes pubescentes, longues de 8-10 cm., droites dans la partie supérieure ; glumes atténuées en pointe aussi longue ou plus longue qu'elles ; panicule presque toujours éloignée de la feuille supérieure	**S. juncea 4024**
Ligule très courte ; panicule serrée ou très étroite, spiciforme.
Arêtes longues de 5-10 cm., bigenouillées, tordues et velues jusqu'au deuxième genou ; glumes atténuées en pointe très fine ; épillets longs d'environ 2 cm. ; panicule épaissie, dense ; plante annuelle, à racine grêle .	**S. tortilis 4025**
Arêtes longues d'environ 15 mm., droites et glabres de la base au sommet ; glumes simplement mucronées ; épillets longs d'environ 1 cm. ; panicule très étroite, peu serrée ; plante vivace, à souche épaisse	**S. Aristella 4026**