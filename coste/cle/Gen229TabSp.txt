TABLEAU DES ESPÈCES

Fruit muni de 3 épines, 2 latérales recourbées, la 3ème terminale au moins aussi longue que lui ; feuilles 1-2 fois dichotomes, à segments linéaires, un peu élargis.	**C. demersum 1313**
Fruit muni d'une seule épine, terminale, beaucoup plus courte que lui, sans épines latérales ; feuilles 3-4 fois dichotomes, à segments capillaires.	**C. submersum 1314**