TABLEAU DES ESPÈCES

4» Plantes annuelles, à racine fibreuse, à feuilles assez larges et planes ; épillets à la fin pendants ou horizontaux, et très ouverts ; glumes à 5-H nervures.
Glumelle inférieure couverte jusqu'au milieu de longs poils soyeux ; fleurs, au moins l'inférieure, articulées sur l'axe de l'épillet dont elles se détachent facilement en laissant une cicatrice ; glumes dépassant les fleurs.
Il Glumelle inférieure couverte de poils blancs, bifide et terminée par 2 longues soies ; fleurs toutes articulées et aristées, laissant sur le rachis velu une cicatrice linéaire-oblongue ; panicule presque toujours unilatérale .	**A. barbata 4032**
Il Glumelle inférieure couverte de poils roussâtres ou bruns, bidentée ou brièvement bifide ; fleurs articulées laissant après leur chute une cicatrice arrondie ou obovale ; panicule étalée en tous sens, au moins à la floraison.
Fleurs toutes articulées et aristées ; axe de l'épillet velu jusqu'au sommet ; glumelle inférieure bidentée, à arête dorsale 1 fois environ plus longue que les glumes ; panicule pyramidale très étalée.	**A. fatua 4053**
Fleur inférieure seule articulée, les supérieures mutiques et glabres ; axe glabre, sauf à la base de la fleur inférieure ; glumelle inférieure brièvement bifide, à arête dorsale environ 2 fois plus longue que les glumes ; panicule à la fin unilatérale	**A. sterilis 4034**
Glumelle inférieure glabre ou glabrcscente ; fleurs non articulées sur l'axe de l'épillet, ne s'en détachant que par la fracture de l'axe lui-même qui est glabre.
Glumes dépassant sensiblement les fleurs ; fleur inférieure sessile ou subsessile ; glumelles presque égales, l'inférieure coriace à nervures peu distinctes dans la partie inférieure.
Panicule pyramidale, étalée en tous sens, très lâche, à rameaux allongés ; épUlets longs d'environ 20 mm. ; glumelle inférieure mutique ou à arête dorsale genouillée, tordue au-dessous du genou.	**A. sativa 4035**
Panicule allongée, étroite resserrée, assez dense et unilatérale, à rameaux courts ; épillets atteignant 23 mm. ; glumelle inférieure à arête dorsale droite ou arquée-flexueuse, uon tordue	**A. orientalis 4036**
■^ Glumes plus courtes que les fleurs ou les égalant ; fleur inférieure subsessile ou pédicellée ; glumelles inégales, l'inférieure à nervures bien marquées dès la base.
" Panicule assez grande, étalée en tous sens ; épillets terminaux à 3-4 fleurs fertiles, l'inférieure subsessile, les supérieures mutiques ; glumes plus courtes que les fleurs ; glumelle inférieure herbacée ; caryopse se détachant des glumelles à la maturité	**A. nuda 4037**
Panicule peu fournie, presque unilatérale ; épillets à 2 fleurs fertiles, l'une et l'autre pédicellées et ordinairement aristées ; glumes égalant les fleurs ; glumelle inférieure à la fin coriace ; caryopse restant enveloppé dans les glumelles.
Epillets longs de 18-20 mm., horizontaux ou pendants ; glumes lancéoléesacuminées ; glumelle inférieure fendue jusqu'au tiers en 2 lobes longuement aristés	**A. strigosa 4038**
Epillets courts, de 12 mm. environ, subhorizontaux ou dressés ; glumes ovales-lancéolées ; glumelle inférieure obtuse, échancrée et terminée par 2 courts mucrons	**A. brevis 4039**
4> Plantes vivaces, à souche gazonnante, à feuilles souvent étroites ou enroulées ; épillets dressés, presque fermés ; glumes à 1-3 nervures.
X Ligule très courte, tronquée ou poilue ; feuilles ordinairement enroulées, à faces dissemblables, la supérieure sillonnée-scabre, l'inférieure lisse, sans bordure marginale. iji Plante à feuilles, gaines et nœuds de la tige mollement velus ; épillets ovoïdes, longs seulement de 3-6 mm., à 2 fleurs fertiles, la supérieure subsessile et mutique, à poils de la base 10 fois plus courts qu'elle ; glumelle inférieure aristée vers le tiers supérieur A. longifolia 4060 i§4 Plantes glabres ou un peu poilues sur les feuilles ou les gaines ; épillets oblongs, ayant plus de mm. de long, à 2-3 fleurs fertiles toutes liédicellées, à poils de la base égalant au moins leur quart ; glumelle aristée vers le milieu du dos.
Feuilles enroulées-filiformes ; poils de la base de chaque fleur égalant ou dépassant la moitié de la glumelle.
Ligule réduite à une rangée de poils ; panicule longue de 12-20 cm., à épillets d'environ 13 mm. ; plante haute de 30 cm. à 1 mètre, munie de gaines rougeâtres à la base, à feuilles dures jonciformes.	**A. filifolia 4061**
Ligule très courte, denticulée ; paniculc longue seulement de 4-8 cm., à épillets de 8-10 mm . ; plante grêle de 20-60 cm . , à gaines de la base blanchâtres, a feuilles fines sétacées	**A. setacea 4062**
Feuilles enroulées ou planes ; poils de la base de chaque fleur n'égalant pas la moitié de la glumelle.
Feuilles planes puis pliées et enroulées, assez courtes, vertes ; épillets à 
2-3 fleurs fertiles, toutes articulées, caduques et aristées ; plante de 
30-00 cm., à tiges plus ou moins coudées à la base,	**A. montana 4063**
Feuilles enroulées-filiformes, allongées, glauques ; épillets à 2 fleurs fertiles, non articulées, persistantes, l'inférieure seule aristée ; plante haute de 
00 cm. à 1 m. SO, à tiges dressées dès la base.	**A. sempervirens 4004**
X Ligule allongée, oblongue ou lancéolée ; feuilles planes ou lâchement enroulées, à faces ordinairement semblables et à bordure marginale blanchâtre.
-j- Feuilles lâchement enroulées-sétacées, longues, sans bordure marginale ; épillets longs d'environ 1 cm., à 2-3 fleurs, l'inférieure seule articulée et caduque, les autres s'arrachant difficilement et persistantes.	**A. Parlatorei 4063**
-1- Feuilles planes, pliées ou plus ou moins tordues-enroulées, munies d'une bordure marginale blanchâtre ; épillets longs de plus de 1 cm., à 2-8 fleurs toutes articulées et "caduques.
Glumelle inférieure bifide, à lobes terminés en pointe fine ; poils de la base de chaque fleur très courts, dépassant peu la base de la glumelle ; feuilles glabres, lisses sur les 2 faces.
Panicule courte {3-Gcm.). ovale, dense, panachée de violet, de vert et de jaune ; glumelle faiblement nervée sur le dos ; plante des hautes montagnes, à tiges de 13-40 cm,, grêles, cylindriques.	**A. versicolor 4066**
Panicule allongée (6-13 cm.) assez lâche, blanchâtre ou violacée ; glumelle à 3-7 nervures saillantes et fortement sillonnée ; plante des landes occidentales,! haute de 40 cm. à 1 mètre, à tiges comprimées à la base.	**A. sulcata 4067**
Glumelle inférieure tronquée-laciniée ou dentelée au sommet, non terminée par 2 pointes fines.
^' Feuilles et gaines inférieures presque toujours poilues ; panicule oblongue, à rameaux inférieurs réunis par 3-5 ; épillets à 2-3 fleurs aristées, à poils égalant la moitié de la glumelle ; glumes égalant à peu près les fleurs	**A. pubescens 4068**
^- Feuilles et gaines glabres ; panicule très étroite, presque simple, à rameaux solitaires ou géminés, rarement ternes ; épillets à 3-8 fleurs aristées, à poils au moins 10 fois plus courts que la glumelle ; glumes plus courtes que les fleurs.
Feuilles planes ou un peu pliées, scabres en dessus et sur les gaines, les inférieures souvent allongées ; panicule de 10-20 cm., panachée de violet ; épillets atténués aux deux bouts ; glumelle inférieure munie vers le milieu du dos d'une arête dressée-étalée.	**A. pratensis 4009**
Feuilles étroites, plissées ou tordues-enroulées, lisses sur les faces et les gaines, souvent très courtes ; panicule de 0-13 cm., blanchâtre, rarement violacée ; épillets plus larges au sommet ; glumelle munie un peu au-dessus du milieu d'une arête élalée-divari([uée.	**A. bromoides 4070**