TABLEAU DES GENRES

X Souche non bulbeuse, à racines fibreuses ou renflées-tuberculeuses.
-\- Fleurs grandes, longues de 4 à 10 cm., en large entonnoir rétréci en tube à la base ; étamines et style arqués-ascendants.
Fleurs fauve rougeâtre ou jaunes, longues de 6-10 cm., à divisions soudées à la base, à nervures nombreuses ; étamines insérées sur le périanthe ; feuilles larges de b-20 mm	**HEMEROCALLIS 700**
Fleurs d'un blanc pur, longues de 4-5 cm., à divisions libres jusqu' à la base, à 3 nervures longitudinales ; étamines insérées sur le réceptacle ; feuilles étroites (1-4 mm.)	**PARADISIA 701**
-\- Fleurs moins grandes, longues de 1-2 cm., étalées en étoile ou rétrécies en tube très court à la base.
Fleurs blanches ou roses, à pédicelles nettement articulés ; racine composée d'un faisceau de fibres ou de tubercules charnus.
Périanthe à divisions marquées de 3-5 nervures longitudinales ; étamines à filets droits, non dilatés à la base ; plantes peu robustes, à feuilles étroites,	**ANTHERICUM 702**
Périanthe à divisions munies sur le dos d'une seule nervure colorée ; étamines à filets arqués, à base dilatée concave recouvrant l'ovaire ; plantes généralement robustes et à feuilles élargies . . .	**ASPHODELUS 703**
Fleurs jaunes ou bleues, à divisions uninervées, à pédicelles non articulés ; souche rampante ou épaisse, émettant des fibres filiformes.
Fleurs jaunâtres, en grappe munie de bractées linéaires ; étamines égales, à filets très velus ; capsule conique saillante ; tige munie de bractées ou de feuilles ; feuilles linéaires-graminoïdes. . . .	**NARTHECIUM 704**
Fleurs bleues, 1-3 en petite tête entourée d'écaillés roussâtres ; étamines inégales, à filets glabres ; capsule en toupie, incluse ; tige nue, en forme de jonc ; feuille^ réduites à quelques gaines radicales.	**APHYLLANTHES 705**
X Souche renflée en bulbe tunique ou écailleux, simple ou entouré de caïeux ou de bulbilles.
»ï< Fleurs disposées en ombelle ou en tête arrondie, enveloppée avant la floraison par une spathe membraneuse à 1-2 valves ; plantes exhalant la plupart par le froissement une odeur piquante ou alliacée ÂLLIUM 699
IÎ1 Fleurs en grappe, en corymbe, ou solitaires et terminales, jamais enveloppées dans une spaUie ; plantes inodores ou à odeur non alliacée.
4J> Périanthe à divisions soudées en tube au moins dans le quart inférieur ; étamines insérées sur le tube du périanthe ; feuilles toutes radicales.
Capsule nettement trlgone, à coques comprimées-aiguës ; périanthe caduc, bleu ou violacé-roussâtre ; fleurs en grappe mulliflore.
Périanthe resserré à la gorge et urcéolé, à 6 dents très courtes étalées-réfléchics ; étamines incluses, à filets courts et filiformes.	**MUSCARI 698**
Périanthe tubuleux en cloche, divisé au moins jusqu'au quart en 6 lobes dressés ou dressés-étalés ; étamines presque égales au périanthe, à filets dilatés à la base	**BELLEVALIA 697**
Capsule obscurément trigone, à coques arrondies ; périanthe marcescent sur le fruit ; fleurs en grappe pauciflore.
Fleurs bleues, roses ou blanches ; périanthe tubuleux en cloche, renflé sur l'ovaire, à G lobes étalés ou recourbés ; graines subglobuleuses, caronculées	**HYACINTHUS 696**
Fleurs fauves ou jaune orangé ; périanthe à tube un peu renflé, à divisions intérieures dressées-conniventes, les 3 extérieures étalées-recourbées ; graines planes, sans caroncule	**UROPETALUM 695**
Il Périanthe à divisions libres ou soudées seulement à la base ; étamines insérées à la base des pétales ou sur le réceptacle.
5^ Feuilles toutes radicales ; tige nue, terminée par une grappe ou un corymbe DES ou munis de bractées membraneuses ; graines noires.
1^ Fleurs bleues, violettes ou roses, rarement blanches ; graines subglobuleuses.
Périanthe tubuleux en cloche, à divisions étalées ou recourbées ; étamines extérieures ou toutes soudées aux pétales dans leur moitié inférieure	**ENDYMION 694**
Périanthe à divisions étalées en étoile pendant la floraison ; étamines libres, à peine adhérentes à la base des pétales. . .	**SCILLA 693**
Fleurs blanches, jaunâtres, verdâtres, rarement rosées.
Fleurs en grappe spiciforme ; étamines égalant presque le périanthe, à filets filiformes ; graines planes ; feuilles naissant après les fleurs.	**URGINEA 692**
Fleurs en grappe ou en corymbe ; étamines plus courtes que le périanthe, à filets dilatés à la base ; graines subglobuleuses ; feuilles naissant avec les fleurs	**ORNITHOGALUM 691**
Feuilles caulinaires rares ou nombreuses ; tige feuillée au moins à la base ou sous l'inflorescence ; graines ordinairement paies ou fauves.
4> Fleurs petites, longues de 1-2 cm., dressées ; périanthe à divisions planes étalées en étoile pendant la floraison.
Fleurs jaunes en dedans, verdâtres en dehors, solitaires ou en corymbe entouré de feuilles florales ; graines subglobuleuses ; 1-3 bulbes ovoïdes	**GAGEA 690**
Fleurs blanches striées de rose, solitaires au sommet de la tige ; graines triangulaires-comprimées ; bulbe oblong-cylindrique à peine renflé.	**LLOTDIA 689**
4i Fleurs grandes, longues de plus de 2 cm., souvent penchées ; périanthe en cloche ou à divisions réfléchies en dehors.
= Feuilles peu nombreuses, 2-5, placées vers la base de la tige ; fleurs solitaires, à divisions dépourvues de nectaires ; anthères dressées, fixées au filet par leur base creuse.
Fleur penchée, à divisions réfléchies en arrière ; filets en fuseau ; style filiforme, stigmate trifide ; capsule réfléchie ; 2 feuilles opposées etpétiolées	**ERYTHRONIUM 688**
Fleur dressée, à divisions droites ; filets dilatés à la l);iso ; style nul, stigmate trigone ; capsule dressée ; 2-5 feuilles alternes et sessiles.	**TULIPA 685**
= Feuilles nombreuses occupant toute la tige ou son sommet ; fleurs à divisions munies à la base d'une fossette ou d'un sillon nectarifère ; anthères fixées au filet par le dos.
Fleurs solitaires ou géminées, penchées, brunes panachées en damier ; stigmate trifide ; bulbe petit (1 cm.), couvert d'une tunique blanchâtre ; 3-10 feuilles	**FRITILLARIA 686**
Fleurs solitaires ou en grappe lâche, dressées ou penchées, rouges, jaunes ou blanches ; stigmate trigone ; bulbe gros, écailleux, blanchâtre ou jaunâtre ; feuilles très nombreuses.	**LILIUM 687**