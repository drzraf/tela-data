TABLEAU DES ESPÈCES

Fleurs jaunes ; gousses arquées, non étranglées aux articulations.
	Fleurs non entourées de bractées ; calice à dents 4-6 fois plus courtes que le tube ; gousses peu comprimées, glabres, finement ridées ; feuilles toutes pétiolées, à 3-6 paires de folioles glabres et vertes.	**O. ebracteatus 1066**
	Fleurs entourées et dépassées par une feuille bractéale ; calice à dents 1-2 fois plus courtes que le tube ; gousses très comprimées, pubescentes, fortement striées-ridées ; feuilles supérieures sessiles, toutes à 6-15 paires de folioles pubescentes-blanchâtres.	**O. compressus 1067**
Fleurs rosées ou blanchâtres ; gousses droites ou peu arquées, contractées et comme étranglées aux articulations,
	Fleurs roses de 6-7 mm. de long, sur des pédoncules plus longs que la feuille ; feuille bractéale petite, égalant à peine les calices ; calices à dents aussi longues que le tube ; gousses glabres, lustrées, d'un brun fauve à la maturité.	**O. roseus 1068**
	Fleurs d'un blanc mêlé de rose et de jaune, de 4-5 mm. à pédoncules plus courts ou un peu plus longs que la feuille ; feuille bractéale dépassant un peu les fleurs ; calice à dents 2-3 fois plus courtes que le tube ; gousses pubescentes, à la fin noirâtres.	**O. perpusillus 1069**