TABLEAU DES ESPÈCES

{1} Feuilles, bractées et calices plus ou moins couverts de poils glanduleux.
	{2} Corolle longue de 10-16 mm., accrescente, à tube dépassant notablement le calice, blanche striée de violet, à lobes de la lèvre supérieure émarginés ; feuilles et bractées à dents obtuses ou aiguës, non aristées.		E. **officinalis 2750**
	{2} Corolle longue de 5-10 mm., non accrescente, à tube inclus dans le calice, à lobes de la lèvre supérieure denticulés.
		Corolle blanche^ striée de violet, à gorge jaunâtre ; grappe s'allongeant peu, restant condensée ; calice à lobes ovales-aigus, presque égaux au tube ; dents des feuilles et des bractées non aristées ; poils glanduleux longuement stipités.			E. **hirtella 2751**
		Corolle violacée ou bleuâtre, tachée de jaune sur la lèvre inférieure ; grappe s'allongeant beaucoup ; calice à lobes lancéolés, aussi longs que le tube ; dents des feuilles et des bractées aristées ; poils glanduleux brièvement stipités.			E. **brevipila 2752**
{1} Feuilles, bractées et calices glabres ou munis de poils presque toujours non glanduleux.
	{2} Feuilles lancéolées, plus de 2 fois plus longues que larges, à dents longuement aristées et séparées sur les côtés par un espace trilatéral ; capsule glabre ou ciliée au sommet de courts poils infléchis ; corolle variable.		E. **salisburgensis 2753**
	{2} Feuilles ovales ou oblongues, moins de 2 fois plus longues que larges, à dents latérales non séparées par un espace trilatéral ; capsule poilue ou glabre, ciliée au bord de poils droits.
		{3} Corolle longue de 7-10 mm. ou plus ; capsule ne dépassant pas les dents du calice.
			Corolle à la fin longue de 10-15 mm., accrescente, à lèvre inférieure bien plus longue et dirigée en avant ; grappe peu allongée ; dents des bractées longuement aristées, les supérieures formant pinceau.				E. **alpina 2754**
			Corolle longue de 7-10 cm., peu ou point accrescente, à lèvre inférieure plus ou moins déjetée en bas ; grappe à la fin très allongée ; dents des bractées aristées, les supérieures non réunies en pinceau.
				Bractées imbriquées, en coin à la base, plus larges vers le milieu ; calice pubescent, très accrescent, à la fin long de 7-8 mm. ; capsule longue de 6-7 mm. ; tige simple ou à 1-2 rameaux courts et dressés.					E. **pectinata 2755**
				Bractées non imbriquées, ovales en coin, plus larges vers le tiers inférieur ; calice glabre ou glabrescent, non accrescent, long de 5 mm. ; capsule longue de 5 mm. ; tige munie intérieurement de rameaux ascendants.					E. **stricta 2756**
		{3} Corolle longue seulement de 3-6 mm. ; capsule dépassant ordinairement le calice.
			Tige rameuse, à rameaux ascendants ou dressés ; feuilles et bractées glabres, non brillantes, à nervures saillantes, à dents aiguës ; bractées étalées ou arquées ; corolle bleuâtre ou blanchâtre.				E. **nemorosa 2757**
			Tige grêle, à rameaux grêles, raides, dressés ; feuilles et bractées petites, luisantes, glabres, à nervures non saillantes, à dents aiguës ; bractées dressées ; corolle violacée ou blanche.				E. **gracilis 2758**
			Tige simple ou à rameaux peu nombreux ; feuilles et bractées plus ou moins couvertes de soies fines, à dents obtuses ou aiguës ; corolle ordinairement jaune.				E. **minima 2759**