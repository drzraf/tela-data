TABLEAU DES ESPÈCES

4^ Inflorescence en panicule allongée, lobée, étalée pendanl la floraison ; épillets convexes sur les deux faces ; glumes à carène non ailée ; plante robuste de 80 cm. à 1 mètre 50, à feuilles larges de 1-2 cm., à souche traçante 
(Gen. Baldingera)	**P. arundinacea 3919**
Inflorescence en panicule spiciforme, dense, toujours resserrée ; épillets un peu concaves sur la face interne ; glumes à carène ailée ; plantes moins robustes, à feuilles plus étroites, à souche jamais traçante.
4i Plantes vivaces, à souche épaisse ; gaîne supérieure faiblement renflée.
Souche courte fibreuse, non tuberculeuse ; feuille supérieure à limbe court ou nul ; glumes obovales, à aile large entière et obliquement tronquée au sommet ; glumelles velues, munies à la base de 2 écailles dix fois plus courtes que la fleur	**P. truncata 3920**
Souche oblique, noueuse-tuberculeuse ; feuille supérieure mimie d'un limbe développé ; glumes lancéolées, acuminées ou arislées ; écailles de la base de la fleur moins courtes.
Glumes aristées, à aile large et érodée-dentée ; glumelle inférieure glabre, n'égalant pas la moitié des glumes, munie à la base de 2 écailles six fois plus courtes qu'elle ; pédicelles un peu plus courts que 1 epillet.	**P. caerulescens 3921**
Glumes aiguës, à aile étroite entière ou brièvement denticulée ; glumelle inférieure velue, égalant au moins la moitié des glumes, munie à la base d'une écaille atteignant son tiers ; pédicelles bien plus courts que l'épillet	**P. nodosa 3922**
4. Plantes annuelles, à racine fibreuse ; gaîne supérieure sensiblement renflée.
Panicule spiciforme oblongue ; pédicelles égalant au moins le quart de l'épillet ; glumes lancéolées, acuminées ou aristées.
Tige nue au sommet, la gaîne supérieure étant toujours éloignée de l'épi ; celui-ci tronqué ou peu atténué à la base ; pédicelles bien plus courts que l'épillet ; glumes aiguës, à carène large et érodée-dentée.	**P. miner 3923**
Tige feuillée jusqu'au sommet, la gaîne supérieure embrassant la base de l'épi ; celui-ci très atténué à la base ; pédicelles aussi longs que l'épillet ; glumes aristées, à aile entière prolongée en pointe .	**P. paradoxa 3924**
Panicule spiciforme courte, ovoïde ou ovoïde-oblongue ; pédicelles presque nuls ; glumes obovales, brièvement mucronées, à aile large presque entière.
Glumelle inférieure munie à la base de 2 écailles très petites, environ 6 fois plus courtes qu'elle ; tige assez longuement nue au sommet ; gaîne supérieure médiocrement renflée P. brachystachys 392S
Glumelle inférieure munie à la base de 2 écailles linéaires-lancéolées, égalant la moitié de sa longueur ; tige souvent brièvement nue au sommet ; gaîne supérieure plus fortement renflée	**P. canariensis 3926**