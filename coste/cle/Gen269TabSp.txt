TABLEAU DES ESPÈCES

Folioles larges, ovales-lancéolées, non ou rarement atténuées à la base ; pétales à pointe dressée ; fruit à ailes marginales plus larges que le méricarpe, plus ou moins ondulées.	**A. silvestris 1499**
Folioles lancéolées, plus ou moins atténuées ou en coin à la base ; pétales à pointe courbée en dedans ; fruit à ailes marginales épaisses, plus étroites que le méricarpe.	**A. heterocarpa 1500**
Folioles lancéolées, rapprochées et décurrentes à la base ; pétales à pointe roulée en dedans ; fruit à ailes marginales aussi larges que le péricarpe.	**A. Razulii 1501**