TABLEAU DES ESPÈCES

Feuilles supérieures ovales, dentées ou incisées ; siliques terminées en pointe longue et effilée.	**B. vulgaris 194**
Feuilles supérieures pennatipartites, à lobe terminal plus grand ; siliques terminées en pointe courte.
	Siliques courtes, de 2-3 centimètres.
		Pédicelles grêles, très étalés, 2-3 fois plus courts que les siliques étalées ; style filiforme.			B. **sicula 195**
		Pédicelles épais, dressés-appliqués, 5-7 fois plus courts que les siliques dressées ; style épais.			B. **intermedia 196**
	Siliques longues, de 4-6 centimètres.
		Pédicelles plus longs que les sépales ; lobe terminal des feuilles ovale en cœur.			B. **rupicola 197**
		Pédicelles un peu plus courts que les sépales ; lobe terminal des feuilles ovale-oblong.			B. **præcox 198**