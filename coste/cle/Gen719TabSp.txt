TABLEAU DES ESPÈCES

Souche bulbeuse tuniquée, ou formée de 2-4 tubercules oblongsnus ; feuilles étroites, linéaires-canaliculées ou quadrangulaires.
4» Tige peu robuste, longue seulement de 10-25 cm. ; feuilles peu nombreuses, bien plus longues que la tige ; fleurs assez petites à tube grêle.
Souche renflée en bulbe globuleux enveloppé de tuniques fibreuses ; 2 feuilles canaliculées ; 2-4 fleurs bleues dans chaque spathe ; tube du périanthe plus long que l'ovaire subsessile ; étamines et pistil soudés en colonne ; capsule oblongue, à 3 loges I. Sisyrinchium .3310
Souche pourvue de 2-4 tubercules oblongs et DES ; 3-4 feuilles tétragones ; fleur solitaire, d'un brun verdâtre ; tube du périanthe bien plus court que l'ovaire longuement pédicellé ; étamines et pistil libres ; capsule elliptique, à 1 loge.
I. tuberosa 3S17
4» Tige robuste, longue de 30-60 cm, ; feuilles nombreuses, canaliculées, plus courtes ou un peu plus longues que la tige ; bulbe ovoïde tunique ; fleurs grandes, à tube presque nul.
Fleur violacée, solitaire dans une gaine peu renflée à valves très inégales ; périanthe à divisions de même longueur, les extérieures dépassant peu les stigmates, à limbe bien plus court que l'onglet ; feuilles plus courtes que la tige.	**I. Xiphium 3318**
Fleurs bleu vif, 2 dans une gaine très renflée à valves presque égales ; périanthe à divisions inégales, les extérieures du double plus longues que les stigmates, à limbe large égalant l'onglet ; feuilles souvent plus longues que la tige.	**I. xiphioides 3519**
Souche horizontale, charnue, noueuse, rameuse ; feuilles en glaive ou linéairesallongées, non canaliculées.
X Périanthe à tube plus court que l'ovaire, à divisions extérieures non barbues en dessus.
= Fleurs violettes ou veinées de bleu ; feuilles linéaires-graminoïdes très allongées.
Feuilles plus longues que la tige comprimée à 2 tranchants ; souche assez petite ; spathe à valve inférieure herbacée et allongée ; divisions intérieures du i)érianthe plus courtes que les stigmates.	**I. graminea 3520**
Feuilles plus courtes que la tige cylindrique ; souche de la grosseur du doigt ; spathe à valves scarieuses aux bords ; divisions intérieures du périanthe dépassant les stigmates.
Divisions extérieures du périanthe plus longues que les intérieures, à limbe 
2 fois plus court que l'onglet ; stigmates à 2 lobes aigus courbés en dehors: capsule conique-hexagone, acuminée. . . .	**I. spuria 3521**
Divisions extérieures du périanthe égalante peu près les intérieures, à limbe un peu plus long que l'onglet ; stigmates à 2 lobes courts obtus denticulés ; capsule oblongue-trigone, à peine apiculée.	**I. sibirica 3522**
= Fleurs entièrement jaunes ou à divisions extérieures bleu-grisâtre ; feuilles élargies en glaive.
Divisions extérieures du périanthe bleuâtres, les intérieures un peu plus courtes, égalant ou dépassant les stigmates ; capsule atténuée mais non apiculée ; tige simple ; plante des bois, fétide par le froissement.	**I. fœtidissima 3523**
Divisions du périanthe toutes jaunes, les intérieures 3 fois plus petites que les extérieures, plus étroites et plus courtes que les stigmates ; capsule apiculée au sommet ; tige rameuse ; plante aquatique, inodore.	**I. Pseudacorus 3524**
X Périanthe à tube aussi long ou plus long que l'ovaire, à divisions extérieures munies en dessus d'une ligne barbue.
-{- Tige de 5-30 cm., terminée par 1-2 fleurs violettes ou jaunes à divisions larges de 2 3 cm. ; souche à peine de l'épaisseur du doigt.
Tige courte (5-15 cm.), égalant à peine ou dépassant peu les feuilles ; feuilles étroites, larges seulement de G-12 mm. ; souche petite, moins épaisse que le doigt ; divisions intérieures du périanthe généralement plus larges que les extérieures	**I. Chamaeiris 3525**
Tige haute de 10-30 cm., dépassant sensiblement les feuilles ; feuilles atteignant 15-22 mm. de large ; souche de la grosseur du doigt ; divisions du périanthe toutes à peu près de même largeur . .	**I. lutescens 3520**
-\- Tige de 30 cm. à 1 mètre, dépassant longuement les feuilles, portant 2 ou plusieurs fleurs à divisions larges de 4-3 cm. ; souche très épaisse (environ 
3 cm.).
Fleurs d'un bleu pâle et clair ; spathe à valves entièrement Manchesscarieuses ; stigmates aussi larges que l'onglet des divisions extérieures ; plante égalant ou dépassant souvent un mètre . ...	**I. pallida 3527**
Fleurs d'un bleu violet vif ou blanches ; spathe à valves herbacées à la base ; stigmates un peu moins larges que l'onglet des divisions extérieures ; plantes moins élevées.
Fleurs blanches ; divisions du périanlhe à limbe beaucoup jjlus long que large, à tube égalant ordinairement l'ovaire ; stigmates plus larges vers leur milieu, à lobes non divariqués	**I. florentina 3528**
Fleurs d'un bleu violet ; divisions du périanthe à limbe presque aussi large que long, à tube ordinairement plus long que l'ovaire ; stigmates plus larges vers leur sommet, à lobes divariqués. . .	**I. germanica 3529**