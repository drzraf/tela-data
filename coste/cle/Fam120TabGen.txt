TABLEAU DES GENRES

Périanthe non prolongé en tube au-dessus de l'ovaire, à divisions presque libres ; étamines insérées sur le disque qui surmonte l'ovaire, à anthères fixées par la base ; fleurs blanches ou rosées, penchées.
Périanthe à divisions de forme différente et inégales, les 3 intérieures de moitié plus courtes, conniventes en cloche, échancrées ; anthères terminées en pointe qiguë, s'ouvrant par 2 pores terminaux ; 2 feuilles. . . .	**GALANTHUS 721**
Périanthe à divisions de même forme et à peu près égales, ouvertes en cloche, entières et épaissies au sommet ; anthères à sommet obtus, s'ouvrant par 2 lignes longitudinales ; 3-G feuilles.'	**LEUCOIUM 722**
Périanthe prolongé au-dessus de l'ovaire en un tube plus on moins long, à divisions soudées intérieurement ; étamines insérées sur le périanthe, à anthères fixées le plus souvent par le milieu du dos ; fleurs blanches ou jaunes.
Périanthe en entonnoir, dépourvu de couronne pétaloïde à l'entrée de la gorge, à étamines inégales et insérées au sommet du tube ; fleur jaune, dressée, solitaire, naissant en automne	**STERNBERGIA 723**
Périanthe muni à l'entrée de la gorge d'une couronne ou d'un tube pétaloïde ; fleurs blanches ou jaunes, dressées ou penchées, naissant le plus souvent au printemps.
Etamines non ou peu saillantes, insérées sur le tube du périanthe au-dessous de la couronne ; celle-ci placée entre le périanthe et les étamines, entière, crénelée ou incisée-dentée ; spathe univalve ; fleurs solitaires ou en ombelle.	**NARCISSUS 724**
Etamines très saillantes, insérées en dedans sur la couronne ; celle-ci formée par les bases dilatées et soudées des filets, divisée en 8-12 dents ou segments ; spathe bivalve ; fleurs grandes, blanches, en ombelle multiflore.	**PANCRATIUM 725**