TABLEAU DES ESPÈCES

Tiges feuillées jusque vers le milieu, portant 3-4 petites feuilles ; feuilles radicales allongées, linéaires-acuminées, à 3-7 nervures ; fleurs jaunâtres, en grappe longue de 1-3 cm. ; bractée aussi longue que le pédicelle ; étamines saillantes.	**T. calyculata 3338**
Tiges presque entièrement nues, portant 1-2 feuilles à la base ; feuilles courtes, lancéolées-aiguës, ordinairement à 3 nervures ; fleurs blanches, en grappe courte ovoïde ; bractée plus courte que le pédicelle ; étamines incluses.	**T. palustris 3339**