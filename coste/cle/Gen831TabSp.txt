TABLEAU DES ESPÈCES

Plantes à souche longuement rampante ou à tiges fortement comprimées. i^ Panicule oblongue, à rameaux inférieurs géminés ou ternes ; glumelle inférieure sans nervures saillantes.
Tiges comprimées à 2 tranchants ; feuilles non distiques ; ligule courte, tronquée ; panicule étroite, presque unilatérale, à rameaux courts, entièrement garnis d'épillets ; épillets lancéolés, à S-9 fleurs, l'inférieure longue de 
2 1/2 mm	**P. compressa 4104**
Tiges cylindriques ; feuilles distiques sur les rejets stériles ; ligule ovale ou oblongue ; panicule étalée, à rameaux assez longs, DES à la base ; épillets ovales, à 3-5 fleurs, l'inférieure longue d'environ 5 mm.	**P. cenisia 4105**
Panicule pyramidale, à rameaux inférieurs ordinairement réunis par 3-5 ; glumelle inférieure à , 5 nervures saillantes.
Tiges cylindriques ou un peu comprimées à la base ; feuilles étroites, n'ayant que 1-5 mm. ; épillets ovales, à 3-5 fleurs réunies à la base par un tomentum laineux ; glumelle inférieure velue -ciliée dans le bas.
P. pratensis 410G
Tiges comprimées à 2 tranchants ; feuilles larges de 3-10 mm. ; épillets ovalesoblongs, à 3-5 fleurs glabres ou peu poilues à la base ; glumelle inférieure scabre, non velue-ciliée sur les nervures.
Feuilles brusquement mucronées et souvent courbées en cuiller au sommet ; souche courtement rampante ; gaines souvent rudes, la supérieure bien plus longue que le limbe ; fleur inférieure longue de 3 1/2 mm. ; glumelle ovale-aiguë	**P. Chaixii 4107**
Feuilles longuement et insensiblement acuminées au sommet ; souche longuement rampante ; gaines lisses, la supérieure de la longueur du limbe ; fleur inférieure longue de 4 1/2 mm. ; glumelle lancéolée-acuminée.	**P. hybrida 4108**
Plantes à souche fibreuse ou à peine rampante, à tiges cylindriques ou un peu comprimées.
'^ Panicule allongée, oblongue ou pyramidale, à rameaux scabres, les inférieurs ordinairement réunis 3-7 en demi-verticilles.
'^ Plantes hautes de 40 cm. à 1 mètre, recherchant les lieux humides ou le bord des eaux ; ligule-oblongue ; panicule ample, iiyramidale, étalée-dif fuse.
Gaines et tiges scabres de bas en haut, surtout au sommet ; glumes inégales, l'inférieure uninervée ; glumelle inférieure à 5 nervures assez distinctes ; plante à port généralement assez robuste	**P. trivialis 4109**
Gaines et tiges lisses ou presque lisses ; glumes peu inégales, toutes deux trinervées ; glumelle inférieure à 5 nervures peu distinctes ; plante à j)ort grêle de P. nemomte	**P. palustris 4110**
Plantes souvent peu élevées, habitant les bois ou les lieux secs, à panicule tantôt étalée diffuse, tantôt étroite ou peu fournie.
Tiges grêles ou un peu raides, lisses, à nœud supérieur situé vers leur milieu ; gaines lisses, plus courtes que les entre-nœuds, ne recouvrant pas les nœuds de la tige.
Feuille supérieure à gaine plus courte que le limbe ; ligule presque nulle ; tiges à nœud supérieur silué un peu au-dessous du milieu ; panicule étalée diffuse ou étroite et peu fournie	**P. nemoralis 4111**
Feuille supérieure à gaine un peu plus longue que le limbe ; ligule oblongue ; tiges à nœud supérieur situé au-dessus de leur milieu ; panicule étroite, souvent réduite à 1-4 épillets	**P. Balbisii 4112**
Tiges raides, souvent scabres, à nœud supérieur ordinairement silué vers le tiers ou le quart inférieur ; gaines souvent rudes, aussi longues ou presque aussi longues que les entre-nœuds.
Feuilles planes, ayant 1-2 mm. de large, à gaines recouvrant entièrement les nœuds de la tige ; ligules inférieures courtes, la supérieure ovale ; glumelle ovale-obtuse ; plante d'un vert bleuâtre . .	**P. caesia 4113**
Feuilles enroulées-filiformes, étroites, longues, à gaines laissant voir souvent les nœuds de la tige ; ligules oblongues-lancéolées ; glumelle inférieure aiguë ou mucronée ; plante verte P. violacea 4114 tj%: Panicule courte, ovale ou oblongue, rarement pyramidale, à rameaux lisses ou un lieu rudes, les inférieurs solitaires ou géminés ; tiges et gaines lisses.
X Panicule plus ou moins dense, souvent fournie et vivipare, à rameaux ordinairement courts et un peu rudes ; feuilles généralement courtes.
-\- Tiges renflées eu bulbe à lu base ; feuilles étroites, insensiblement acuminées eu alêne ; ligules oblongues-laucéolées.
Feuilles pluues qu canaliculées et un peu enroulées ; pauicule très souvent vivipare ; épillets ovales, à 4-6 fleurs réunies par de longs et nombreux poils laineux ; plante de 10-40 cm	**P. bulbosa 4113**
Feuilles enroulées-sétacées, très menues ; panicule petite, n'ayant que 
1-2 cm. ; épillets ovales-oblongs, à 6-10 fleurs munies à la base de poils courts et peu abondants ; plante très grêle de 5-2S cm.	**P. concinna 4116**
-\- Tiges non renflées-bulbeuses à la base ; feuilles assez larges, planes, brusquement aiguës ou un peu obtuses.
Tiges longues de 8-40 cm., raides ; ligules inférieures assez courtes et tronquées, la supérieure oblongue-aiguë ; panicule ovale ou pyramidale, à épillets nombreux et rapprochés ; glumes et glumelles aiguës.	**P. alpina 4117**
Tiges naines de 3-6 cm., faibles, ascendantes ; ligules courtes, tronquées ; panicule très petite, ovoïde, n'ayant que S-10 mm., composée de 
3-8 épillets ; glumes et glumelles ovales- obtuses.	**P. Foucaudi 4118**
X Panicule lâche, souvent peu fournie, jamais vivipare, à rameaux un peu longs, lisses ; feuilles assez allongées, planes, molles, à ligules oblongues ou lancéolées. tî< Panicule souvent penchée, oblongue, étroite, à rameaux dressés après la floraison ; épillets largement ovales ; glumelle inférieure à nervures peu distinctes.
Rameaux de la panicule filiformes, mais fermes, flexueux ; épillets non trembloltants, à 2-4 fleurs ; glumes égalant au moins les trois quarts de l'épillet, toutes deux trinervées ; tiges assez fermes.	**P. laxa 4119**
Rameaux de la panicule fins, capillaires, moins fermes, non flexueux ; épillets trembloltants, à 4-6 fleurs ; glumes égalant la moitié de l'épillet, l'inférieure plus courte uninervée ; tiges faibles, filiformes.
P. minor 4120 tî« Panicule dressée, ovale, presque unilatérale, à rameaux étalés en angle droit ou réfléchis ; épillets ovales-oblongs ; glumelle inférieure à 5 nervures assez distinctes.
Plante vivace, à tiges couchées et un peu radicantes à la base ; panicule élégamment panachée de violet, de vert et de blanc, peu fournie, à rameaux inférieurs ne portant que 2 ou 3 épillets ; plantes des hautes montagnes	**P. supina 4121**
Plante annuelle ou bisannuelle, à tiges étalées-ascendantes ; panicule verdâtre ou un peu violacée, généralement assez fournie, à rameaux inférieurs portant 3-10 épillets ; plante répandue partout.	**P. annua 4122**