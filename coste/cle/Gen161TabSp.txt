TABLEAU DES ESPÈCES

Fleurs en grappes allongées, interrompues ; feuilles simples ou trifoliolées ; sous‑arbrisseaux à fleurs jaunes.
	Sous‑arbrisseau de 10‑50 cm., à rameaux tortueux, glabrescents, les anciens terminés en épine ; feuilles linéaires‑oblongues, souvent pliées ; carène obtuse.	**A. Hermanniae 812**
	Sous‑arbrisseau de 30‑80 cm., à rameaux élancés, tomenteux‑blanchâtres, non spinescents ; feuilles ovales ou elliptiques, planes ; carène apiculée.	**A. cytisoides 813**
Fleurs en têtes terminales ou en glomérules axillaires ; feuilles imparipennées, au moins les supérieures.
	Tiges ligneuses, au moins à la base ; folioles nombreuses, toutes égales ; calice jamais renflé en vessie.
		Arbrisseau atteignant 1 mètre, dressé, soyeux‑argenté ; fleurs jaunâtres, en têtes terminales et axillaires ; calice à dents bien plus courtes que le tube ; étendard à limbe égalant l'onglet.	**A. Barba‑Jovis 814**
		Plante de 10‑30 cm., ligneuse seulement à la base, gazonnante, velue‑hérissée ; fleurs rouges, en têtes terminales ; calice à dents plumeuses égalant le tube ; étendard à limbe 2 fois plus long que l'onglet.	**A. montana 815**
	Tiges entièrement herbacées ; folioles peu nombreuses, plus ou moins inégales ; calice renflé en vessie après la floraison.
		Fleurs jaunes, rouges ou blanches, en tètes terminales denses, le plus souvent géminées ; calice bilabié, à ouverture oblique, à dents très inégales ; carène obtuse.	**A. Vulneraria 816**
		Fleurs jaunâtres, en glomérules axillaires lâches et peu fournis ; calice régulier, à ouverture droite, à 5 dents égales ; carène courbée et apiculée.	**A. tetraphylla 817**