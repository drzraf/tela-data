TABLEAU DES GENRES

Fruit oblong, en silique, comprimé, à 2 valves, à 2 ou plusieurs graines ; grappes pauciflores. 	**CORYDALIS 30**
Fruit court, globuleux ou ovoïde, à 1 graine ; grappes multiflores. 	**FUMARIA 31**