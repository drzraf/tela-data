TABLEAU DES ESPÈCES

Epillets lancéolés ou linéaires-oblongs, largos de 2-3 mm. ; glumes ovales-lancéolées ; caryopses subglobuleux ; feuilles finement denticulées et tuberculeuses aux bords.
Epillets grands, atteignant 3 mm. de large, rapprochés-fascicules, formant une panicule ovale-oblongue, assez dense, à rameaux et pédicellés courts ; gaines glabres, excepté autour de la ligule	**E. major 4123**
Epillets médiocres, n'atteignant que 2 mm. de large, non fascicules, formant une panicule oblongue, assez lâche, à rameaux et pédicellés plus fins et plus longs ; gaines garnis de longs poils sur toute leur surface	**E. minor 4124**
Epillets linéaires, étroits, n'ayant que 1-1 1/2 mm. de large ; glumes lancéolées ; caryopses oblongs ; feuilles finement denticulées mais sans tubercules glandulif ormes aux bords.
Panicule toujours étalée, à rameaux solitaires ou géminés, garnis d'épillets presque dès la base ; tige portant à toutes les gaines une petite panicule ; pédicelles bien plus courts que les epillets à fleurs densément imbriquées.	**E. Barrelleri 4125**
Panicule d'abord contractée puis étalée, à rameaux réunis par 4-S en demi-verticille, capillaires et DES dans la moitié inférieure ; pas de petite panicule à l'aisselle des feuilles ; pédicelles aussi longs que les epillets courts et à fleurs lâchement rapprochées	**E. pilosa 4126**