TABLEAU DES ESPÈCES

{1} Plantes pubescentes ou tomenteuses.
	Fleurs en corymbe ; feuilles ovales, demi‑embrassantes ; plante tomenteuse‑blanchâtre de 20‑40 cm.	**H. tomentosum 672**
	Fleurs en panicule étroite ; feuilles ovales‑oblongues, brièvement pétiolées ; plante pubescente‑verdâtre de 40 cm. à 1 mètre.	**H. hirsutum 673**
{1} Plantes entièrement glabres.
	{2} Tiges offrant 2 ou 4 lignes plus ou moins saillantes ; sépales ordinairement non ciliés.
		{3} Tiges de 5‑20 cm., filiformes, couchées ou ascendantes ; fleurs solitaires ou en petites cymes feuillées ; 12‑20 étamines.
			Tiges munies de 4 lignes saillantes ; feuilles ovales‑arrondies, demi‑embrassantes ; pédicelles plus courts que le calice ; sépales lancéolés‑aigus.	**H. corsicum 674**
			Tiges munies de 2 lignes faibles ; feuilles oblongues, sessiles ; pédicelles égalant le calice ou plus longs ; sépales inégaux, oblongs‑obtus.	**H. humifusum 675**
		{3} Tiges dépassant 20 cm., raides, dressées ; fleurs nombreuses, en corymbe ou en panicule ; étamines en nombre indéfini.
			{4} Tiges offrant seulement 2 lignes longitudinales ; capsule munie de 1-3 canaux et de vésicules nombreuses.
				Feuilles demi‑embrassantes, élargies en coeur à la base ; sépales frangés ; capsule couverte de vésicules obliquement disposées en séries régulières.	**H. perfoliatum 676**
				Feuilles sessiles, non élargies en coeur à la base ; sépales dépourvus de franges ; capsule couverte de vésicules irrégulièrement disposées.	**H. perforatum 677**
			{4} Tiges offrant 4 angles ou 4 ailes ; capsule munie de nombreux canaux longitudinaux, sans vésicules.
				{5} Tiges à 4 angles ailés ; feuilles élargies à la base et demi‑embrassantes, ponctuées ; fleurs petites, d'un jaune pâle, à pétales dépassant peu les sépales lancéolés‑aigus.	**H. tetrapterum 678**
				{5} Tiges à 4 angles non ailés ; feuilles rétrécies à la base et simplement sessiles ; fleurs grandes, d'un jaune doré, à pétales bien plus longs que les sépales.
					Sépales lancéolés‑aigus, sans points noirs ; pétales dépourvus de glandes noires ; feuilles munies de points transparents, à nervures secondaires peu ramifiées.	**H. Desetangsii 679**
					Sépales ovales‑obtus, munis de points noirs ; pétales marqués de glandes noires allongées ; feuilles non ponctuées, à nervures secondaires ramifiées en réseau.	**H. quadrangulum 680**
	{2} Tiges cylindriques, sans lignes saillantes ; sépales bordés de cils glanduleux ou de franges.
		{3} Feuilles linéaires ou oblongues, à bords souvent enroulés.
			{4} Feuilles verticillées ou fasciculées, ponctuées‑transparentes ; sépales obtus ou subobtus.
				Feuilles toutes verticillées par 3‑4 ; fleurs en panicule courte, corymbiforme ; capsule 1 fois seulement plus longue que le calice.	**H. Coris 681**
				Feuilles supérieures fasciculées aux noeuds ; fleurs en panicule longue, pyramidale ; capsule 3 fois plus longue que le calice.	**H. hyssopifolium 682**
			{4} Feuilles opposées 2 à 2, non ponctuées‑transparentes ; sépales aigus ou subaigus.
				Feuilles linéaires ou linéaires‑oblongues ; fleurs en corymbe composé, multiflore ; sépales munis de longs cils ; capsule 1‑2 fois plus longue que le calice.	**H. linarifolium 683**
				Feuilles elliptiques ou linéaires‑oblongues ; fleurs en corymbe court, pauciflore ; sépales à peine ciliés‑glanduleux ; capsule égalant ou dépassant peu le calice.	**H. australe 684**
		{3} Feuilles orbiculaires, ovales ou oblongues, planes, non enroulées par les bords.
			{4} Feuilles non ponctuées‑transparentes ; fleurs grandes, peu nombreuses, à pétales 3‑4 fois plus longs que le calice.
				Feuilles orbiculaires, brièvement pétiolées ; sépales presque obtus ; étamines égalant environ les pétales ; capsule munie de nombreux canaux longitudinaux.	**H. nummularium 685**
				Feuilles ovales‑lancéolées, demi‑embrassantes ; sépales lancéolés‑aigus ; étamines de moitié plus courtes que les pétales ; capsule ponctuée de noir, sans canaux.	**H. Richeri 686**
			{4} Feuilles toutes ou seulement les supérieures ponctuées‑transparentes ; fleurs assez petites.
				Feuilles ovales‑obtuses, embrassantes en coeur, toutes ponctuées‑transparentes ; fleurs en panicule allongée, étroite ; sépales ovales‑obtus.	**H. pulchrum 687**
				Feuilles ovales‑lancéolées, sessiles, les supérieures seules ponctuées‑transparentes ; fleurs en corymbe court et serré ; sépales lancéolés‑aigus.	**H. montanum 688**