TABLEAU DES ESPÈCES

Feuilles intérieures orbiculaires, peltées-ombiliquées, crénelées ; fleurs d'un blanc jaunâtre, pédicellées, penchées, en grappe allongée ; plante vivace de 10-40 cm.	**U. pendnlinus 1392**
Feuilles toutes oblongues, obtuses, nombreuses et imbriquées sur les tiges ; fleurs roses, subsessiles, dressées, 2-4 en têtes courtes et serrées ; plante annuelle de 3-6 cm.	**U. sedoides 1393**