TABLEAU DES ESPÈCES

4» Epi ovale ou subglobuleux, court (1-2 cm.) ; feuille supérieure à gaine fortement renflée en vessie.
Plante vivace, à souche épaisse et oblique-rampante ; épi velu-soyeux blanchâtre, à rameaux portant 2-3 epillets longs de 4 mm. ; glumes libres, aristées ; 
2 glumelles, l'inférieure portant sur le dos une arête incluse.	**A. Gerardi 3938**
Plante annuelle, à racine fibreuse grêle ; épi presque glabre et souvent violacé, à rameaux portant 1 ou 2 epillets longs de 6-7 mm. ; glumes soudées jusqu'au milieu, acuminées ; glumelle unique, munie d'une arête très saillante.	**A. utriculatus 3939**
4» Ei)i cylindrique, long de 3 à 13 cm. ; feuille supérieure à gaine cylindrique, peu ou point renflée.
Epi souvent très long, épais de 4-8 mm., à epillets longs de S-6 mm. ; glumes soudées intérieurement ; tiges ni bulbeuses, ni couchées.
Plante annuelle, à racine fibreuse grêle ; épi glabrescent, assez grêle, atténué aux deux bouts, à rameaux portant 1 ou 2 epillets ; glumes soudées jusqu'au milieu, brièvement pubescentes sur la carène un peu ailée.	**A. agrestis 3940**
Plantes vivaces, à souche stolonifère ; épi velu-soyeux, compact, obtus, à rameaux portant 4-6 epillets ; glumes soudées seulement dans le tiers ou quart inférieur, longuement ciliées sur la carène non ailée.
Souche à rhizomes courts et obliques ; feuilles larges de 3-7 mm. ; épi long de 3-8 cm. ; glumes non divergentes ; glumelle à arête presque une fois plus longue que les glumes	**A. pratensis 3941**
Souche à rhizomes longuement traçants ; feuilles larges de 5-12 mm. ; épi long de S-lo cm. ; glumes divergentes ; glumelle un i)eu plus courte que les glumes, à arête peu ou point saillante . .	**A. arundinaceus 3942**
Epi long seulement de 3-S cm., épais de 2-4 mm., à epillets petits (2-3 mm.) ; glumes presque libres ou à peine soudées à la base.
Tige renflée en bulbe à la base, dressée ou ascendante ; feuilles étroites 
(1-3 mm.) ; glumes aiguës, faiblement ciliées ; glumelle à arête environ 
2 fois plus longue que les glumes	**A. bulbosus 3943**
Tige non renflée en bulbe, mais couchée-radicante et genouillée dans le bas ; feuilles larges de 3-5 mm. ; glumes obtuses ou subobtuses, assez longuement ciliées.
Glumelle munie vers le quart inférieur d'une arête 1-2 fois plus longue que les glumes ; anthères d"uu blanc jaunâtre ; feuilles vertes ou glaucescentes.	**A. geniculatus 3944**
Glumelle munie vers le milieu d'une arête courte ne dépassant pas ou très peu les glumes ; anthères fauves-orangées ; feuilles glauques-bleuâtres.	**A. fulvus 3945**