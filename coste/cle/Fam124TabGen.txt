TABLEAU DES GENRES

Frondes ayant au moins 2 mm. de long, émettant en dessous une ou plusieurs fibres radicales	**LEMNA 752**
Frondes minuscules, ayant à peine 1mm. de long, toujours dépourvues de fibres radicales.	**WOLFFIA 753**