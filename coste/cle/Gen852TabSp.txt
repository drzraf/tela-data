TABLEAU DES ESPÈCES

Une seule glume aux épillets latéraux ; ceux-ci appliqués contre l'axe par le dos de la glumelle inférieure, à peine plus longs que les entrenœuds de l'épi ; plante dressée ou ascendante, à épi cylindrique droit	**L. cylindricus 4239**
Deux glumes extérieures et contiguës dans les épillets latéraux ; ceux-ci appliqués contre l'axe par les côtes de la fleur.
Epi plus ou moins fortement arqué, grêle, raide, cylindrique ; épillets dépassant sensiblement les entrenœuds ; glumes dépassant l'épillet de près d'un tiers ; fleurs s'ouvrant peu et tardivement ; tiges élalées-arquées. . .	**L. incurvatus 4260**
Epi droit ou un peu arqué-flexueux, très grêle et très délié, un peu arqué ; épillets dépassant à peine les entrenœuds ; glumes égalant à peu près l'épillet ; fleurs s'ouvrant et s'étalant promptement ; liges dressées ou ascendantes, souvent filiformes L. filiformis. 4261