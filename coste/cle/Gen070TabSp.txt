TABLEAU DES ESPÈCES

Feuilles inférieures sinuées‑dentées, les supérieures à oreillettes aiguës ; plantes d'un vert gai, à odeur d'ail.
	Silicules orbiculaires, planes, largement ailées tout autour ; graines striées ; tige entièrement glabre.	**T. arvense 321**
	Silicules elliptiques, en coin, convexes, étroitement bordées ; graines alvéolées ; tige pubescente à la base.	**T. alliaceum 322**
Feuilles entières ou denticulées, à oreillettes obtuses ou subaiguës ; plantes plus ou moins glauques, inodores.
	Pétales violets ; grappe fructifère en corymbe serré ; silicules non échancrées, ni ailées ; graines ponctuées.	**T. rotundifolium 323**
	Pétales blancs ; grappe fructifère non en corymbe ; silicules mûres échancrées et ailées ; graines lisses.
		Style bien plus court que les lobes de la silicule ; anthères jaunâtres ou blanches. 
			Silicules ovales, courtes, peu atténuées à la base, à aile bordée ; plante annuelle, grêle.	**T. perfoliatum 324**
			Silicules oblongues, allongées, fortement atténuées à la base, à aile non bordée ; plantes bisannuelles ou vivaces.
				Grappe fructifère très longue ; silicules largement ailées au sommet ; plante élancée.	**T. brachypetalum 325**
				Grappe fructifère courte et serrée ; silicules très étroitement ailées au sommet ; plante basse.	**T. brevistylum 326**
		Style égalant ou dépassant les lobes de la silicule ; anthères ordinairement violettes ou lilas.
			Étamines bien plus courtes que les pétales orbiculaires ; sépales dressés ; silicules oblongues, à peine ailées.	**T. alpinum 327**
			Étamines égalant ou dépassant les pétales obovales ; sépales étalés ; silicules plus ou moins largement ailées.
				Fleurs grandes ; grappe fructifère courte ; silicules ovales, arrondies à la base ; loges à 1‑4 graines.	**T. montanum 328**
				Fleurs assez petites ; grappe fructifère longue ; silicules obovales, en coin ; loges à 4‑8 graines.	**T. silvestre 329**