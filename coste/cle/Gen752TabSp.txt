TABLEAU DES ESPÈCES

Frondes adultes larges de plus de 2 mm., planes des deux côtés, munies en dessus de nervures fines.
Frondes brun rougeâtre en dessous, un peu épaisses, opaques, suborbiculaires, non rétrécies à la base, émettant chacune un faisceau de fibres radicales.
L. polyrrhiza 3G73
Frondes d'un vert clair, minces, transparentes, lancéolées-aiguës, atténuées ou pétiolées à la base et réunies en croix, émettant en dessous 1 seule radicelle.	**L. trisulca 3674**
Frondes larges d'environ 2 mm., ovales ou suborbiculaires, opaques, vertes et sans nervures en dessus, émettant 1 seule radicelle en dessous.
Frondes lenticulaires, un peu épaisses, planes des deux côtés, non spongieuses en dessous ; fruit à 1 seule graine, indéhiscent L. miner 3G7o 
Frondes hémisphériques, un peu convexes en dessus, très renflées-spongieuses en dessous ; fruit à 2-7 graines, s'ouvrant en travers	**L. gibba 3676**