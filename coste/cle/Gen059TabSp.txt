TABLEAU DES ESPÈCES

Silicules en toupie, tronquées‑échancrées au sommet, très renflées, à valves molles ; grappes fructifères courtes.	**C. foetida 291**
Silicules en poire, arrondies au sommet, peu ou point renflées, à valves dures ; grappes plus ou moins allongées.
	Silicules en poire oblongue, ventrues, peu renflées ; grappes fructifères et pédoncules assez allongés.	**C. sativa 292**
	Silicules en poire obovale, convexes, ni renflées, ni ventrues ; grappes et pédoncules très allongés.	**C. silvestris 293**