TABLEAU DES ESPÈCES

Fleurs en petit corymbe ; capsule plus courte que le pédicelle ; valves à 3 nervures ; folioles épaisses, en coeur. 	**C. enneaphylla 137**
Fleurs en grappe courte ; capsule égalant au moins le pédicelle ; valves à 1 nervure ; folioles minces, ovales ou oblongues.
	Fleurs jaunes, en grappes opposées aux feuilles ; éperon court et obtus ; racine fibreuse.
		Pétioles terminés en vrille rameuse ; bractées dépassant le pédicelle ; fleurs petites.	**C. claviculata 138**
		Pétioles sans vrille ; bractées bien plus courtes que le pédicelle ; fleurs assez grandes.	**C. lutea 139**
	Fleurs purpurines ou blanches, en grappe terminale ; éperon allongé ; souche bulbeuse.
		Bulbe creux ; tige dépourvue d'écailles au-dessous des feuilles ; éperon arrondi et courbé au sommet.	**C. cava 140**
		Bulbe plein ; tige munie de 1-3 écailles au-dessous des feuilles ; éperon atténué, droit ou à peine courbé.
			Bractées ovales, entières ; grappes réfléchies après la floraison ; pédicelles 3-4 fois plus courts que la capsule.	**C. fabacea 141**
			Bractées digitées ou incisées ; grappes dressées ; pédicelles égalant la capsule.	**C. solida 142**