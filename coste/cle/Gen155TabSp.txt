TABLEAU DES ESPÈCES

Feuilles inférieures trifoliolées, pétiolées, les supérieures simples et sessiles ; calice glabre ; style enroulé sur lui‑même ; gousse velue‑hérissée sur les bords.	**S. scoparius 752**
Feuilles toutes trifoliolées et pétiolées ; calice pubescent‑soyeux ; style courbé presque en cercle ; gousse glabrescente.	**S. catalaunicus 753**