TABLEAU DES ESPÈCES

Fruit obconique, fortement sillonné dans toute sa longueur, à soies toutes étalées-ascendantes ; folioles cendrées-tomenteuses et sans glandes odorantes en dessous.	**A. Eupatoria 1234**
Fruit presque hémisphérique, faiblement sillonné surtout dans le bas, à soies du rang extérieur réfléchies ; folioles vertes-pubescentes et munies de glandes odorantes en dessous.	**A. odorata 1235**