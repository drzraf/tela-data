TABLEAU DES ESPÈCES

{1} Plantes vivaces, à souche épaisse ou rameuse, rarement plante ligneuse.
	{2} Plante ligueuse de 1 à 2 mètres ; feuilles persistantes, coriaces, oblongues, atténuées à la base ; ombelles à 8-20 rayons égaux ; involucre réfléchi et caduc.		B. **fruticosum 1573**
	{2} Plantes herbacées, peu élevées ; feuilles non persistantes, les caulinaires ordinairement élargies et embrassantes à la base ; ombelles à rayons moins nombreux ; involucre étalé ou dressé, persistant.
		{3} Feuilles à 1 seule nervure, fortement veinées en réseau.
			Ombelles à 5-12 rayons ; involucre à 5-8 folioles ; involucelle à 5-8 folioles ovales-aiguës, libres ; feuilles à réticulation large et serrée, les inférieures ovales ou oblongues ; tige creuse, feuillée.				B. **longifolium 1574**
			Ombelles à 3-6 rayons ; involucre à 2-5 folioles ; involucelle à folioles orbiculaires ou soudées ; feuilles à réticulation fine et serrée, les inférieures lancéolées ou linéaires ; tige pleine.
				Tige feuillée dans toute sa longueur ; involucre à folioles ovales-obtuses, parfois sublobées à la base ; involucelle à 5-6 folioles libres, orbiculaires, obtuses, non mucronées.					B. **angulosum 1575**
				Tige longuement nue intérieurement ; involucre à folioles ovales ou lancéolées, mucronées, entières ; involucelle à folioles longuement soudées, à 5-10 lobes courts, mucronulés.					B. **stellatum 1576**
		{3} Feuilles à 3 ou plusieurs nervures, non ou faiblement veinées en réseau.
			{4} Tige presque nue ou à feuilles la plupart réunies à la base.
				Tige longuement nue inférieurement, presque simple ; feuilles radicales linéaires-allongées, non coriaces, faiblement nervées, sans nervure marginale ; ombelles à 5-10 rayons ; involucelle à folioles lancéolées ; fruit à côtes subailées.					B. **petræum 1577**
				Tige densément feuillée la la base, très rameuse au sommet ; feuilles inférieures ovales ou oblongues, coriaces, à nervures très saillantes, dont 2 marginales ; ombelles petites, à 2-5 rayons ; involucelle à folioles linéaires ; fruit à côtes fines et peu saillantes.					B. **rigidum 1578**
			{4} Tige régulièrement feuillée, à feuilles équidistantes.
				Feuilles inférieures linéaires ou linéaires-lancéolées, sans nervure marginale, les supérieures à base élargie-embrassante ; involucelle à folioles ovales ou elliptiques-lancéolées ; vallécules à 1-2 bandelettes ; plante de 10-50 cm.					B. **ranunculoides 1579**
				Feuilles inférieures généralement ovales ou lancéolées, souvent en faux, munies d'une nervure marginale, les supérieures peu ou point élargies à la base ; involucelle à folioles lancéolées ou linéaires ; vallécules à 3-6 bandelettes ; plante de 30-80 cm.					B. **falcatum 1580**
{1} Plantes annuelles, à racine grêle, simple, pivotante.
	{2} Feuilles perfoliées, largement ovales ou arrondies, obtuses, à nervures rayonnantes ; involucre nul ; involucelle à folioles largement ovales, brièvement cuspidées.
		Ombelles à 4-8 rayons ; involucelle à folioles dressées après le floraison ; à fruit lisse ; feuilles ovales-suborbiculaires.			B. **rotundifolium 1581**
		Ombelles à 2-3 rayons ; involucelle à folioles restant étalées après la floraison ; fruit tuberculeux-granuleux ; feuilles ovales-oblongues, un peu atténuées au sommet.			B. **protractum 1582**
	{2} Feuilles non perfoliées, linéaires ou lancéolées, très aiguës, à nervures parallèles ; involucre à 1-5 folioles persistantes ; involucelle à folioles longuement acuminées.
		{3} Involucre à folioles larges, ovales-lancéolées, dépassant longuement les rayons de l'ombelle ; involucelle à folioles glumacées, membraneuses, ovales-lancéolées, aristées, cachant les fleurs et les fruits ; plante trapue de 5-25 cm.			B. **aristatum 1583**
		{3} Involucre à folioles étroites, linéaires ou linéaires-lancéolées, plus courtes que les rayons les plus longs de l'ombelle ; involucelle à folioles linéaires ou lancéolées-acuminées, ne cachant pas les fleurs ni les fruits ; plantes élancées ou très grêles.
			{4} Fruit ovoïde-oblong, lisse ; feuilles allongées ; tige raide, dressée ; plantes d'un vert clair.
				Ombelles à 2-3 rayons ; involucelle à folioles plus courtes que les fleurs et les fruits ; fruit à côtes très saillantes, aiguës ; vallécules sans bandelettes ; feuilles largement linéaires ; plante de 40 cm. à 1 mètre, à rameaux étalés.					B. **junceum 1584**
				Ombelles à 3-8 rayons ; involucelle à folioles dépassant ou au moins égalant les fleurs et les fruits ; fruit à côtes fines à peine saillantes ; vallécules à 1-3 bandelettes ; feuilles étroitement linéaires ; plante de 20-50 cm., à rameaux dressés ou ascendants.					B. **affine 1585**
			{4} Fruit subglobuleux, presque didyme, tuberculeux ; feuilles courtes ; tige grêle, souvent tombante ; plantes glauques, à la fin d'un vert sombre.
				Ombelles terminales à 2-4 rayons, les latérales réduites à des ombellules de 2-5 fleurs espacées le long des rameaux ; involucre à 1-4 folioles ; fruit gros, granuleux-tuberculeux, à côtes saillantes ondulées-crispées ; plante de 10-50 cm.					B. **tenuissimum 1586**
				Ombelles petites à 3-6 rayons filiformes ; involucre et involucelle à 5 folioles ; fruit petit, hérissé de petits tubercules blancs, à côtes presque nulles ; plante de 5-15 cm. à rameaux courts.					B. **semicompositum 1587**