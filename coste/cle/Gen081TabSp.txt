TABLEAU DES ESPÈCES

Tiges ligneuses ; 3 sépales égaux ; funicules filiformes.
	Fleurs blanches, rarement rouges ; feuilles linéaires ou linéaires‑lancéolées ; graines peu nombreuses.	**H. umbellatum 362**
	Fleurs jaunes ; feuilles oblongues ou lancéolées ; graines nombreuses.
		Rameaux dressés ; feuilles blanches‑argentées, veloutées ; pétales tachés de noir violet à l'onglet.	**H. halimifolium 363**
		Rameaux décombants ; feuilles vertes et poilues en dessus ; pétales non maculés.	**H. alyssoides 364**
Tiges herbacées ou ligneuses à la base ; à sépales inégaux, les 2 extérieurs plus petits ; funicules en massue.
	Style court, droit ; étamines sur 1 rang ; plantes ordinairement annuelles.
		Feuilles toutes ou les inférieures sans stipules ; pétales 1‑2 fois plus longs que les sépales ; style nul.
			Feuilles toutes sans stipules ; grappe munie de bractées ; sépales glabres ; plante vivace.	**H. Tuberaria 365**
			Feuilles supérieures munies de stipules foliacées ; grappe nue ; sépales poilus, plante annuelle.	**H. guttatum 366**
		Feuilles inférieures seules stipulées ; pétales égalant à peine les sépales ; style court.
			Pédicelles dressés, 2‑3 fois plus courts que le calice ; bractées égalant ou dépassant les fleurs ; capsule grosse.	**H. ledifolium 367**
			Pédicelles étalés, plus longs que le calice ; bractées égalant à peine les pédicelles ; capsule petite.	**H. salicifolium 368**
	Style long, genouillé ou contourné à la base ; étamines sur plusieurs rangs ; plantes vivaces.
		Feuilles sans stipules ; pétales petits, dépassant peu les sépales ; style contourné en cercle.
			Inflorescence en grappe simple ; feuilles ovales‑lancéolées, en fausse rosette sur les rameaux stériles.	**H. montanum 369**
			Inflorescence bi‑trichotome ; feuilles ovales en cour, non rapprochées en rosette sur les rameaux stériles.	**H. marifolium 370**
		Feuilles stipulées ; pétales grands, bien plus longs que les sépales ; style genouillé.
			Fleurs en 3‑5 grappes courtes et serrées en corymbe, sans bractées ; capsules elliptiques‑trigones.	**H. lavandulaefolium 371**
			Fleurs en grappe simple, terminale, biche, bractéolée ; capsules subglobuleuses.
				Fleurs blanches ; sépales tomenteux ; stipules en alène.	**H. polifolium 372**
				Fleurs jaunes, rarement roses ; sépales hérissés ou presque glabres ; stipules linéaires ou lancéolées.
					Sépales hispides, 1‑2 fois plus longs que la capsule à 3 graines ; feuille à bords enroulés ; stipules linéaires.	**E. hirtum 373**
					Sépales à peine poilus, égalant la capsule à plusieurs graines ; feuille planes ; stipules lancéolées.	**E. vulgare 374**