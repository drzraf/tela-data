TABLEAU DES ESPÈCES

Graines subglobuleuses, entourées d'un rebord très étroit ; feuilles munies en dessous d'un sillon longitudinal.	**S. arvensis 575**
Graines aplaties, largement ailées ; feuilles non sillonnées en dessous.
	Pétales lancéolés‑aigus ; aile des graines blanche, lisse, aussi large qu'elles.	**S. pentandra 576**
	Pétales ovales‑obtus ; aile des graines rousse, ponctuée, moins large qu'elles.	**S. Morisonii 577**