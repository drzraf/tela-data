TABLEAU DES ESPÈCES

Involucre à folioles entièrement herbacées ; ombelles a 2-5 rayons inégaux ; fleurs de la circonférence à peine rayonnantes ; styles plus courts que le stylopode conique ; plante velue de 5-15 cm.	**O. maritima 1474**
Involucre à folioles largement scarieuses aux bords ; ombelles à rayons presque égaux ; fleurs de la circonférence longuement rayonnantes ; styles bien plus longs que le stylopode discoïde ; plantes glabrescentes de 20-40 cm.
	Ombelles à 2-3 rayons ; involucre à 2-3 folioles ; pétales de la circonférence 2-3 fois plus grands que les autres ; fruit oblong, peu atténué au sommet, à aiguillons rougeâtres, élargis à la base, aussi longs que sa largeur.	**O. platycarpos 1475**
	Ombelles à 5-8 rayons ; involucre à 5-8 folioles ; pétales de la circonférence 7-8 fois plus grands que les autres ; fruit ovale, atténué au sommet, à aiguillons blanchâtres, en alène dès la base, plus courts que sa largeur.	**O. grandiflora 1476**