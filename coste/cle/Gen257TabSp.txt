TABLEAU DES ESPÈCES

Fleurs en têtes sessiles ou subsessiles ; petites plantes de 3-20 cm., à feuilles radicales oblongues-lancéolées.
	Tiges couchées, les latérales souvent stolonifères ; fleurs bleues ; involucre à 5 folioles ; paillettes plus courtes que les folioles de l'involucre ; calice fructifère à dents dressées-convergentes ; fruit globuleux, nu à la base.	**E. viviparum 1459**
	Tiges dressées, rameuses-dichotomes au sommet ; fleurs blanches ; involucre à folioles nombreuses ; paillettes égalant les folioles de l'involucre ; calice fructifère à dents étalées ; fruit ovoïde-cylindrique, tout couvert d'écailles.	**E. Barrelieri 1460**
Fleurs en têtes nettement pédonculées ; plantes robustes de 20-60 cm., à feuilles radicales ovales en cœur ou palmatipartites.
	Involucre à folioles nombreuses, longuement pectinées ou pennatifides-épineuses.
		Feuilles radicales ovales en cœur, dentées-épineuses, peu coriaces ; tige simple ou ne portant que 2-3 têtes ; involucre bleuâtre, à folioles presque molles, faiblement nervées ; fruit muni de quelques écailles obtuses.	**E. alpinum 1461**
		Feuilles radicales palmatipartites, à segments profondément divisés en lobes dentés-épineux ; tige très épaisse, rameuse dès la base ; involucre blanc très coriace et nervé ; fruit couvert d'écailles aiguës.	**E. Spina-alba 1462**
	Involucre à folioles entières, dentées ou lobées.
	Involucre à 10-12 folioles étalées-dressées, bleuâtres, linéaires-lancéolées, entières ; fruit couvert d'écailles ovales-obtuses ; plante de 20-40 cm., à tige simple ou rameuse au sommet, à fleurs bleues.	**E. Bourgati 1463**
	Involucre à 4-6 folioles étalées ; fruit muni d'écailles acuminées ; plantes de 30-60 cm., rameuses, à rameaux étalés.
		Fleurs bleuâtres ; folioles de l'involucre larges, ovales ou rhomboïdales, incisées-lobées ; calice à. dents étalées en étoile ; paillettes du réceptacle trifides ; feuilles palmatilobées ; plante d'un glauque bleuâtre.	**E. maritimum 1464**
		Fleurs blanches ; folioles de l'involucre étroites, linéaires-acuminées ; calice à dents dressées ; paillettes du réceptacle entières ; feuilles 1-2 fois pennatipartites ; plante d'un vert blanchâtre.	**E. campestre 1465**