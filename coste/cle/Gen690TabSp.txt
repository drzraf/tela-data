TABLEAU DES ESPÈCES

Tige portant 2-S feuilles alternes et plus ou moins espacées ; 2 feuilles radicales et 
2 bulbes renfermés dans une tunique commune.
X Plante naine de 3-8 cm., presque toujours à 1-2 fleurs ; feuilles radicales filiformessétacées, cylindracées, n'ayant pas 1 mm. de large ; périanthe à divisions oblongues-spatulées, très obtuses ou subaiguës ; étamines d'un quart seulement plus courtes que le périanthe	**G. bohemica 3387**
X Plantes de S-IS cm., à l-S fleurs en corymbe ; feuilles radicales linéaires, presque planes, ayant au moins 1 mm. de large ; périanthe à divisions lancéolées plus ou moins aiguës ; étamines d'un tiers au moins plus courtes que le périanthe.
Feuilles radicales larges de 2-3 mm., les caulinaires inférieures à peine plus larges, velues-ciliées, portant à leur aisselle de nombreux bulbilles ; pédicelles laineux, aussi épais que la tige ; périanthe long de 13 mm., velu en dehors	**G. foliosa 3388**
Feuilles radicales larges de 1 mm., les caulinaires larges de 2 mm., à peine ciliées, élargies-embrassantes et sans bulbilles à leurs aisselles ; pédicelles glabres, capillaires, bien plus minces que la tige ; périanthe long de 10 mm,, entièrement glabre	**G. Soleirolii 3389**
Tige nue ou à 1 seule feuille en forme de spathe, portant sous l'ombelle 2-3 feuilles involucrales opposées ou très rapprochées.
»ï< Pédicelles glabres, ainsi que le périanthe.
= Feuilles radicales 2-3, linéaires-étroites (1 mm.), subcylindriques ; 1 seule caulinaire en spathe concave large de 3-3 mm. et éloignée de l'ombelle ; 
2 bulbes renfermés dans une seule tunique, accompagnés de nombreux bulbilles ; périanthe long de 8-12 mm	**G. spathacea 3390**
= Feuille radicale unique, linéaire ou lancéolée, large au moins de 2 mm., presque plane, sans feuille caulinaire ; 1 seul bulbe dans la tunique, accompagné ou non de bulbes nus.
Feuille radicale large de 2-3 mm., linéaire, insensiblement atténuée au sommet ; 2-3 bulbes horizontaux, dont 1-2 sans tunique ni feuille ; périanthe à divisions étroites, lancéolées ; graines ovales.	**G. stenopetala 3391**
Feuille radicale large de G-12 mm., lancéolée, élargie et brusquement contractée au sommet ; bulbe unique, dressé, tunique, émettant la feuille au sommet ; périanthe à divisions oblongues ; graines anguleuses.	**G. lutea 3392**
•Il Pédicelles velus ; 2 bulbes renfermés dans une tunique commune.
Feuilles radicales 1 ou 2, linéaires demi-cylindriques (1-3 mm.de large), creuses, peu ou point recourbées ; ombelle à 1-3 fleurs ; pédicelles dressés, presque aussi épais que la tige ; périanthe glabre, à divisions elliptiques-lancéolées obtuses	**G. Liottardi 3393**
Feuilles radicales 2, linéaires, canaliculées en dessus, recourbées au sommet ; ombelle fournie, à 3-12 fleurs ; pédicelles bien plus minces que la tige ; périanthe pubescent en dehors, à divisions lancéolées-aiguës.
Feuilles involucrales souvent plus étroites que les radicales, peu distinctes des bractées et ne dépassant pas les fleurs ; pédicelles dressés, non flexueux, formant une ombelle serrée ; périanthe long de 12-13 mm.	**G. Granatelli 3394**
Feuilles involucrales bien plus larges que les radicales, très distinctes des bractées, dépassant les fleurs ; pédicelles étalés, souvent flexueux, formant une ombelle lâche ; périanthe long de 16-20 mm., à divisions étroitement lancéolées	**G. arvensis 3393**