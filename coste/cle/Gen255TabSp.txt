TABLEAU DES ESPÈCES

Calice à dents lancéolées, acuminées-aristées ; feuilles palmatipartites, à 3-7 segments ovales en coin, incisés-dentés ; plante assez robuste de 20-60cm.	**A. major 1456**
Calice à dents ovales-obtuses, brusquement et brièvement aristées ; feuilles palmatiséquées, à 7-9 segments profonds, lancéolés. profondément dentés ; plante grêle de 10-35 cm.	**A. minor 1457**