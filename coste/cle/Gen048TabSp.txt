TABLEAU DES ESPÈCES

{1} Pétales larges, étalés, 2-3 fois plus longs que les sépales.
	Feuilles simples ou à 3-5 lobes.
		Feuilles en rein, sinuées-crénelées ; anthères violettes ; funicules filiformes.			C. **asarifolia 237**
		Feuilles à 3-5 lobes ; anthères jaunes ; funicules coniques.
			Feuilles grandes, à 3 lobes presque égaux, les caulinaires nulles ou solitaires ; grappe fructifère droite.				C. **trifolia 238**
			Feuilles petites, nombreuses sur la tige, à 3-5 lobes inégaux ; grappe fructifère à axe flexueux.				C. **Plumieri 239**
	Feuilles toutes ou la plupart pennatiséquées.
		Siliques hispides ; funicules coniques ; feuilles à lobes profondément incisés.
			Fleurs petites, blanches ; siliques lancéolées-linéaires, à bec long et à 2 tranchants.				C. **græca 240**
			Fleurs grandes, purpurines ; siliques linéaires, apiculées par le style court.				C. **chelidonia 241**
		Siliques glabres ; funicules filiformes ; lobes des feuilles entiers ou dentés.
			Pétales d'un blanc pur ; anthères violettes ; style filiforme, aigu. 			C. **amara 242**
			Pétales lilas ou roses, rarement blancs ; anthères jaunes ; style court et obtus.
				Feuilles toutes lyrées, à lobe terminal grand, orbiculaire, les latéraux ovales-arrondis.					C. **latifolia 243**
				Feuilles supérieures pennées, à lobes presque égaux, linéaires ou lancéolés-oblongs.					C. **pratensis 244**
{1} Pétales étroits, dressés, dépassant peu le calice.
	Plantes vivaces, peu élevées ; feuilles de la tige entières ou à 3-7 lobes ; siliques dressées.
		Feuilles entières ou sinuées-trilobées ; siliques atteignant la même hauteur ; graines non ailées.			C. **alpina 245**
		Feuilles de la tige pennatifides, a 3-7 lobes ; siliques n'atteignant pas la même hauteur ; graines ailées.			C. **resedifolia 246**
	Plantes annuelles ou bisannuelles ; feuilles pennatiséquées, à lobes nombreux ; siliques écartées de l'axe.
		Fleurs dépassées par les jeunes siliques ; grappes fructifères courtes ; plantes hérissées à la base.
			Feuilles de la tige 1-3, petites, à 5-7 lobes linéaires ou oblongs ; 4 étamines ; plante annuelle.				C. **hirsuta 247**
			Feuilles de la tige 6-12, grandes, à 5-11 lobes obovales ; 6 étamines ; plante bisannuelle.				C. **silvatica 248**
		Fleurs non dépassées par les jeunes siliques ; grappes fructifères longues ; plantes glabres.
			Feuilles de la tige auriculées, à lobes élargis, incisés-dentés ; pétales égalant à peine les sépales.				C. **impatiens 249**
			Feuilles de la tige non auriculées, à lobes linéaires, égaux, entiers ; pétales 1 fois plus longs que les sépales.				C. **parviflora 250**