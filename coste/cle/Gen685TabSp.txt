TABLEAU DES ESPÈCES

H Feuilles inférieures oblongues-lancéolées, larges de 3-6 cm. ; fleurs rouges à l'intérieur, très grandes, ayant ordinairement plus de 5 cm. de long ; étamines à filets glabres, brunâtres.
Divisions du périanthe portant à leur base une tache noire égalant au moins le tiers de leur longueur, très nettement cerclée de jaune et apparente à l'extérieur.
Feuilles plus longues que la fleur ; périanthe atténué à la base, ouvert au sommet, à divisions étalées, droites, oblongues-lancéolées, acuminées, à tache oblongue-lancéolée ; anthères dépassant beaucoup l'ovaire.
T. Oculus-solis 3368
Feuilles n'atteignant pas la fleur ; périanthe arrondi à la base, non évasé, fermé au somihet, à divisions appliquées, concaves, ovales, les intérieures obtuses, toutes à tache large et obovale ; anthères égalant l'ovaire ou plus courtes.
T. praecox 33G9
]g Divisions du périanthe portant à leur base une tache n'égalant que le quart de leur longueur, à peine ou obscurément bordée de jaune et peu ou point apparente à l'extérieur.
Feuilles atteignant la fleur ; périanthe d'un rouge vif uniforme sur les 2 faces ; anthères fortement mucronulées ; stigmates petits, plus étroits que le diamètre de l'ovaire ; bulbe laineux sous la tunique.	**T. Lortetii 3370**
Feuilles n'atteignant pas la fleur ; périanthe rouge, pourpre pâle ou jaunâtre, surtout en dehors ; anthères non ou à peine mucronulées ; stigmates larges, dépassant le diamètre de l'ovaire ; bulbe glabre ou peu poilu.	**T. Didieri 3371**
Q Feuilles toutes lancéolées-linéaires ou linéaires, étroites, ayant au plus 1 ou 2 cm. de large ; fleurs jaunes ou blanches à l'intérieur, atteignant rarement 5 cm. de long.
Feuilles 4-5, écartées ; bulbe laineux sous la tunique ; périanthe à divisions blanches, les extérieures roses en dehors, toutes portant à la base une tache violette ; étamines à filets brunâtres, glabres	**T. Clusiana 3372**
Feuilles 2-3, rapprochées ; bulbe glabre ou un peu poilu au sommet ; périanthe à divisions jaunes en dedans, jamais tachées de violet à la base interne ; étamines à filets jaunes, barbus à la base.
Feuilles ordinairement 3 ; périanthe long de 3-5 cm., presque en entonnoir, à divisions barbues au sommet, inégales, les intérieures ovales-lancéolées, les extérieures lancéolées ; capsule obovale ou oblongue, plus longue que large	**T. silvestris 3373**
Feuilles presque toujours 2 ; périanihe long de 2-3 cm., en forme de cloche, à divisions glabres, égales ou presque égales, toutes étroites, lancéolées ; capsule subglobuleuse, presque aussi large que longue ; plante grôle.	**T. australis 3374**