TABLEAU DES ESPÈCES

Cladodes assez petits (1 1/2-3 cm. de long sur 1 1/2-2 cm. de large), terminés en épine très aiguë, portant 1-2 fleurs sur la face supérieure ; tige très rameuse au sommet.	**R. aculeatus 3497**
Cladodes grands (6-12 cm. de long sur 3-4 de large), acuminés mais non épineux au sommet, portant 2-5 fleurs sur la face inférieure ; tige simple ou peu rameuse.	**R. Hypoglossum 3498**