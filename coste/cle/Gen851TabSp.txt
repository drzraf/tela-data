TABLEAU DES ESPÈCES

Epi unilatéral, au moins dans la partie supérieure ; epillets élargis au sommet après la floraison, ovales-oblongs, ordinairement aristés ; glumes inégales, la supérieure de moitié plus longue, très aiguë ainsi que la glumelle inférieure.
N, unilateralis 4236
Epi distique ou cylindracé ; epillets atténués au sommet même après la floraison, ordinairement non aristés ; glumes presque égales, obtuses ou subobtuses ainsi que la glumelle inférieure.
Epi nettement distique, raide, toujours dressé ; epillets assez grands, ovales-obtus, à 
^-9 fleurs ; glumelles égales, l'inférieure non échancrée ; caryopse oblong, adhéreift ; plante généralement de 20-SO cm	**N. Lachenalii 4237**
Epi filiforme-cylindracé, très grêle, souvent arqué ; epillets très petits, cachés dans l'axe, lancéolés, à 3-S fleurs ; glumelles inégales, l'inférieure échancrée au sommet ; caryopse linéaire-trigone, libre ; plante très grêle de 10-30 cm.	**N. Salzmanni 4238**