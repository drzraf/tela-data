TABLEAU DES ESPÈCES

4i Feuilles radicales et caulinaires d'abord planes.
Epillets aristés, à arêtes environ deux fois plus longues que la glumelle bidentée au sommet ; plante élevée, à feuilles larges et ligule très courte.	**F. gigantea 4158**
Epillets mutlques ou à arêtes plus courtes que la glumelle entière ou denticulée au sommet.
X Ligule très courte, réduite à deux petites oreillettes latérales ; glumelle largement scarieuse aux bords ; ovaire glabre.
Souche fibreuse, sans stolons ; panicule étroite, contractée, k rameaux inférieurs courts, portant l'un 1-2 epillets, l'autre plus long 2-5 epillets oblongs, à 5-11 fleurs mutiques ; glume supérieure obtuse.	**F. pratensis 4159**
Souche rampante et courtement stolonifère ; panicule à rameaux de la base portant plusieurs epillets ; ceux-ci ovales ou elliptiques, à 4-7 fleurs mutiques ou brièvement aristées ; glumelle supérieure aiguë.
Panicule grande, oblongue, lâche, à rameaux allongés, étalés à la floraison ; epillets ovales-lancéolés, longs de 10-15 mm., à fleurs souvent aristées ; feuilles vertes, longues, planes ; plante atteignant 1-2 mètres.	**F. arundinacea 4160**
Panicule étroite, linéaire, interrompue, à rameaux courts et dressés ; epillets elliptiques, longs de 5-9 mm., à fleurs ordinairement mutiques ; feuilles glaucescentes, à la fin enroulées ; plante de 30 cm. à 1 mètre.	**F. Fanas 4161**
X Ligule ovale ou oblongue, sans oreillettes latérales ; glumelle étroitement scarieuse aux bords, mutique.
Tiges plus ou moins renflées-bulbeuses à la base ; feuilles radicales glaucescentes, très longues, lisses, à la fin enroulées ; epillets d'un jaune roussâtre mat, grands, ayant 10-16 cm. de long ; glumes largement scarieuses	**F. spadicea 4162**
Tiges nullement renflées au-dessus de la souche rampante ; feuilles vertes, restant planes ; epillets verts, rarement d'un jaune luisant, petits de 
5-7 mm. de long ; glumes étroitement scarieuses aux bords.
Feuilles larges de 5-15 mm., scabres aux bords et sur les gaines ; ligule oblongue, déchirée ; panicule longue de 10-15 cm., à rameaux rudes ; ovaire velu au sommet ; plante d'environ un mètre.	**F. silvatica 4163**
Feuilles étroites (2-3 mm. ), lisses ainsi que les gaines ; ligule ovale souvent tronquée ; panicule courte (5-10 cm.), ovale, à rameaux lisses ; ovaire glabre ; plante grêle de 20-50 cm	**F. pulchella 4164**
4> Feuilles radicales et souvent les caulinaires enroulées-sétacées. t^i Feuilles caulinaires planes ou canaliculées, ou bien souche rampante ; rejets des feuilles stériles naissant tous ou la plupart en dehors des gaines inférieures.
O Feuilles glauques, toutes enroulées-jonciformes, lisses ; epillets grands, atteignant 12-15 mm., à 5-10 fleurs presque toujours poilues ; plante des sables maritimes, à souche longuement traçante, non gazonnante .	**F. arenaria 4165**
Feuilles vertes, les caulinaires planes ou un peu en gouttière ; epillets moins grands, k fleurs presque toujours glabres ; plantes non maritimes, k souche gazonnante.
♦< Souche rampante, émettant des stolons ; feuilles radicales lisses ; arête très courte ou égalant au plus le tiers de la glumelle ; ovaire glabre.
Plante ubiquiste, assez robuste, haute de 30-80 cm. ; panicule longue de 
6-15 cm., rameuse, k rameaux inférieurs géminés, portant 2-8 epillets ; ceux-ci longs de 7-12 mm., oblongs, à 4-8 fleurs aristées.	**F. rubra 4166**
Plante pyrénéenne, très grêle, de 10-30 cm. ; panicule courte (2-3 cm.), étroite, presque simple, k rameaux solitaires, portant 1-3 epillets ; ceux-ci petits (6 mm.), elliptiques, k 3-5 fleurs presque mutiques.	**F. pyrenaica 4167**
4" Souche fibreuse, sans stolons ; feuilles radicales scabres sur les angles ; arête égalant la moitié de la glumelle ou presque aussi longue qu'elle ; ovaire poilu au sommet.
Plante ubiquiste, assez robuste, haute de 50 cm. à 1 mètre ; feuilles caulinaires bien plus larges que les radicales, ayant 2-3 mm., planes ; panicule verdâtre ou un peu violacée ; épillets à 4-8 fleurs.	**F. heterophylla 4168**
Plante alpine, assez grêle, haute de 30-50 cm. ; feuilles caulinaires un peu plus larges que les radicales, ayant à peine 2 mm., presque planes ; panicule d'un violet noirâtre ; épillets à 3-4 fleurs .	**F. nlgricans 4169**
•J< Feuilles caulinaires enroulées-sétacées comme les radicales ; rejets des feuilles stériles sortant presque toujours des gaines inférieures ; souche fibreuse gazonnante.
►> Glumelle inférieure entièrement scarieuse au sommet jusqu' à la nervure ; caryopse poilu au sommet ; ligule plus ou moins saillante, sans oreillettes ; plantes des hautes montagnes.
Panicule d'un vert jaunâtre ; ligule courte, d'environ 1 mm.
Rejets stériles sortant en dehors des gaines inférieures, à feuilles canaliculées, longues, scabres ; épillets à 3-4 fleurs ; glume supérieure égalant presque le sommet de la fleur contiguë ; glumelle inférieure dentéef rangée au sommet, à cinq nervures distinctes .	**F. dimorpha 4170**
Rejets stériles sortant des gaines inférieures, à feuilles étroitement pliées ou enroulées ; épillets à 4-6 fleurs ; glume supérieure égalant au plus les 3/4 de la fleur contiguë ; glumelle inférieure entière au sommet, à peine nervée.
Rejets stériles à gaines enroulées, fendues jusqu' à la base, à 4-6 feuilles très inégales, vertes, molles, acuminées et rudes au sommet, égalant au moins le milieu de la tige ; celle-ci rude au sommet ; glumelles inégales, la supérieure plus courte F. flavesçens 4171
Rejets stériles à gaines tubuleuses, entières ou à peine fendues, à 
7-11 feuilles presque égales, glaucescentes, raides, piquantes, lisses, 5-6 fois plus courtes que la tige ; celle-ci entièrement lisse ; glumelles égales	**F. scoparia 4172**
►I» Panicule panachée de violet, rarement jaunâtre ; ligule ovale ou oblongue ; rejets stériles à 4-6 feuilles, à gaines fendues et enroulées.
Feuilles grosses, ayant 1 ou 1/2 mm. d'épaisseur, jonciformes, très raides, piquantes, lisses, égalant ou dépassant le milieu de la tige ; ligules toutes allongées, de 3-6 mm. ; épillets grands, à 5-8 fleurs mutiques.	**F. Eskia 4173**
Feuilles ayant moins de 1 mm. d'épaisseur, raides ou fines, souvent plus courtes que la moitié de la tige ; ligules inférieures courtes, les supérieures longues de 1-2 mm. ; épillets moins grands, à fleurs mutiques ou aristées.
Tige de 20-50 cm., lisses, rarement scabres au sommet ; feuilles raides, piquantes, lisses, glaucescentes ; épillets à 4-8 fleurs, les plus grandes de 6-8 mm. avec l'arête ; glumelle inférieure insensiblement acuminée, la supérieure un peu scabre sur les carènes.	**F. varia 4174**
Tiges de 10-20 et rarement 30 cm., filiformes, scabres dans le haut ; feuilles fines, sétacées, plus ou moins rudes et vertes ; épillets à 
3-5 fleurs, les plus grandes de 4-5 mm . avec l'arête ; glumelle inférieure brusquement acuminée, la supérieure à carènes fortement ciliées	**F. pumila 4175**
*^ Glumelle inférieure très étroitement scarieuse aux bords vers le sommet ; caryopse presque toujours très glabre ; ligule très courte, réduite à deux petites oreillettes latérales.
44 Gaines des rejets stériles tubuleuses, entières jusque près du sommet ; plantes montagnardes, peu élevées, atteignant rarement 30 cm., à tiges et feuilles lisses.
— Tiges anguleuses au sommet ; feuilles anguleuses, capillaires, ordinairement vertes.
Rejets stériles naissant en dehors des gaines inférieures ; panicule de 
3-5 cm., à rameaux inférieurs géminés, plus longs que les 2-3 épillets ; glumelle inférieure sans nervures distinctes, à arête égalant le tiers ou la moitié de sa longueur	**F. violacea 4176**
Rejets stériles sortant des gaines inférieures ; panicule courte (1 1/2 à 
3 cm.) et dense, à rameaux tous solitaires, plus courts que l'épillet solitaire ; glumelle inférieure à 5 nervures, à arête aussi longue qu'elle	**F. Halleri 4177**
— ïiges cylindriques ou striées au sommet ; feuilles cylindracées ou un peu comprimées par le côté.
Limbes des feuilles sèches caducs, les gaines seules persistantes ; feuilles raides, glaucescentes, subjonciformes, épaisses de 0,7 à 0,8 mm., à 
7 nervures ; épillets à fleurs rapprochées ; glumelle inférieure à cinq nervures	**F. Borderei 4178**
Limbes des feuilles sèches persistants comme les gaines ; feuilles molles, très fines, capillaires, de 0,3 à 0,5 mm. d'épaisseur, à 3-5 nervures ; épillets à fleurs espacées ; glumelle inférieure sans nervures distinctes.
Tiges à deux nœuds, le supérieur placé vers le tiers inférieur ; feuilles glaucescentes ; panicule violacée, longue de 2-4 cm. ; glumes et glumelles lancéolées-acuminées ; arête bien plus courte que la glumelle ; anthères de 1 mm. .	**F. glacialis 4179**
Tiges à nœud unique, placé près de la base ; feuilles vertes, très ténues ; panicule verdâtre, courte (1-3 cm.) ; glumes et glumelles étroites, linéaires-lancéolées en alêne ; arête presque aussi longue que la glumelle ; anthères de 2 à 3 mm.	**F. alpina 4180**
■♦« Gaines des rejets stériles enroulées, fendues jusque près de la base, très rarement tubuleuses jusqu'au milieu ; plantes ubiquistes ou atteignant jusqu' à 40-80 cm., à tiges et feuilles lisses ou scabres.
I Epillets à fleurs mutiques ; feuilles très fines, capillaires, n'ayant que 0,3 à 0,5 mm. d'épaisseur, rudes, vertes.
Tiges assez robustes, hautes de 50-80 cm. ; gaines des rejets stériles tubuleuses jusqu'au milieu, fendues en dessus, avec un sillon au-dessous de la fente ; feuilles très longues, anguleuses ; panicule dé 
8-20 cm., à épillets grands (7-10 mm.), lancéolés.	**F. amethystlna 4181**
Tiges grêles, hautes de 15-50 cm, ; gaines des rejets stériles fendues jusque près de la base, non sillonnées ; feuilles assez longues, cylindracées ; panicule de 3-8 cm., étroite, à épillets petits (4-6 mm.), elliptiques-ovales	**F. capillata 4182**
I Epillets à fleurs aristées ; feuilles séiacées ou subjonciformes, fines ou assez épaisses.
Feuilles cylindracées, sétacées (0,4-0,6 mm. d'épaisseur), rudes, vertes ou glaucescentes ; tiges grêles de 10-50 cm., plus ou moins anguleuses et rudes au sommet	**F. ovina 4183**
Feuilles plus ou moins comprimées par le cùlé, sétacées ou jonciformes 
(0,4 à 1 mm. et plus d'épaisseur), ordinairement lisses et glaucescentes ; tiges un peu raides, égalant 20-70 cm.
Feuilles sillonnées sur les faces à l'état sec, sétacées ou subjonciformes, rudes ou lisses ; tiges sillonuées-anguleusos au sommet ; épillets longs de 5-7 mm., à fleurs de 3-5 mm.. . .	**F. valesiaca 4184**
Feuilles non sillonnées sur les faces, raides, subjonciformes, presque toujours lisses ; tiges cylindracées ou striées au sommet ; épillets longs de C-lOmm., à fleurs de 4-6 mm.. .	**F. duriuscula 4185**