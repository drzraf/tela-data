TABLEAU DES ESPÈCES

Plante vivace, à souche densément gazonnante ; feuilles radicales très nombreuses, fasciculées ; panicule étroitement oblongue, à rameaux assez courts et garnis d'épillets presque jusqu' à leur base C canescens 4044
Plantes annuelles, à racine grêle non gazonnante ; feuilles radicales peu nombreuses, non fasciculées ; panicule ovale-oblongue, à rameaux à la fin allongés et longuement DES à la base .
Epillets longs de 3 mm., en fascicules assez petits, formant une panicule très lâche ; tiges ordinairement dressées dès la ])ase ; arête atténuée en massue filiforme atteignant souvent le sommet des glumes C. fasciculatus 404S
Epillets longs de 4 mm., en fascicules assez gros, formant une panicule assez lâche ; tiges genouillées à la base, ascendantes ; arête brusquement renflée en massue courte n'atteignant pas le sommet des glumes	**C. articulatus 4046**