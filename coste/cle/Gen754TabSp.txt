TABLEAU DES ESPÈCES

Feuilles pédatiséquées, plus larges que longues, découpées en segments lancéolés ou linéaires ; spathe et spadice violets-rougeâtres.
Spathe glabre en dedans, égalant ou dépassant peu le sommet du spadice ; spadice à massue terminale entièrement nue, à fleurs mâles et femelles contiguës et sans filaments stériles entre elles	**A. Dracunculus 3678**
Spathe velue en dedans, d'un quart plus longue que le spadice ; spadice à massue terminale toute garnie de longs filaments, à fleurs mâles séparées des femelles par quelques rangées de filaments . ,	**A. muscivorum 3679**
Feuilles entières, plus longues que larges, hastées ou sagittées ; spadice violet ou jaune.
Spathe d'un violet foncé, d'un quart plus longue que le spadice ; celui-ci à massue d'un pourpre noir ; feuilles ovales-oblongues en cœur, à pétiole égalant environ le limbe	**A. pictum 3680**
Spathe d'un vert jaunâtre ou rougeâtre, 2-3 fois plus longue que le spadice ; celui-ci violacé ou jaune ; feuilles hastées ou sagittées, à pétiole environ 2 fois plus long que le limbe.
Spadice à massue terminale rouge violacée, environ une fois plus courte que son pédicelle ; feuilles ne paraissant qu'après l'hiver, tachées de noir ou entièrement vertes, sans veines blanches	**A. maculatum 3681**
Spadice à massue terminale jaune, aussi longue ou plus longue que son pédicelle ; feuilles paraissant avant l'hiver, plus ou moins marquées de veines blanches en dessus	**A. italicum 3682**