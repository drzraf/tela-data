TABLEAU DES ESPÈCES

Inflorescence formée de fleurs solitaires, la plupart longuement pédicellées, jamais réunies en glomérules ; graines munies au sommet d'un appendice en forme de crête.
Feuilles radicales larges de 8-10 mm. ; rameaux et pédoncules fructifères divariqués ou réfractés ; périanthe à divisions aiguës, un peu plus courtes que la capsule obtuse mucronulée	**L. pilosa 3732**
Feuilles toutes linéaires-étroites (2-4 mm.); rameaux et pédoncules fructifères dressés ou étalés ; périanthe à divisions acuminées, égalant ou dépassant un peu la capsule aiguë et mucronée.
Souche fibreuse, sans stolons ; fleurs brunâtres, 2-S sur chaque rameau ; pédoncules fructifères dressés ou ascendants ; graines terminées par un appendice droit L. Forsteri 37:33
Souclie longuement stolonifère ; fleurs jaunâtres, ordinairement solitaires sur le rameau ; pédoncules fructifères étalés ; graines terminées par un appendice courbé	**L. flavescens 3734**
Inflorescence formée de fleurs réunies en glomérules ou en épis, toutes sessiles ou subsessiles.
= Fleurs blanches ou jaunes, rarement un peu rosées ; souche stolonifère ; graines terminées au sommet par un petit tubercule.
Fleurs jaunâtres, en glomérules mulfiflores denses ; inflorescence à rameaux simples ou presque simples ; feuilles courtes, brièvement acuminées ; plante de 
10-30 cm., entièrement glabre L. lutea 373S
Fleurs blanches ou blanchâtres ; inflorescence à rameaux décomposés ; feuilles allongées, longuement acuminées ; plantes de 20-80 cm., munies de longs poils.
Inflorescence toujours dense, à rameaux dressés en corymbe, bien plus courte que les feuilles florales ; fleurs assez grandes, longues de o-6 mm., 6-20 à chaque glomérule ; anthères aussi longues que leur filet. .	**L. nivea 3736**
Inflorescence devenant lâche, à rameauxplusoumoinsétalés,unpeupluscourteou plus longue que les feuilles florales ; fleurs petites, longues de 3-4 mm., 
2-6 à chaque glomérule ; anthères de moitié plus longues que le filet.
Feuilles toutes linéaires-planés, larges de 3-6 mm. ; inflorescence très décomposée, à rameaux divariqués, un peu plus courte que la bractée involucrale foliacée ; plante de 40-80 cm., un peu raide	**L. albida 3737**
Feuilles radicales inférieures capillaires, les autres linéaires-étroites (1-3 mm.); inflorescence peu divisée, à rameaux ascendants, dépassant ou égalant la bractée membraneuse-sétacée ; plante de20-o0 cm., très grêle,	**L. pedemontana 3738**
= Fleurs brunes, roussâtres ou noirâtres.
Inflorescence lâche, à rameaux 2-4 fois divisés ; fleurs 1-6 à chaque glomérule ; graines terminées au sommet par un petit tubercule.
Feuilles poilues, les radicales nombreuses, larges de 6-12 mm., les caulinaires souvent plus courtes que leur gaine ; inflorescence très décomposée, dépassant beaucoup les feuilles florales, à rameaux divariqués ; souche grosse, un peu rampante	**L. silvatica 3739**
Feuilles glabres ou peu poilues, les radicales peu nombreuses, bien moins larges, les caulinaires plus longues que leurs gaines ; inflorescence à rameaux étalés ou ascendants, à fleurs petites ; souche grêle.
Souche oblique, non stolonifère ; feuilles larges de 4-7 mm., allongées ; inflorescence dépassant peu ou point les feuilles florales, très décomposée, à rameaux étalés ; plante élancée, haute de 30-60 cm.	**L. glabrata 3740**
Souche rampante-stolonifère ; feuilles étroites, peu allongées ; inflorescence dépassant 4-S fois les feuilles florales, moins décomposée, à rameaux étalés-dressés ; plante grêle de 10-30 cm	**L. spadicea 3741**
Inflorescence plus ou moins dense, à rameaux simples ou presque nuls ; fleurs nombreuses en glomérules ou en épi.
^c Inflorescence en ombelle, à plusieurs rameaux plus ou moins allongés et inégaux ; graines appendiculées à la base.
Inflorescence dépassant les feuilles florales, à 2-6 rameaux ; fleurs brunâtres, en glomérules à la fin penchés ; périanlhe à divisions presque égales ; anthères environ 3 fois plus longues que leur filet.	**L. campestris 3742**
Inflorescence égalant ou dépassant les feuilles florales, à 4-10 rameaux ; fleurs roussâtres ou brunes, en glomérules toujours dressés ; périautbe à divisions égales ; anthères égales au filet	**L. multiflora 3743**
Inflorescence longuement dépassée par une feuille florale, à 3-7 rameaux courts ; fleurs noirâtres, en petits glomérules toujours dressés ; périanthe à divisions inégales ; anthères un peu plus courtes que le filet.	**L. sudetica 3744**
Inflorescence contractée en épi oblong, lobule, penché ; graines appendiculées au sommet ou non appendiculées.
Feuilles linéaires très étroites (1-2 mm.), canaliculées, peu poilues ; fleurs noirâtres ou brunes, petites, en épi plus long que les feuilles florales ; plante de 10-30 cm., grêle, à souche gazonnante.	**L. spicata 3745**
Feuilles larges de 4-8 mm., planes, bordées de longs poils ; fleurs bi'unes marbrées de blanc, grandes, en épi dépassé par une feuille florale ; plante de 
20-30 cm., assez robuste, à grosse souche oblique.	**L. pediformis 3746**