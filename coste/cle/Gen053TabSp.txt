TABLEAU DES ESPÈCES

{1} Fleurs jaunes ; filets des étamines dentés ou ailés.
	{2} Plantes annuelles ; pétales dépassant peu les sépales, à limbe dressé, d'un jaune pale, à la fin blanchâtres.
		Sépales persistants ; silicule échancrée, à poils appliqués ; style 10 fois plus court que la silicule	**A. calycinum 259**
		Sépales caducs ; silicule non échancrée, à poils étalés ; style 3-5 fois plus court que la silicule.	**A. campestre 260**
	{2} Plantes vivaces ; pétales 1-2 fois plus longs que les sépales, à limbe étalé, restant jaunes.
		Pétales émarginées ; 2 graines dans chaque loge.
			Grappe fructifère allongée, lâche ; silicules suborbiculaires, échancrées au sommet.	**A. montanum 261**
			Grappe fructifère très courte, en corymbe ; silicules grandes, elliptiques, arrondies aux 2 bouts.	**A. cuneifolium 262**
		Pétales entiers ; 1 graine dans chaque loge.
			Silicules glabres ; tiges assez élevées, dressées, raides, à rameaux rapprochés en panicule.	**A. corsicum 263**
			Silicules poilues ; tiges peu élevées, diffuses ou tortueuses.
				Silicules grandes, rhomboïdales ; graines ailées ; pétales tronqués.	**A. Robertianum 264**
				Silicules petites, oblongues-lancéolées ; graines à peine bordées ; pétales arrondis.	**A. alpestre 265**
{1} Fleurs blanches ; filets des étamines ni dentés, ni ailés.
	Tiges herbacées, étalées-dressées ; feuilles linéaires ; grappes fructifères très allongées.	**A. maritimum 266**
	Tiges ligneuses, entrelacées en buisson ; feuilles obovales ou oblongues ; grappes fructifères courtes.
		{2} Rameaux à la fin transformés en épines.
			Rameaux très épineux, à épines rameuses ; silicules non enflées, arrondies au sommet.	**A. spinosum 267**
			Rameaux peu épineux, à épines simples ; silicules enflées‑vésiculeuses, tronquées au sommet.	**A. macrocarpum 268**
		{2} Rameaux jamais terminés en épines.
			Silicules pubescentes, rhomboïdales, à peine plus longues que le style ; pétales orbiculaires.	**A. pyrenaicum 269**
			Silicules glabres, non rhomboïdales, 3‑5 fois plus longues que le style ; pétales obovales.
				Grappes fructifères assez allongées, lâches ; silicules obovales ; graines non ailées.	**A. Lapeyrousianum 270**
				Grappes fructifères courtes et serrées ; silicules orbiculaires ; graines largement ailées.	**A. halimifolium 271**