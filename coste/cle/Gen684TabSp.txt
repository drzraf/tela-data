TABLEAU DES ESPÈCES

Feuilles paraissant avec les fleurs, lancéolées-linéaires, larges seulement de 2-5 mm. ; anthères d'un pourpre noirâtre ; styles droits, dépassant un peu les étamines.	**C. Bertolonii 3363**
Feuilles ne paraissant qu'après les fleurs, larges de plus de S mm. ; anthères jaunes.
Styles droits ou à peine arqués au sommet, ordinairement plus courts que les étamines toutes insérées à la même hauteur ; fleurs solitaires, relativement assez petites.
Stigmates très courts, presque en tète ; capsule petite, ovale, assez longuement mucronée, entourée ordinairement par 2 feuilles ; hulbe petit (2 cm. de long sur 1 de large)	**C. alpinum 3364**
Stigmates allongés en massue ; capsule plus grande, ovale-oblongue, brusquement contractée en mucron court, ordinairement entourée par 2 feuilles ; bulbe plus gros	**C. corsicum 3363**
Styles recourbés ou arqués au sommet, égalant ou dépassant plus ou moins longuement les étamines ; fleurs solitaires ou fasciculées par 2-S, grandes.
Stigmates arqués ou un peu courbés ; étamines toutes insérées à la même hauteur ; capsule elliptique, de la grosseur d'une noisette, entourée ordinairement par 
3 feuilles linéaires-lancéolées obtuses ; bulbe de 2-3 cm. de diam.	**C. neapolitanum 3366**
Stigmates recourbés en crochet ; les 3 étamines longues insérées plus haut que les 
3 étamines courtes ; capsule obovale, grosse comme une noix, entourée ordinairement par plus de 3 feuilles largement lancéolées ; bulbe plus gros.	**C. autumnale 3367**