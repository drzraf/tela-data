TABLEAU DES ESPÈCES

Tiges feuillées jusqu'au sommet, florifères parfois dès la base ; fruit très gros, atteignant 6-8 mm., à disque relevé en coupe évasée, à lobes à la fin rigides et dressésétalés ; plante annuelle ou bisannuelle, à racine grêle. . .	**B. macrocarpa 3082**
Tiges terminées en épis nus, florifères seulement dans le haut ; fruit plus petit, à disque non évasé en coupe, à lobes du périanthe concaves et appliqués ; racine souvent épaisse.
Plante annuelle ou bisannuelle, à tige ordinairement solitaire, robuste, dressée ; feuilles radicales grandes, ovales en cœur, obtuses, à côtes charnues ; panicule grande, à rameaux dressés ; stigmates ovales	**B. vulgaris 3083**
Plante ordinairement vivace, à plusieurs tiges presque toujours couchées ou ascendantes, faibles ; feuilles inférieures moins grandes, ovales-rhomboïdales, aiguës, à nervures non charnues ; panicule à rameaux étalés ; stigmates lancéolés.	**B. maritima 3084**