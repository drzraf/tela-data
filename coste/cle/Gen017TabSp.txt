TABLEAU DES ESPÈCES

Fleurs jaunâtres.
	Feuilles a lobes linéaires, décurrents ; casque large, convexe ; 5 carpelles, velus.	**A. Anthora 104**
	Feuilles à 5-7 lobes larges, en coin ; casque allongé, conique ; 3 Carpelles, glabres.	**A. lycoctonum 105**
Fleurs bleues, très rarement blanches.
	Feuilles à lobes étroits, linéaires ou lancéolés ; rameaux et pédicelles dressés.	**A. Napellus 106**
	Feuilles à lobes larges, rhomboïdaux ; rameaux et pédicelles étalés-divariqués.	**A. paniculatum 107**