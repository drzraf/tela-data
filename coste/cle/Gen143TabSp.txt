TABLEAU DES ESPÈCES

Fleurs petites, globuleuses dans le bouton, en épis grêles et un peu lâches ; disque hypogyne à 10 angles obtus, séparés par des sinus portant les étamines saillantes ; capsule pyramidale, longuement atténuée.	**T. gallica 728**
Fleurs petites, ovoïdes dans le bouton, en épis grêles et un peu lâches ; disque hypogyne à 5 angles aigus sur lesquels sont insérées les étamines saillantes, à anthères apiculées ; capsule brusquement contractée au‑dessous de la base ; feuilles à bords opaques.	**T. anglica 729**
Fleurs assez grandes, ovoïdes dans le bouton, en épis épaissis ; disque hypogyne à 5 angles aigus portant les étamines non saillantes, à anthères mutiques ; capsule insensiblement atténuée sommet ; feuilles membraneuses‑transparentes aux bords.	**T. africana 730**