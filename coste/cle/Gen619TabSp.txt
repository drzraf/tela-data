TABLEAU DES ESPÈCES

Feuilles très étroites, larges à peine de 1 mm. ; pèrianthe fructifère muni d'ailes membraneuses plus longues que son disque.
Sous-arbrisseau de 30-80 cm., à souche grosse et ligneuse, à tiges nombreuses, décombantes ou redressées, à rameaux ascendants ; pèrianthe fructifère brièvement pubescent, à ailes demi-orbiculaires	**K. prostrata 3103**
Plante annuelle de 10-30 cm., à racine grêle, à tige unique, dressée, ordinairement rameuse dès la base, à rameaux très étalés ; pèrianthe fructifère densément laineux, à ailes oblongues-spatulées	**K. arenaria 3104**
Feuilles caulinaires larges de plus de 2 mm., pèrianthe fructifère muni de tubercules, d'épines ou d'ailes courtes ; plantes annuelles.
Plante de 40 cm. à 1 mètre, raide, très rameuse à rameaux dressés ; feuilles caulinaires presque glabres, ciliées, lancéolées, à 3 nervures ; pèrianthe fructifère muni de tubercules triangulaires aigus ou d'ailes courtes. .	**K. scoparia 3103**
Plantes de 20-GO cm., dressées ou décombantes, à rameaux étalés ; feuilles plus ou moins velues, linéaires ou linéaires-lancéolées, à 1 nervure ; pèrianthe fructifère muni d'appendices coniques ou spiniformes.
Plante décombante ou ascendante, toute velue-tomenteuse, d'un vert cendré ; feuilles charnues, linéaires-obtuses, demi-cylindriques ; rameaux très flexueux ou contournés en spirale ; appendices fructifères brièvement coniques.
K.hirsuta 310G
Plante dressée, brièvement pubescente, verte ; feuilles minces, planes, lancéoléeslinéaires, aiguës ; rameaux raides, jamais contournés en spirale ; appendices fructifères eu forme d'épines subulées, non dilatées à la base.	**K. hyssopifolia 3107**