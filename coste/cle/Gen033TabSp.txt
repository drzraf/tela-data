TABLEAU DES ESPÈCES

Siliques écartées de l'axe, valves à 3-5 nervure.
	Bec conique en alène, plus court que la silique ; feuilles supérieures sessiles, sinuées-dentées.	**S. arvensis 153**
	Bec comprimé en sabre, plus long que la silique ; feuilles toutes pétiolées et pennatifides.
		Feuilles lyrées-pennatifides, à lobes oblongs ; tiges, feuilles et siliques hérissées.	**S. alba 154**
		Feuilles pennatipartites, découpées en lobes lancéolés ; tiges, feuilles et siliques glabrescentes.	**S. dissecta 155**
Siliques serrées contre l'axe ; valves à 1 nervure ou sans nervures.
	Feuilles supérieures sessiles ; rameaux élancés ; valves de la silique sans nervures apparentes.	**S. pubescens 156**
	Feuilles toutes pétiolées ; rameaux divariqués ; valves à 1 nervure bien marquée.
		Siliques cylindracées, à bec renflé au milieu en une boule monosperme ; graines ovoïdes.	**S. incana 157**
		Siliques presque quadrangulaires, en bec conique, sans graine au milieu ; graines globuleuses.	**S. nigra 158**