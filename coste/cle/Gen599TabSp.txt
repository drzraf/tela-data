TABLEAU DES ESPÈCES

ti* Feuilles profondément divisées jusqu'au del à du milieu.
Feuilles à 3-3 segments linéaires, mucronés, entiers ; fleurs blanches ou rosées, 
2 par verticille ; calice en cloche, non bossu à la base, à dents lancéolées en alêne, bien plus longues que le tube ; plante vivace, ligneuse à la base.
T. Pseudochamaepitys 2973
Feuilles bipennatifides, à segments oblongs-lancéolés obtus ; fleurs purpurines, 
4-6 par verticille ; calice tubuleux-enflé, bossu à la base, à dents triangulaires-aiguës, bien plus courtes que le tube ; plante annuelle, herbacée.
T. Botrys 2974 tii Feuilles entières, crénelées ou dentées, jamais divisées jusqu'au milieu.
X Fleurs axillaires, non disposées en épis ni en têtes terminales ; feuilles florales semblables aux autres, dépassant longuement les fleurs ; plantes molles, herbacées, à souche stolonifère.
Feuilles caulinaires oblongues, non embrassantes, les raméales atténuées et entières à la base, dentées au sommet ; plante très rameuse, pubescente ou velue, à stolons ordinairement fohifères T. Scordium 2975
Feuilles caulinaires larges, échancrées en cœur à la base et embrassantes, les raméales arrondies à la base, dentées tout autour ; plante peu rameuse, raide, velue-soyeuse, à stolons ordinairement écailleux.
T. scordioides 2976
X Fleurs en grappes spiciformes ou en tètes terminales ; feuilles florales plus petites ou bractéiformes, plus courtes que les fleurs supérieures ; plantes raides, souvent ligneuses, sans stolons, cf Fleurs en grappes spiciformes terminales, plus ou moins unilatérales, w Feuilles entières, à bords enroulés en dessous. Alertes en dessus, blanchestomenteuses en dessous ainsi que les rameaux.
Arbrisseau égalant ou dépassant 1 mètre, à rameaux étalés ; feuilles assez grandes (2-4 cm. de long sur environ 1 de large, à nervures secondaires saillantes ; fleurs bleues, 2 par verticille, en grappes un peu lâches ; inflorescence blanche-tomenteuse.	T. fruticans 2977
Sous-arbrisseau de 20-50 cm., à rameaux dressés ; feuilles petites (1 cm. de long sur 2-d mm. de large), à nervure médiane seule saillante ; fleurs purpurines, 2-4 par verticille, en grappes serrées ; inflorescence velue laineuse T. Marum 2978 y=y Feuilles crénelées ou dentées, à bords non enroulés en dessous, vertes ou cendrées, jamais blanches, ainsi que les rameaux.
> Calice bilabié, nettement bossu à la base, à dent supérieure bien plus large, ovale-suborbiculaire ; feuilles ridées-réticulées, crénelées ; tiges herbacées.
Corolle jaunâtre, à tube 1-2 fois plus long que le calice ; calice presque glabre, vert, nettement veiné en réseau ; feuilles échancrées en cœur à la base, d'un beau vert en dessus, plus pâles en dessous ; plante velue ou pu])escente T. Scorodonia 2979
Corolle rose, petite, à tube inclus ; calice pubescent-glanduleux, cendré, faiblement veiné en réseau ; feuilles tronquées ou arrondies à la base, d'un vert grisâtre, tomenteuses-cendrées en dessous, ainsi que toute la plante T. massiliense 2980
> Calice non bilabié, à peine bossu à la base, à dents presque égales, lancéolées ; feuilles simplement nervées ; tiges plus ou moins ligneuses à la base.
Fleurs jaunâtres, en longues grappes munies de bractées non conformes aux feuilles ; calice d'un vert jaunâtre, velu-laineux ; corolle à lobes supérieurs et latéraux tronqués ou obtus ; feuilles et rameaux pubescents-veloutés, cendrés T. flavum 2981
Fleurs purpurines, en grappes feuillées inférieurement ; calice rougeâtre, pubescent ou glabre ; corolle à lobes supérieurs et latéraux aigus ; feuilles et rameaux poilus ou glabres, verts ou rougeâtres.
Tiges, feuilles et calices glabres ; grappe allongée, lâche, interrompue ; feuilles florales supérieures petites, presque bractéif ormes ; feuilles assez grandes (3 cm. de long sur 2 de large), ovales-rhomboïdales, incisées-dentées ; plante haute de 20-60 cm. . T. lucidum 2982
Tiges, feuilles et calice poilus ; grappe peu allongée, oblongue, assez dense ; feuilles florales presque conformes aux caulinaires ; feuilles assez petites (2 cm. de long sur 1 de large), ovales ou oblongues, fortement crénelées ; plante gazonnante de 10-30 cm.
T. Chamœdrys 2983 d* Fleurs en têtes terminales globuleuses ou ovales, jamais unilatérales.
Q Feuilles vertes en dessus, planes ou peu enroulées, les supérieures entourant les fleurs ; calice verdâtre, non tomenteux ; corolle caduque après la floraison ; tiges grêles, couchées, sans poils rameux.
Feuilles suborbiculaires, fortement crénelées, plurinervées, molles, vertes et velues sur les 2 faces ; calice hérissé, à dents presque aristées ; corolle à lobes supérieurs pourpres, l'inférieur d'un blanc jaunâtre ; rameaux mollement velus T. pyrenaicum 2984
Feuilles linéaires ou lancéolées, entières, uninervées, coriaces, blanchestomenteuses en dessous ; calice glabre, à dents acuminées ; corolle d'un blanc un peu jaunâtre ; rameaux tomenteux- blanchâtres.
T. montanum 298S
Feuilles tomenteuses-grisâtres ou blanchâtres sur les 2 faces, enroulées, les supérieures rapprochées ou écartées des fleurs ; calice très velu ou tomenteux ; corolle persistante ; tiges ligneuses, ascendantes ou dressées, blanches-tomenteuses .
=: Capitules et sommet de la plante d'un jaune doré ou blanchâtres, fortement velus-tomenteux, à poils simples longs et étalés ; calice couvert de longs poils étalés, à dents aiguës, carénées sur le dos ; corolle jaune ou blanchâtre, à lobes supérieurs velus-hérissés. , T. aureum 2986
Capitules et sommet de la plante jamais jaunes, couverts d'un duvet blanc, court et appliqué ; calice densément et brièvement tomenteux, à dents subaiguës ou obtuses, non carénées ; corolle blanche, rarement purpurine, à lobes supérieurs pubescents.
Capitules assez grands, ayant au moins 1 cm. de diamètre ; calice à dents inférieures brièvement aiguës ; étamines ne se roulant pas sur ellesmêmes ; feuilles linéaires-oblongues ou oblongues, crénelées dans le haut ; tiges robustes et trapues T. Polium 2987
Capitules petits, n'ayant pas 1 cm. de diamètre ; calice à dents toutes obtuses et concaves ; étamines à filets à la fin roulés en spirale ; feuilles linéaires ou linéaires-lancéolées, entières ou à peine crénelées au sommet ; tiges grêles et élancées. . T. capitatum 2988