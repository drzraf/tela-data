TABLEAU DES ESPÈCES

Tige glabre ou à poils rares et étalés, hispide a la base ; ombelles ordinairement à 3 rayons ; styles égalant le stylopode ; calice à dents lancéolées, subobtuses ; fruit gros, ellipsoïde, à côtes secondaires armées d'un seul rang d'aiguillons robustes et crochus.	**C. daucoides 1478**
Tige et rameaux couverts de poils appliqués ; ombelles ordinairement à 2 rayons ; stigmates sessiles sur le stylopode ; calice à dents linéaires-acuminées ; fruit petit, linéaire-oblong, à côtes armées de 2-3 rayons d'aiguillons grêles et droits.	**C. leptophylla 1479**