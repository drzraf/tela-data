TABLEAU DES ESPÈCES

Plante vivace, à souche ligneuse ; sépales à 5‑7 nervures atteignant presque leur sommet ; 8 étamines, à filets égalant la moitié des sépales.	**B. perennis 513**
Plantes annuelles, à racine grêle ; sépales à 3‑5 nervures ; 2‑4 étamines.
	Sépales à 5 nervures dont 3 plus saillantes atteignant presque le sommet ; 4 étamines, à filets égalant le quart des sépales ; graines grosses, fortement tuberculeuses.	**B. macrosperma 514**
	Sépales à 3 nervures confluentes bien au‑dessous du sommet ; 2‑3 étamines, à filets égalant le sixième des sépales ; graines petites, à peine tuberculeuses.	**B. tenuifolia 515**