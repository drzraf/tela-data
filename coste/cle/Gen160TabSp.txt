TABLEAU DES ESPÈCES

{1} Fleurs roses ou purpurines, rarement blanches.
	{2} Plantes vivaces, ligneuses à la base ; corolle dépassant le calice.
		Pédoncules pluriflores, articulés ; gousses renflées, 4‑13 fois plus longues que le calice.
			Tiges herbacées ; feuilles pétiolées, à folioles orbiculaires ou ovales, crénelées, la terminale longuement pétiolulée ; fleurs 2‑3 sur des pédoncules axillaires.	**O. rotundifolia 792**
			Tiges ligneuses ; feuilles subsessiles, à folioles toutes oblongues, sessiles, fortement dentées en scie ; fleurs en panicule terminale.	**O. fruticosa 793**
		Pédoncules uniflores ; gousses dépassant au plus 1 fois le calice.
			Pédoncules plus longs que la feuille, articulés ; calice à lobes à peine plus longs que le tube ; gousse un peu renflée, 1 fois plus longue que le calice.	**O. cenisia 794**
			Pédoncules très courts, égalant à peine le tube du calice, non articulés ; calice à lobes bien plus longs que le tube ; gousse comprimée, dépassant peu ou point le calice.
				Souche verticale, non traçante ; tiges dressées ou ascendantes, non radi­cantes, toujours épineuses, à épines souvent géminées ; folioles linéaires-­oblongues ; gousse égalant ou dépassant le calice.	**O. campestris 795**
				Souche longuement rampante ; tiges couchées ou redressées, souvent radi­cantes, inermes ou à épines solitaires ; folioles obovales ou oblongues ; gousse plus courte que le calice.	**O. repens 796**
	{2} Plantes annuelles, à racine pivotante ; corolle égalant ou dépassant peu le calice.
		Tiges de 20‑60 cm., robustes, dressées ; calice tubuleux.
			Feuilles trifoliolées, pétiolées, à pétiole plus long que les stipules rapprochées en gaine embrassante ; calice à lobes égalant le tube ; gousse égalant le calice, à 3‑4 graines.	**O. mitissima 797**
			Feuilles simples, sessiles sur la gaine formée par les stipules connées ; calice à lobes 2 fois plus longs que le tube ; gousse égalant la moitié du calice, à 1‑2 graines.	**O. alopecuroides 798**
		Tiges de 5‑35 cm., grêles, étalées‑diffuses ; calice en cloche, à lobes bien plus longs que le tube.
			Folioles elliptiques‑oblongues, fortement dentées en scie ; pédoncules très courts, non articulés ; corolle dépassant le calice ; gousse ovale, à 2 graines grosses.	**O. serrata 799**
			Folioles obovales en coin, dentées au sommet ; pédoncules longs, articulés et courbés au sommet ; corolle petite, ne dépassant pas le calice ; gousse cylindracée, pendante, à 10‑12 graines petites.	**O. reclinata 800**
{1} Fleurs jaunes, à étendard souvent strié de pourpre.
	{2} Plantes annuelles, d'un vert pâle, à racine pivotante.
		{3} Gousse longuement saillante ; petites plantes de 5‑25 cm.
			Feuilles trifoliolées, pétiolées, à pétiole 3‑4 fois plus long que les stipules ovales‑entières ; fleurs petites, en grappes feuillées, à pédoncules longs, articulés, aristés ; gousse pendante, linéaire, bosselée.	**O. ornithopodioides 801**
			Feuilles la plupart simples, subsessiles, à stipules grandes, engainantes, denticulées ; fleurs assez grandes, en grappes non feuillées, à pédoncules courts, non articulés, ni aristés ; gousse oblongue, non bosselée.	**O. variegata 802**
		{3} Gousse incluse ou brièvement saillante ; plantes de 15‑40 cm., à feuilles moyennes seules trifoliolées.
			Pédoncules non aristés, plus courts que la feuille ; pédicelle épais, très court ; calice à lobes lancéolés, à 5‑7 nervures, égalant environ la corolle ; gousse ovale‑rhomboïdale, plus courte que le calice, à 2‑3 graines.	**O. pubescens 803**
			Pédoncules aristés, filiformes ; pédicelle 2 fois plus long que le tube du calice ; calice à lobes linéaires, à 3 nervures ; gousse linéaire‑oblongue, 1 fois plus longue que le calice, à plusieurs graines. Pédoncules plus longs que la feuille, à arête 1‑2 fois plus longue que le pédicelle ; fleurs assez petites, à corolle dépassant un peu le calice à lobes linéaires acuminés ; graines en rein, tuberculeuses.	**O. viscosa 804**
Pédoncules très ténus, égalant à peine la feuille, à arête 2‑3 fois plus longue que le pédicelle ; fleurs petites, à corolle d'un tiers plus courte que le calice à lobes sétacés ; graines globuleuses, chagrinées.	**O. breviflora 805**
	{2} Plantes vivaces, vertes, à souche ligneuse.
		{3} Pédoncules longs, égalant ou dépassant la feuille, articulés sous le sommet ; corolle et gousse plus longues que le calice.
			Tiges ligneuses, tortueuses ; feuilles trifoliolées, à folioles orbiculaires ; fleurs en grappes terminales, interrompues, non feuillées ; gousse dépassant peu le calice, à 2 graines.	**O. aragonensis 806**
			Tiges herbacées, non tortueuses ; feuilles florales simples, les autres à folioles obovales ou oblongues ; fleurs en grappes feuillées ; gousse longuement saillante, à plusieurs graines.
				Fleurs grandes, à corolle 1 fois plus longue que le calice ; pédoncules égalant ; ou dépassant les feuilles ; stipules ovales‑lancéolées ; plante de 30‑50 cm., rameuse et visqueuse.	**O. Natrix 807**
				Fleurs assez petites, à corolle d'un tiers seulement plus longue que le calice ; pédoncules souvent 1 fois plus longs que les feuilles ; stipules lancéolées‑linéaires ; plantes de 20‑30 cm., très rameuse et très visqueuse.	**O. ramosissima 808**
		{3} Pédoncules presque nuls, bien plus courts que les feuilles florales, non articulés, ni aristés ; corolle et gousse dépassant peu ou point le calice.
			Fleurs en petites ombelles ; pédoncules plus longs que le tube du calice ; corolle dépassant un peu le calice ; gousse à 2 graines ; plante couchée‑stolonifère.	**O. striata 809**
			Fleurs en grappes spiciformes ; pédoncules plus courts que le tube du calice corolle plus courte que le calice ; gousse à 3‑6 graines ; plantes dressées oui ascendantes, sans stolons.
				Foliole terminale pétiolulée ; stipules ovales‑lancéolées, bien plus courtes que le pétiole ; calice à lobes lancéolés‑linéaires, égalant ou dépassant peu la gousse pubescente.	**O. Columnae 810**
				Folioles toutes sessiles ; stipules linéaires en alène, bien plus longues que le pétiole ; calice à lobes linéaires en alène, dépassant longuement la gousse glabre.	**O. minutissima 811**