TABLEAU DES ESPÈCES

Feuilles triangulaires-sagittées, atténuées au sommet, à oreillettes divariquées et insensiblement acuminées, à environ 10 nervures principales ; fleurs monoïques, les supérieures mâles plus nombreuses, larges de 15-20 mm. .	**S. sagittœfolia 3348**
Feuilles largement ovales-sagittées, arrondies au sommet, à oreillettes à peine divergentes et brusquement acuminées, à environ 15 nervures principales ; fleurs dioïques, toutes mâles chez nous, grandes, atteignant 30 cm. de diam.	**S. obtusa 3349**