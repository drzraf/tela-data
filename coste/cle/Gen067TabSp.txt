TABLEAU DES ESPÈCES

Tiges ligneuses, tortueuses, couchées‑diffuses.
	Tiges florifères latérales, glabres ; feuilles planes, linéaires‑oblongues ; sépales blancs‑scarieux aux bords.	**I. sempervirens 304**
	Tiges florifères terminales, pubescentes ; feuilles demi‑cylindriques, charnues ; sépales colorés aux bords.	**I. saxatilis 305**
	Tiges herbacées, dressées ou ascendantes.
	Grappes fructifères plus ou moins lâches ; pédicelles naissant à des hauteurs différentes.
		Feuilles oblongues ou spatulées, dentées ; grappe fructifère à la fin allongée et lâche.	**I. amara 306**
		Feuilles de la tige linéaires‑lancéolées, très entières ; grappe fructifère courte et assez serrée.	**I. intermedia 307**
	Grappes fructifères courtes, serrées, en corymbe ou en ombelle ; pédicelles naissant presque tous à la même hauteur.
		Tiges assez élevées, raides, dressées ; feuilles minces ; plantes des basses montagnes.
			Feuilles pennatiséquées, à 2‑5 lobes linéaires‑obtus ; silicules presque carrées, à lobes obtus.	**I. pinnata 308**
			Feuilles entières ou denticulées ; silicules à lobes aigus.
				Feuilles ciliées, linéaires‑spatulées, très obtuses et entières ; fleurs blanches.	**I. ciliata 309**
				Feuilles glabres, lancéolées ou linéaires, subaiguës ; fleurs purpurines.
					Feuilles inférieures lancéolées, denticulées ; silicules ovales, élargies a sommet, à lobes longs et dressés.	**I. umbellata 310**
					Feuilles de la tige, toutes linéaires ; entières ; silicules petites, suborbiculaires rétrécies, à lobes courts et divergents.	**I. linifolia 311**
		Tiges basses, de 2‑15 cm.; feuilles épaisses ou charnues ; plantes des hautes montagnes.
			Silicules élargies au sommet, à lobes divergents, aigus, séparés par un large sinus obtus.
				Feuilles de la tige linéaires‑spatulées, obtuses ; style égalant ou dépassant peu les lobes de la silicule.	**I. Tenoreana 312**
				Feuilles de la tige linéaires‑lancéolées, aiguës ; style dépassant longuement les lobes de la silicule.	**I. aurosica 313**
				Silicules rétrécies au sommet, à lobes rapprochés, séparés par un sinus étroit et très aigu.
				Feuilles linéaires‑oblongues, peu épaisses, dentées ; silicules petites, à peine dépassées par le style.	**I. Bernardiana 314**
				Feuilles obovales ou spatulées, charnues, entières ; silicules assez grandes, longuement dépassées par le style.
					Feuilles ciliées, les inférieures suborbiculaires ; silicules bien plus étroite au sommet qu'à la base.	**I. spathulata 315**
					Feuilles glabres, les inférieures obovales‑oblongues ; silicules aussi larges au sommet qu'à la base.	**I. Candolleana 316**