TABLEAU DES ESPÈCES

Fruits glabres, pédicellés, lâchement groupés ou solitaires sur des pédicellés partant des pétioles ; plante glabre	**M. quadrifoliata 4320**
Fruits pubescents, sessiles, très rapprochés, distiques, appliqués sur le rhizome ; plante pubescente	**M. pubescens 4321**