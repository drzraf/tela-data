TABLEAU DES ESPÈCES

4» Eperon très court (1-3 mm.), en forme de sac, égalant au plus le tiers de l'ovaire ; divisions extérieures du périanthe plus ou moins conniventes en casque.
Fleurs grandes, en gros épis larges de 4-6 cm. ; bractées environ 2 fois plus longues que l'ovaire ; rétinacles soudés en un seul renfermé dans une bursicule uniloculaire ; plantes robustes.
Fleurs verdâtres rayées et ponctuées de pourpre, à odeur de bouc, en épi long et un peu lâche ; divisions du périanthe toutes conniventes en casque ; labelle très allongé (4-6 cm.), a 3 lanières linéaires tordues en spirale, la moyenne 3-4 fois plus longue que lefe latérales.
0. hircina 3o82
Fleurs roses ou pourpres, à odeur d'Iris, en épi ample et serré ; divisions latérales externes un peu ouvertes et étalées ; labelle 2 fois plus long que les divisions, à 3 lobes plans, les latéraux lancéolés en faux, le moyen un peu plus long, en cœur renversé à 2 lobes divariqués.
0. longibracteata 3o83
Fleurs petites ou très petites, en épis étroits (1-2 cm.) ; bractées plus courtes ou plus longues que l'ovaire ; 2 rétinacles libres ; plantes grêles ou peu robustes.
Bractées égalant ou dépassant l'ovaire ; rétinacles dépourvus de bursicule ; tubercules palmés ou comme fascicules.
Fleurs verdâtres, assez petites, à odeur de prune, en épi un peu lâche ; labelle 1-2 fois plus long que le casque, pendant, linéaire, di\isé au sommet en 3 dents, la moyenne plus courte ; tubercules lobés au sommet et palmés 0. viridis 3384
Fleurs blanchâtres, peu ou point odorantes, très petites, en épi serré très étroit ; labelle égalant environ le casque, petit, trifide, à lobes divergents, le moyen un peu plus grand ; tubercules divisés presque jusqu' à la base et fascicules 0. albida 3o8o 
Bractées plus courtes que l'ovaire ; rétinacles renfermés dans une bursicule à 
2 loges ; tubercules entiers, ovoïdes ou subglobuleux.
Fleurs d'un rose pâle ou carné, à odeur de vanille, en épi unilatéral très serré ; divisions extérieures du périanthe ovales-lancéolées, soudées inférieurement, un peu gibbeuses à la base ; labelle un peu plus court que le casque 0. Intacta 3.o8G
Fleurs d'un pourpre noirâtre au sommet, à odeur agréable, en épi conique serré ; divisions extérieures du périanthe ovales, libres jusqu' à la base, non gibbeuses ; labelle plus long que le casque . . 0. ustulata 3587
4" Éperon plus ou moins allongé, conique ou linéaire, égalant au moins la moitié de l'ovaire, très rarement le tiers seulement.
Divisions extérieures du périanthe toutes conniventes en casque ; tubercules entiers, ovoïdes ou subglobuleux.
A Bractées minuscules, en forme d'écaillés, beaucoup plus courtes que l'ovaire ; labelle tripartit, à lobe moyen profondément bifide avec une dent dans l'échancrure ; plantes élevées, atteignant 30-80 cm.
J> Casque d'un pourpre noir, courtement ovoïde-aigu ; lobes latéraux du labelle liuéaires-oblongs, rapprochés du lobe moyen ; celui-ci dilaté dès la base, divisé au sommet en 2 lobules 4-6 fois plus larges que les lobes latéraux 0. purpurea 3588
J> Casque d'un rose clair ou cendré, ovoïde-acuminé ; lobes latéraux du labelle linéaires, écartés du lobe moyen ; celui-ci étroit à la base, puis divisé en 
2 lobules linéaires ou oblongs.
Lobe moyen du labelle dilaté au sommet et divisé en 2 lobules divergents, 
2-3 fois plus larges et au moins 2 fois plus courts que les latéraux ; épi floral oblong, peu serré 0. militaris 3589
Lobe moyen du labelle non dilaté, mais divisé en 2 lobules linéaires filiformes, arqués-recourbés en avant, aussi étroits et à peu près aussi longs que les lobes latéraux ; épi floral courtement ovoïde, serré.
0. Simia 3590
A Bractées oblongues ou lancéolées-linéaires, égalant presque ou dépassant l'ovaire ; labelle trifide, trilobé ou entier ; plantes de 10-40 cm.
-\- Labelle trifide ou nettement trilobé, à lobe moyen dépassant les latéraux ; casque à divisions soudées intérieurement ; fleurs en épi serré.
Fleurs d'un rose pâle ou blanchâtres, presque inodores ; divisions extérieures du périanthe divergentes au sommet ; labelle trifide, à lobe moyen plus grand et bifide ou tronqué-émarginé, avec une dent dans l'échancrure.
Fleurs rose lilas, en épi d'abord subglobuleux ; casque à divisions brièvement acuminées ; labelle à lobes latéraux linéaires-spatules, à lobe moyen obovale en coin et échancré-bilobé ; tige grêle et élancée.
0. tridentata 3591
Fleurs rose très pâle ou blanchâtres, en épi d'abord ovoïde-conique ; casque à divisions longuement acuminées ; labelle à lobes latéraux obovales-obliques, à lobe moyen en éventail tronqué-denticulé ou émarginé ; tige basse et trapue 0. lactea 3592
Fleurs d'un rouge brun ou vineux, odorantes ; divisions du périanthe toutes conniventes jusqu'au sommet ; labelle trilobé, à lobes peu inégaux, le moyen oblong, aigu, entier.
Fleurs rouge brun sale, à forte odeur de punaise, en épi oblong étroit assez serré ; bractées à peine membraneuses ; divisions du périanthe brièvement acuminées ; lobe moyen du labelle dépassant peu les latéraux . 0. coriophora 3593
Fleurs rouge A'ineux plus clair, à odeur agréable de vanille, en épi ovale-oblong très serré ; bractées menibrancuses-blancbàtrcs ; divisions du périanthe longuement acuminées ; lobe moyen du labelle dépassant sensiblement les latéraux 0. fragrans 3^94
+ Labelle obscurément trilobé ou non divisé, à lobe moyen plus petit que les latéraux ou nul ; casque à divisions libres ; fleurs en épi lùclie.
^" Fleurs grandes, d'un rouge écarlate ; bractées plus longues que l'ovaire, multinervées ; casque allongé, lancéolé ; labelle simple, suborbiculaire en éventail, étalé ; éperon dirigé en bas et presque pendant.
0. papilionacea 3b95
1^ Fleurs moyennes ou petites, non d'un rouge écarlate ; bractées égalant à peine l'ovaire, à 1-3 nervures ; casque court, obtus, subglobuleux ; labelle trilobé, à lobes latéraux plus grands réfléchis ; éperon ascendant ou horizontal.
Labelle à peine plus long que le casque, concolore, pourpre- violet, rosé ou blanc, ponctué de pourpre ; éperon presque droit, cylindrique ou en massue ; épi court, ovale, peu fourni.	0. Morio 3396
Labelle bien plus long que le casque, discoloro, les lobes latéraux d'un violet foncé, le moyen blanc ponctué de pourpre et très courl ; éperon courbé, long, comprimé-spatulé ; épi oblong-cylindrique.
0. longicornu 3397
DiAÙsions extérieures du périanthe plus ou moins étalées ou redressées, au moins les 2 latérales ; tubercules entiers ou palmés.
»!» Divisions extérieures d'abord rapprochées des intérieures et un peu conniventes, puis les latérales plus ou moins étalées ; éperon environ de moitié plus court que l'ovaire.
Fleurs d'un violet clair ou lilas, petites, nombreuses, en épi subglobuleux très serré ; divisions extérieures longuement cuspidées ; éperon grêle, cylindracé ; tige munie de gaines à la base ; tubercules oblongs, parfois incisés au sommet 0. globosa 3398
Fleurs purpurines un peu verdâtres en dedans, assez grandes, peu nombreuses, en épi cylindrique assez lâche ; divisions extérieures obtuses, non cuspidées ; éperon épais, conique obtus ; tige munie de 4-6 feuilles à la base ; tubercules ovoïdes entiers 0. Spitzelii 3399 ij* Divisions extérieures latérales nettement étalées ou redressées en forme d'ailes ; éperon souvent aussi long ou plus long que l'ovaire.
= Labelle simple, entier ou seulement crénelé au bord.
]§[ Fleurs d'un poupre foncé, peu nombreuses ; labelle suborbiculaire ou obovale en coin, crénelé ; éperon épais, dirigé en bas, 1-2 fois plus court que l'ovaire ; bursicule à 2 loges ; plusieurs feuilles oblongueslancéolées, souvent maculées ; tubercules ovoïdes-obtus.
0. saccata 3600
Fleurs blanches ou un peu verdâtres, nombreuses ; labelle linéaireallongé, entier ; éperon grêle, presque horizontal, 1-2 fois plus long que l'ovaire ; bursicule nulle ; 2-3 feuilles grandes, ovalesoblongues, non maculées ; tubercules en fuseau aigu.
Anthères à loges rapprochées et parallèles ; fossette stigmatique à bord épais ; éperon en alêne ; divisions latérales extérieures du périanthe lancéolées ; fleurs odorantes le soir . 0. bifolia 3(!01
Anthères à loges écartées, divergentes à la base ; fossette stigmatique à ])ord étroit ; éperon en massue grêle ; divisions latérales du périanthe oblongues ; fleurs plus grandes, souvent inodores.
0. montana 3602
= Labelle trilobé, à lobes plus ou moins distincts.
X lî^peron horizontal ou dirigé en haut, cylindracé ou conique ; épi généralement li\che ; tubercules entiers, ovoïdes ou suhg!ol)uloux.
Fleurs d'un jaune paie ou blanchâtre, très rarement purpurines tachées de jaune.
5j^ Feuilles largement obovales ou oblongues, dilatées au-dessus du milieu, non tachées ; fleurs odorantes, en épi serré ; bractées uninervées ; labelle non ponctué ; éperon un peu i^lus court que l'ovaire 0. pallens 3603
Feuilles oblongues-lancéolées, non élargies dans le haut ; fleurs presque inodores, en épi lâche ; bractées inférieures trinervées ; labelle ponctué de pourpre ; éperou à peu près aussi long que l'ovaire.
Feuilles tachées de pourpre noir ; fleurs assez grandes, 6-20 en épi à la fin allongé ; labelle concolore, d'un jaune à peine plus foncé ; éperon ne dépassant pas l'ovaire ; plante assez grêle et élancée 0. provincialis 3604
Feuilles non tachées ; fleurs plus grandes, 3-10 en épi court ; labelle discolore, d'un jaune foncé au milieu ; éperon dépassant l'ovaire ; plante moins élevée mais plus robuste. . 0. pauciflora 3605
►î< Fleurs purpurines ou rosées, accidentellement blanches, jamais tachées de jaune.
Tige nue au sommet, munie à la base de feuilles étalées, oblonguesobtuses ou subobtuses, ordinairement tachées de noir ; bractées à 
1-3 nervures ; labelle fortement ponctué de pourpre ; éperon aussi long que l'ovaire 0. mascula 3606
Tige feuillée jusqu'au sommet, à feuilles dressées, lancéolées-linéaires aiguës, non tachées ; bractées à 3-7 nervures ; laljelle non ou finement ponctué à la base ; éperon d'un tiers environ plus court que l'ovaire.
Labelle presque bilobé, à lobe moyen presque nul, les latéraux larges et rabattus l'un contre l'autre ; éperon horizontal ou dirigé en haut, dilaté-échancré au sommet ; bractées un peu plus courtes que l'ovaire 0. laxiflora 3607
Labelle trilobé, à lobe moyen égalant ou dépassant les latéraux, ceux-ci d'abord étalés, à la fin un peu repliés ; éperon horizontal ou dirigé en bas, atténué-obtus au sommet ; bractées dépassant l'ovaire 0. palustris 3608
X Eperon dirigé en ])as ou pendant ; épi multiflore serré ; tubercules ordinairement palmés ou lobés, à lobes terminés par une fibre.
Fleurs petites, non rayées ni ponctuées de pourpre ; éperon grêle, filiforme, allongé ; 1 seul rétinacle dans une bursicule uniloculaire ou 
2 sans bursicule ; bractées à 3 nervures simples.
Fleurs d'un rose vif, inodores, en épi court, ovoïde-conique, très obtus ; labelle tripartit, muni à la base de 2 petites lamelles verticales ; rétinacles soudés en 1 seul dans une bursicule uniloculaire ; tubercules ovoïdes entiers . . . . 0. pyramidalis 3609
Fleurs d'un rose pâle, odorantes, en épi grêle, allongé, cylindracé, aigu ; labelle trilobé, dépourvu de lamelles à la base interne ; 
2 rétinacles libres sans bursicule ; tubercules palmés, à lobes terminés en fibre allongée. Éperon très grêle, 1-2 fois plus long que l'ovaire ; divisions latérales extérieures du périanthe étalées-dressées et descendantes ; labelle obovale, plus large que long ; fleurs petites (8-10 mm.), en épi assez serré 0. conopea 3610 Éperon légèrement épaissi, égalant à peu près l'ovaire ; divisions latérales du périanthe étalées en ligne droite ; labelle plus étroit, un peu plus long que large ; fleurs très petites (5-7 mm.), en épi plus grêle et plus serré ... 0. odoratissima 3611
Fleurs assez grandes, rayées ou ponctuées de pourpre ; éperon assez épais, cylindrique ou conique, presque droit ; 2 rétinacles libres dans une bursicule à 2 loges ; bractées inférieures à nervures ramifiées en réseau.
•î» Fleurs d'un blanc lilas ou jaunâtres, rarement purpurines ; tige basse et creuse, ou élancée et pleine.
Fleurs jaunâtres ou purpurines tachées de jaune, peu odorantes, en épi ovale court ; bractées inférieures dépassant les fleurs ; éperon gros, conique, égalant ou dépassant l'ovaire ; feuilles d'un vert clair ; tubercules incisés-lobés.	0. sambucina 3612
Fleurs d'un blanc lilas ou rosées, à odeur agréable, en épi cylindracéconique ; bractées ne dépassant pas les fleurs ; éperon cylindrique, plus court (jue l'ovaire ; feuilles tachées de noir en travers ; tubercules profondément lobés . . 0. maculata 3613
Fleurs purpurines ou rose carné, inodores ; tige creuse, ordinairement robuste, souvent très élevée.
Feuilles étalées-dressées, oblongues-lancéolées, plus larges vers le milieu, souvent tachées de l)run ; tubercules à loi)es presque parallèles ; fleurs d'un pourpre foncé un peu violacé ; labelle à loljes latéraux rejetés en arrière 0. latifolia 3014
Feuilles dressées, plus étroites, lancéolées, plus larges à la base et insensiblement atténuées, non tachées ; tubercules à lobes nettement divergents ; fleurs plus pâles, carnées ou roses ; labelle à lobes latéraux ordinairement à peine repliés. 0. incarnata 3615