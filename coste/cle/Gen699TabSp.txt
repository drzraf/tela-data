TABLEAU DES ESPÈCES

4» Feuilles cylindriques ou demi-cylindriques, plus ou moins creuses à l'intérieur.
Périanthe à divisions étalées en étoile ; feuilles exactement cylindriques, sans sillon en dessus ; tige creuse, feuillée dans le tiers inférieur.
X Étamines saillantes ; pédicelles 4-3 fois plus longs que la fleur blanchâtre ; tige renflée en fuseau ; bulbe globuleux ou ovale, solitaire.
Tige renflée-ventrue au-dessous de son milieu ; étamines intérieures munies à la base de chaque côté d'une dent triangulaire ; style court.	**A. Cepa 3431**
Tige renflée-ventrue en son milieu ; étamines toutes dépourvues de dents à la base des filets ; style allongé ; plante moins élevée.	**A. fistulosum 3432**
X Étamines non saillantes ; pédicelles plus courts ou à peine plus longs que la fleur ; tige cylindrique ; bulbes oblongs, en touffe.
Fleurs blanches-bleuâtres, souvent entremêlées de bulbilles ; pédicelles un peu plus longs que la fleur ; étamines égalant environ le périanthe, les 
3 intérieures munies à la base de chaque côté d'une dent courte.	**A. ascalonicum 3433**
Fleurs roses, non entremêlées de bulbilles ; pédicelles plus courts que la fleur ; étamines de moitié plus courtes que le périanthe, toutes dépourvues de dents à la base des filets	**A. Schœnoprasum 3434**
C Périanthe à divisions conniventes en cloche ; feuilles sillonnées ou canaliculées en dessus ; tige presque pleine, feuillée au moins dans la moitié inférieure.
= Étamines saillantes, à filets alternativement simples et à 3 pointes ; spathe à 
1-2 valves ovales-aiguës plus courtes que l'ombelle ; bulbe entouré de nombreux bulbilles.
Fleurs rose pâle ou blanchâtres, peu nombreuses, souvent toutes remplacées par des bulbilles serrés en tête compacte ; pédicelles renflés-globuleux sous la fleur, 4-5 fois plus longs qu'elle ; capsule à sommet ombiliqué.	**A. vineale 3433**
Fleurs rose, vif, très nombreuses, serrées en tête globuleuse ou ovale, très rarement entremêlées de bulbilles ; pédicelles graduellement épaissis sous la fleur, un peu plus longs qu'elle ; capsule à sommet non ombiliqué.	**A. sphsrocephalum 3436**
= Étamines non saillantes, à filets tous simples et en alêne ; spathe à 2 longues pointes linéaires dépassant beaucoup l'ombelle ; bulbe simple ovoïde sous la tunique.
Fleurs rose brun sale, peu nombreuses, entremêlées de bulbilles assez nombreux ; pédicelles inégaux, les plus longs égalant 3 cm. ; capsule obovale, tronquée au sommet ; feuilles un peu glauques ; odeur alliacée.	**A. oleraceum 3437**
Fleurs blanchâtres ou rosées, nombreuses, en ombelle diffuse très rarement bulbillifère ; pédicelles très "inégaux, les plus longs atteignants cm. ; capsule ellipsoïde, rétrécie aux 2 bouts ; feuilles vertes ; odeur non alliacée.	**A. paniculatum 3438**
4» Feuilles planes, en gouttière ou cylindracées, jamais creuses, 
-j- Tige paraissant feuillée au moins dans son tiers inférieur.
»§* Étamines à filets tous simples, rarement les intérieurs munis à la base de chaque côté d'une dent très courte ; bulbe simple.
]§ Spathe à 2 valves terminées par une pointe linéaire plus longue que l'ombelle ; étamines saillantes.
Fleurs roses ou violacées, en ombelle plus ou moins lâche et peu fournie, souvent munie de bulbilles ; anthères purpurines ; feuilles peu épaisses, rudes aux bords et sur les nervures en dessous.	**A. carinatum 3439**
Fleurs d'un jaune doré, en ombelle un peu serrée et multitlore, toujours dépourvue de bulbilles ; anthères jaunes ; feuilles charnues et comme pulpeuses, abords obtus, très lisses sur toute leur surface.	**A. flavum 3440**
]§[ Spathe à 1-2 valves ovales ou lancéolées, toujours plus courtes que l'ombelle.
Etamines saillantes ; fleurs nombreuses, en ombelle globuleuse serrée ; bulbe allongé, à tunique très filamenteuse ; plantes de 2b-60 cm.
Fleurs roses, en ombelles petites (1 1/2 à 2 cm. de diam.) ; pédicelles de la longueur de la fleur ; filets des étamines intérieures munis d'une dent à la base ; feuilles linéaires, égalant presque la lige.	**A. strictum 3441**
Fleurs blanches ou jaunâtres, en ombelles larges de 2-3 cm. ; pédicelles plus longs que la fleur ; filets tous dépourvus de dent à la base ; feuilles sensiblement plus courtes que la tige.
Feuilles elliptiques-lancéolées, larges de 3-S cm., courtement pétiolées, à nervures convergentes ; bulbe à fibres entrecroisées en réseau ; tige épaisse, un peu anguleuse au sommet ; spathe à valve unique.	**A. Victorialis 3442**
Feuilles linéaires-étroites (1-5 mm. de large), sessiles, à nervures parallèles ; bulbe à fibres de la tunique déchirées en long ; tige grêle, cylindrique ; spathe à 2 valves égales.
A. ericetorum 3443 Étamines incluses ; fleurs peu nombreuses, en ombelle lâche ; bulbe ovoïde ; plantes grêles de 5-30 cm.
Ombelle presque plane, à pédicelles égaux ou presque égaux ; tige feuillée au sommet ou dans le tiers inférieur ; bulbe revêtu d'une tunique coriace ou fibreuse.
Tige souterraine, naine, feuillée au sommet ; feuilles très étalées, linéaires-lancéolées, larges de 3-7 mm., planes, velues-ciliées, 
3-4 fois plus longues que l'ombelle ; fleurs d'un blanc pur, anthères jaunâtres	**A. Chamsemoly 3444**
Tige aérienne, effilée, feuillée dans le tiers inférieur ; feuilles dressées, linéaires-sétacées (1/2 mm. de large), sillonnées, glabresceutcs, ne dépassant pas l'ombelle ; fleurs rosées, anthères brunes.	**A. moschatum 3445**
Ombelle plus ou moins arrondie, à pédicelles nettement inégaux ; tige feuillée dans sa moitié inférieure ; bulbe revêtu de tuniques membraneuses.
Ombelle assez grande, longue de 2-4 cm., très lâche, pauciflore ; périanthe en cloche cylindracée, long de 5-G mm., à divisions lancéolées-acuminées ; lige feuillée jusqu'au-dessus du milieu.	**A. parciflorum 3440**
Ombelle petite, longue 1-2 cm., un peu serrée et parfois multitlore ; périanthe en cloche courte, long de 3-4 mm., à divisions oblonguesobtuses ou sublronquées ; lige feuillée à peine jusqu'au milieu.
A. maritimum 3447 tî< Étamines intérieures à filets terminés par 3 pointes, dont la médiane porte l'anthère ; bulbe presque toujours entouré de nombreux bulbilles.
Fleurs peu nombreuses, en ombelle munie de bulbilles ou même toute bulbillifère ; étamines incluses.
Spathe univalve, terminée en pointe très longue dépassant l'ombelle ; fleurs blanc sale' ou rougeâtre ; filets intérieurs à 3 pointes presque égales ; feuilles lisses ; tige de 20-40 cm., en cercle avant la floraison.	**A. sativum 3448**
Spathe à 2 valves ovales, brusquement terminées en pointe plus courte que l'ombelle ; fleurs purpurines ; filets intérieurs à 3 pointes, les 2 latérales 2 fois plus longues ; feuilles rudes aux bords ; tige de 30-80 cm., droite	**A. Scorodoprasum 3449**
Fleurs très nombreuses, en ombelle grande, presque jamais bulbillifère.
\^ Etamines incluses, à anthères purpurines plus courtes que le périanthe ; périantbe ovale-pyramidal ; ombelle serrée, large de 2-3 cm.
Il Fleurs d'un pourpre foncé mêlé de blanc, en ombelle très serrée ; pédicelles inégaux, les inférieurs beaucoup plus courts ; périanthe à divisions oblongues, obtuses ou mucronulées ; feuilles lisses aux bords.	**A. rotundum 3450**
Il Fleurs rose clair avec carènes plus foncées, en ombelle moins serrée ; pédicelles tous presque égaux ; périanthe à divisions lancéoléesacuminées ; feuilles rudes aux bords et sur la carène.	**A. acutiflorum 3431**
Etamines saillantes, égalant ou dépassant le périanthe ; périanthe en cloche connivente ; ombelle ample, large de 4-8 cm. ; plantes robustes.
■y|c Fleurs blanchâtres ou carnées, à carènes d'un blanc verdâtre ; anthères rougeâtres ou blanc jaunâtre.
Spathe membraneuse, terminée en pointe à peine 1 fois plus longue qu'elle ; fleurs toujours blanc pâle, en ombelle d'environ 4 cm. de diam. ; anthères blanc jaunâtre ; feuilles très scabres aux bords ; bulbe petit, entouré de bulbilles jaunâtres.	**A. scaberrimum 3432**
Spathe herbacée, terminée en pointe 4-5 fois plus longue qu'elle ; fleurs blanches ou carnées, en ombelle très ample de 4-6 cm. ; anthères rougeâtres ; feuilles lisses ; bulbe allongé, simple.	**A. Porrum 3433**
Fleurs rosées ou purpurines, à carènes généralement roses ; anthères jaunes ; spathe scarieuse, à pointe courte.
Fleurs rosées, en ombelle ample assez lâche ; pointe anthérifère des etamines intérieures 3 fois plus courte que son filet ; tige de 40-80 cm., feuillée jusqu'au tiers, à feuilles lisses, et à odeur piquante de poireau	**A. polyanthum 3454**
Fleurs rose-lilas, en grosse ombelle compacte ; pointe anthérifère des etamines intérieures égalant environ son filet ; tige atteignant souvent 1 mètre, feuillée jusqu'au milieu, à feuilles rudes aux bords et à forte odeur d'ail commun.	**A. Ampeloprasum 3433**
-\~ Tige nue ou paraissant feuillée seulement dans son quart inférieur ; etamines à filets tous simples.
Fleurs d'un blanc pur ou jaunes, à etamines incluses ; feuilles peu nombreuses (1-3).
Tige cylindrique ou demi-cylindrique ; périanthe étalé en étoile.
Fleurs d'un jaune doré ; périanthe à la fin presque scarieux, recouvrant le fruit ; feuille unique, un peu raide, large de 1-2 cm. ; bulbe ovoïdeoblong, assez gros (2 cm. de large), à tunique coriace brunâtre.
A. Moly 345G
Fleurs d'un blanc pur ; périanthe mince, à la fin caduc ou laissant voir le fruit ; feuilles 2-3, molles et flasques ; bulbe petit, jamais large de 
2 cm.
Feuilles ovales-lancéolées courtement acuminées, larges de 2-5 cm., longuement pétiolées, glabres, à nervures convergentes ; tige demicylindrique, à 2 angles saillants ; bulbe oblong, à tunique blanche membraneuse	**A. ursinum 3437**
Feuilles linéaires-allongées, longuement acuminées, larges de 4-12 mm., sessiles velues-ciliées, à nervures parallèles ; tige cylindrique, sans angles saillants ; bulbe ovoïde-arrondi, à tunique brune coriace.	**A. subhirsutum 3438**
Tige trigone ou triquètre ; périanthe blanc, en cloche ou en coupe à la fin connivente.
V Tige trigone, à 2 angles aigus et 1 obtus ; feuilles larges de 1-3 cm., un peu rudes aux bords ; spathe univalve, ovale-acuminée ; périanthe à divisions largement ovales très obtuses ; stigmate obtus.
A. Neapolitanum 34S9
V Tige triquètre, à 3 angles aigus ; feuilles larges de 4-10 mm., lisses aux bords ; spathe à 2 valves lancéolées-acuminées ; périanthe à divisions oblongues ou lancéolées ; stigmate trifide.
Ombelle à 4-12 fleurs, à pédicelles penchés du même côté, épaissis au sommet ; périanthe cylindracé, à divisions aiguës ; feuilles égalant à peu près la tige ; tige un peu épaisse, longue de 20-SO cm.	**A. triquetrum 3460**
Ombelle à 2-5 fleurs, à pédicelles d'abord dressés puis arqués et réfléchis en tous sens, peu épaissis au sommet ; périanthe fructifère globuleuxellipsoïde, à divisions subobtuses ; feuilles dépassant la tige ; celleci grêle, longue de 10-30 cm	**A. pendulinum 3461**
Fleurs purpurines ou violacées, rarement blanchâtres ; feuilles assez nombreuses (3-8). w» Bulbes oblongs, naissant d'une souche horizontale rampante ; feuilles étroitement linéaires (1-5 mm. de large).
Fleurs grandes, d'un rose vif, en ombelle pauciflore penchée avant la floraison ; périanthe long de 12-15 mm., à divisions ovales fortement mucronées ; étamines égalant à peine la moitié du périanthe ; stigmate trifide, inclus ; tunique très fibreuse. . . .	**A. narcissiflorum 3462**
•î» Fleurs petites, purpurines, en ombelle multiflore dressée ; périanthe long de 5-7 mm., à divisions oblongues-lancéolées ; étamines égalant ou dépassant le périanthe ; stigmate obtus, tressaillant ; tunique membraneuse entière. Étamines égalant le périanthe ; feuilles presque aussi longues que la tige, planes, nervées et carénées en dessous ; tige longue de 
30-60 cm., à 2 tranchants dans le haut ; plante des marais.
A. angulosum 3463 Étamines dépassant le périanthe ; feuilles nettement plus courtes que la tige, obscurément nervées et sans carène en dessous ; tige de 15-40 cm., anguleuse dans le haut ; i^lanle des rochers.	**A. fallax 3464**
**<• Bulbes ovales, jamais implantés dans une souche horizontale ; feuilles larges de plus de 5 mm. ; étamines incluses.
4- Fleurs rose vif ou violettes, en ombelle parfois bulbillifère ; loges de la capsule à 2-3 graines ; style naissant au fond de l'ovaire.
Fleurs d'un rose vif, en ombelle un peu lâche ; périanthe en cloche, à la fin scarieux et recouvrant le fruit ; tige feuillée à la base ; feuilles larges de 5-12 mm. ; bulbe entouré de nombreux bulbilles et d'une tunique élégamment alvéolée	**A. roseum 3465**
Fleurs violacées à carènes vertes, en ombelle serrée et très fournie ; périanthe étalé en étoile, à la fin réfléchi sous le fruit nu ; tige nue jusqu' à la base ; feuilles larges de 3-6 cm. ; bulbe gros, généralement simple, à tunique sans alvéoles	**A. nigrum 3466**
-j- Fleurs pourpre sale ou blanc verdâtre, en ombelle jamais bulbillifère ; loges de la capsule à plusieurs graines ; style naissant au sommet de l'ovaire.
Fleurs pourpre vert sale, penchées à la floraison ; périanthe long de 
15 mm., à la fin coriace et soudé inférieurement avec la capsule ; pédicelles renflés et dilatés au sommet en large disque ; étamines égalant la moitié du périanthe ; feuilles larges de 1-2 cm.	**A. siculum 3467**
Fleurs blanches à fond verdâtre, dressées ; périanthe long d'environ 1 cm., ni coriace, ni soudé avec l'ovaire ; pédicelles grêles, non dilatés au sommet ; étamines égalant presque le périanthe ; feuilles larges de 
V 4-8 mm	**A. fragrans 3468**