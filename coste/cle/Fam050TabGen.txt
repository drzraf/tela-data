TABLEAU DES GENRES

Fleurs jaunes, sessiles ; sépales caducs ; 6-12 étamines ; capsule s'ouvrant circulairement, à graines nombreuses.	**PORTULACA 234**
Fleurs blanches, pédonculées ; sépales persistants ; 3-5 étamines; capsule s'ouvrant par 3 valves, à 3 graines.
	Corolle monopétale, en entonnoir, fendue d'un coté, à lobes inégaux ; tige feuillée, à feuilles oblongues-spatulées, opposées, atténuées en court pétiole.	**MONTIA 235**
	Corolle à 5 pétales ordinairement libres, égaux, entiers ou émarginés ; tige nue, portant au sommet 2 feuilles soudées en involucre,les radicales ovales-rhomboïdales, longuement pétiolées.	**CLAYTONIA 236**