TABLEAU DES ESPÈCES

{1} Fleurs jaunes.
	{2} Feuilles munies à la base de 2 petites glandes, les florales subopposées ; rameaux anguleux ; pétales à onglets soudés ; stigmates en massue.
		Plante annuelle, à racine grêle ; feuilles scabres aux bords ; sépales linéaires, 1 fois plus courts que ]a corolle, 3 fois plus longs que la capsule.	**L. nodiflorum 595**
		Plante vivace, à souche ligneuse ; feuilles bordées d'une membrane transparente et lisse ; sépales lancéolés, 3‑4 fois plus courts que la corolle, dépassant peu la capsule.	**L. campanulatum 596**
	{2} Feuilles sans glandes à la base, les supérieures alternes ; rameaux arrondis ; pétales à onglets libres.
		Plante vivace, à souche épaisse ; feuilles inférieures opposées, spatulées, à 3 nervures ; sépales 3‑4 fois plus courts que la corolle, égalant la capsule ; stigmates en massue.	**L. maritimum 597**
		Plantes annuelles, à racine grêle ; feuilles toutes alternes, linéaires‑lancéolées, à 1 nervure ; sépales à peine 1 fois plus courts que la corolle, dépassant la capsule ; stigmates en tête.
			Feuilles très rudes aux bords ; fleurs rapprochées, en corymbe compacte ; pédicelles raides, plus courts que le calice ; sépales lancéolés, 1‑2 fois plus longs que la capsule.	**L. strictum 598**
			Feuilles un peu rudes aux bords ; fleurs espacées, en corymbe très lâche ; pédicelles filiformes, aussi longs que le calice ; sépales ovales‑lancéolés, dépassant peu la capsule.	**L. gallicum 599**
{1} Fleurs bleues, roses ou blanches.
	{2} Feuilles toutes opposées, obovales ou oblongues ; fleurs blanches, petites, à pétales 1 fois seulement plus longs que le calice ; stigmates en tête.	**L. catharticum 600**
	{2} Fleurs foutes alternes ou éparses, lancéolées ou linéaires ; fleurs grandes, à pétales 2 fois au moins plus longs que le calice.
		{3} Fleurs rosées, carnées ou blanchâtres ; sépales tous ciliés‑glanduleux.
			Feuilles velues, oblongues‑lancéolées, à 3‑5 nervures ; tiges couvertes de poils étalés ; pétales veinés de violet ; stigmates en massue.	**L. viscosum 601**
			Feuilles glabres, étroitement linéaires en alène, à 1 nervure ; tiges glabres ou pubérulentes ; pétales veinés de pourpre ; stigmates en tête.
				Tiges couchées‑étalées, pubérulentes dans le haut ; sépales ovales‑acuminés, à 3 nervures ; pétales exactement arrondis au sommet, 3‑4 fois plus longs que le calice.	**L. salsoloides 602**
				Tiges ascendantes ou dressées, glabres dans le haut ; sépales lancéolés en alène, à 1 nervure ; pétales brièvement acuminés au sommet, 2‑3 fois plus longs que le calice.	**L. tenuifoiium 603**
		{3} Fleurs bleues ; sépales tous, ou les extérieurs, non ciliés‑glanduleux.
			Fleurs très grandes, de 3‑4 cm. de long ; sépales dépassant la capsule ; pétales à onglet très long ; stigmates allongés, filiformes.	**L. narbonense 604**
			Fleurs moins grandes ; sépales ne dépassant pas la capsule ; pétales à onglet court ; stigmates courts, non filiformes.
				Sépales bien plus courts que la capsule, les trois intérieurs ovales‑obtus, non ciliés ; stigmates en tète ; graines ternes.	**L. alpinum 605**
				Sépales égalant presque la capsule, tous ovales‑acuminés ; stigmates en massue ; graines luisantes.
					Fleurs d'un bleu clair, assez grandes, à pétales 2 fois plus longs que le calice ; sépales intérieurs ciliés ; capsule à cloisons barbues ; graines non rostrées ; tiges ascendantes.	**L. angustifolium 606**
					Fleurs d'un bleu vif, grandes, à pétales 3 fois plus longs que le calice ; sépales tous glabres ; capsule à cloisons non barbues ; graines rostrées ; tige solitaire, dressée.	**L. usitatissimum 607**