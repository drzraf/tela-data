TABLEAU DES ESPÈCES

Panicule en tète hémisphérique, plus large que longue, enveloppée par les gaines des 
2 feuilles supérieures ; glumelle supérieure uninervée ; 2 étamines ; caryopse oblong	**C. aculeata 3928**
Panicule en épi ovale ou cylindrique, saillant ou enveloppé par une seule gaine ; glumelle supérieure binervée ; 3 étamines ; caryopse ovale-arrondi {Heleochloa Host) .
Panicule spiciforme ovale, enveloppée à la base par la gaine supérieure courte et très renflée ; glumes aiguës ; tiges couchées-étalées, ordinairement rameuses.	**C. schœnoides 3929**
Panicule spiciforme cylindrique, saillante ou d'abord enveloppée par la gaine non ou peu renflée et allongée ; glumes subobtuses ; tiges étalées-ascendantes, ordinairement simples	**C. alopecuroides 3930**