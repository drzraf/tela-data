TABLEAU DES ESPÈCES

Calice hérissé ; plantes velues ou cotonneuses.
	Plantes blanchâtres ; fleurs hermaphrodites ; capsule 5 dents.
		Plante annuelle, velue‑soyeuse ; feuilles linéaires‑lancéolées ; calice plus long que la corolle.	**L. Githago 460**
		Plantes vivaces, cotonneuses ; feuilles oblongues‑lancéolées ; calice plus court que la corolle.
			Fleurs longuement pédonculées, en grappe très lâche ; calice à dents, linéaires, tordues ; pétales entiers.	**L. Coronaria 461**
			Fleurs courtement pédonculées, en corymbe serré ; calice à dents ovales‑aiguës, non tordues ; pétales bifides.	**L. Flos‑Jovis 462**
	Plantes verdâtres ; fleurs dioïques ; capsule à 10 dents.
		Plante velue, non glanduleuse ; fleurs rouges ; capsule petite, subglobuleuse, à dents fortement enroulées.	**L. diurna 463**
		Plantes velues‑glanduleuses ; fleurs blanches ou un peu rosées ; capsule grosse, ovale‑conique.
			Calice fructifère à dents lancéolées‑aiguës ; capsule à dents courbées en dehors ; graines concaves.	**L. macrocarpa 464**
			Calice fructifère à dents triangulaires‑obtuses ; capsule à dents dressées ; graines planes sur le dos.	**L. vespertina 465**
Calice glabre ; plantes vertes, glabres ou pubérulentes.
	Feuilles de la tige largement ovales ; pétales entiers, blancs ; graines lisses ; tiges florifères latérales.	**L. pyrenaica 466**
	Feuilles de la tige linéaires‑lancéolées ; pétales divisés, rouges ; graines tuberculeuses ; tiges florifères terminales.
		Pétales découpés en lanières linaires ; carpophore nul ; tiges hispidules surtout à la base.	**L. Flos‑cuculi 467**
		Pétales bifides ; carpophore allongé ; tiges glabres.
			Fleurs courtement pédicellées, en grappes serrées et fournies ; calice rouge ; capsule à 5 loges à la base.
				Grappes oblongues, interrompues ; calice ombiliqué, en massue ; onglet auriculé.	**L. Viscaria 468**
				Grappes en corymbe dense ; calice non ombiliqué, en cloche ; onglet non auriculé.	**L. alpina 469**
			Fleurs longuement pédicellées, solitaires ou en grappes pauciflores ; calice vert ; capsule à 1 loge.
				Fleurs grandes ; calice fructifère allongé, en massue ; capsule égalant le carpophore.	**L. Cœli‑Rosa 470**
				Fleurs petites ; calice fructifère court et renflé ; capsule 3‑5 fois plus longue que le carpophore.	**L. læta 471**