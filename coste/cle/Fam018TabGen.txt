TABLEAU DES GENRES

Calicule nul ou à 10‑12 divisions ; fruit capsulaire, déhiscent, 'a plusieurs loges polyspermes.
	Fleurs jaunes ; calicule nul ; corolle dépassant peu le calice ; 12‑15 capsules, plus longues que le calice.	**ABUTILON 119**

	Fleurs rouges ; calicule à 10‑12 lobes linéaires ; corolle 3‑5 fois plus longue que le calice ; 1 capsule, subglobuleuse, plus courte que le calice.	**HIBISCUS 118**
Calicule à 3‑9 divisions ; fruit composé de carpelles monospermes, disposés en verticille ou en tète, se détachant de l'axe à la maturité.
	Calicule à 3 folioles distinctes, non soudées entre elles ; stigmates obtus ou en tète.
		Calicule naissant du pédoncule, à 3 folioles largement en coeur à la base ; carpelles disposés en tête sur un réceptacle globuleux.	**MALOPE 114**
		Calicule naissant de la base du calice, à 3 folioles non cordiformes ; carpelles disposés en verticille autour d'un axe central.	**MALVA 115**
	Calicule à 3‑9 divisions soudées inférieurement, naissant du pédoncule ; stigmates sétacés.
		Calicule à 3 divisions.	**LAVATERA 116**
		Calicule à 6‑9 divisions.	**ALTHAEA 117**