TABLEAU DES ESPÈCES

Tige grêle, nue, pauciflore ; pétales dépassant la moitié des étamines.
	Tige portant au sommet 1-3 bractées ovales ; feuilles toutes radicales, coriaces, à 7 folioles.	**H. niger 86**
	Tige portant sous les rameaux des feuilles digitées ; feuilles radicales nulles à la floraison, à 7-11 folioles.	**H. viridis 87**
Tige robuste, feuillée, multiflore ; pétales égalant la moitié des étamines.
	Feuilles triséquées, à 3 folioles bordées de dents spinescentes ; sépales étalés, presque plans.	**H. lividus 88**
	Feuilles palmatiséquées, à 7-11 folioles simplement dentées ; sépales dressés connivents, concaves.	**H. fœtidus 89**