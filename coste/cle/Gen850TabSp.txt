TABLEAU DES ESPÈCES

Fleurs elliptiques-oblongues, lâches et renflées à la maturité ; glumelle inférieure coriace, ovale-oblongue, à la fin débordée en tous sens par la supérieure ; caryopse épais, ovale-oblong ; plantes annuelles.
Glume égalant ou dépassant l'épillet ; épillets grands, oblongs en coin, égalant ou dépassant les entrenœuds ; plante assez robuste, à feuilles larges de 4-10 mm., habitant les moissons	**L. temulentum 4250**
Glume un peu plus courte que l'épillet ; épillets assez petits, obovales en coin, écartés, plus courts que les entrenœuds ou les égalant ; plante assez grêle, à feuilles larges de 2-G mm., habitant les champs de lin.	**L. remotum 4251**
Fleurs lancéolées, serrées et non renflées à la maturité ; glumelles membraneuses, également larges, l'inférieure lancéolée, jamais dépassée sur les côtés par la supérieure ; caryopse peu épais, linéaire-oblong.
Epillets mutiques, souvent pauciflores, plus ou moins appliqués contre l'axe, d'un tiers ou d'un quart plus longs que la glume ; plantes de 20-60 cm., généralement assez grêles.
Plante annuelle, à tiges non accompagnées à la base de fascicules stériles de feuilles ; feuilles enroulées par les bords dans le jeune Age, les supérieures à gaine un peu renflée ; plante des cultures	**L. rigidum 4252**
Plante vivace, à souche tenace, émettant des fascicules stériles de feuilles d'abord pliées en long dans toute leur longueur ; feuilles caulinaires à gaines non sensiblement renflées ; plante des prés et pelouses . . . ,	**L. perenne 4253**
Epillets aristés, multiflores, plus ou moins étalés et écartés de l'axe, 2-3 fois plus longs que la glume ; plantes atteignant souvent 1 mètre, généralement assez robustes.
Plante bisannuelle ou plurannuelle, à tiges accompagnées à leur base de fascicules stériles des feuilles étroitement enroulées par les bords dans leur jeunesse ; epillets à S-12 fleurs, environ deux fois plus longs que leur glume.	**L. italicum 4234**
Plante annuelle, à tiges non accompagnées à la base de fascicules stériles de feuilles ; epillets plus grands, contenant 10-2S fleurs et environ trois fois plus longs que leur glume	**L. multiflorum 4233**