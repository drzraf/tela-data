TABLEAU DES ESPÈCES

Fleurs fasciculées par 2-5 ; stipules glabres ; feuilles largement ovales en cœur ; arbrisseau non épineux, à drupes jaunâtres	**P. brigantiaca 1086**
Fleurs solitaires ou géminées ; stipules pubescentes ; feuilles jamais en cœur à la base.
	Arbres non ou à peine épineux ; pédoncules pubescents, le plus souvent géminés dans chaque bourgeon ; drupe grosse, penchée, à saveur douce ; noyau rugueux sur les faces.
		Jeunes rameaux glabres ; feuilles glabres ou légèrement pubescentes en dessous ; calice poilu à l'intérieur ; drupe ordinairement oblongue.	**P. domestica 1087**
		Jeunes rameaux pubescents ou tomenteux ; feuilles velues en dessous surtout sur les nervures ; calice glabre à l'intérieur ; drupe globuleuse ou ovale.	**P. insititia 1088**
	Arbrisseaux épineux ; pédoncules glabres ou pubérulents, ordinairement solitaires dans chaque bourgeon ; drupe petite, globuleuse, dressée, acerbe ; noyau presque lisse.
		Arbrisseau élancé, à rameaux peu épineux ; feuilles adultes larges de 15-25 mm., velues en dessous surtout sur les nervures ; fruit assez petit, à noyau ovoïde déprimé.	**P. fruticans 1089**
		Arbrisseau buissonneux, à rameaux divariqués et très épineux ; feuilles étroites, d'abord pubescentes, puis glabres ; fruit petit, à noyau subglobuleux, à peine déprimé.	**P. spinosa 1090**