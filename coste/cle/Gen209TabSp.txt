TABLEAU DES ESPÈCES

{1} Fleurs en fascicules latéraux, opposés aux feuilles ; 1-2 étamines ; plantes annuelles de 3-20 cm. (//Aphanes//).
	Tiges ascendantes ou étalées en cercle ; feuilles brièvement pétiolées, tripartites, à divisions incisées-lobées ; stipules formant une sorte de cornet foliacé.	**A. arvensis 1240**
	Tiges dressées, très feuillées ; feuilles sessiles, imbriquées, tripartites, à divisions bi-trilobées ; stipules grandes, largement connées avec les feuilles et formant une gaine embrassante.	**A. cornucopioides 1241**
{1} Fleurs en cymes corymbiformes terminales et latérales ; ordinairement 4 étamines ; plantes vivaces.
	{2} Feuilles divisées presque jusqu'à la base en 5 segments profondément incisés-dentés, verts et glabres sur les 2 faces ; fleurs disposées en 1 ou 2 verticilles terminaux ; lobes du calicule réduits à 1 dent bien plus courte que les lobes du calice.	**A. pentaphyllea 1242**
	{2} Feuilles à segments ou lobes non profondément incisés ; fleurs en glomérules axillaires et terminaux plus ou moins nombreux ; lobe du calicule égalant environ ceux du calice.
		{3} Feuilles palmatiséquées, divisées jusqu'à la base ou presque jusqu'à la base en 5-9 folioles oblongues, vertes et glabres en dessus, argentées-soyeuses en dessous.
			{4} Folioles 5-7, obtuses-tronquées au sommet, à dents profondes et non conniventes, à pubescence de la face inférieure peu abondante et peu brillante ; tiges dépassant une fois seulement les feuilles radicales.	**A. subsericea 1243**
			{4} Folioles obtuses ou subaiguës, à dents petites et conniventes, à pubescence de la face inférieure abondante et très brillante ; tiges dépassant au moins 2 fois les feuilles radicales.
				{5} Folioles 5-7, distinctes jusqu'à la base, luisantes en dessus sur le vif ; fleurs petites, en glomérules denses.
					Folioles presque toujours 5 sur chaque individu, obovales ou obovales-oblongues ; souche à stolons allongés; tiges 3-5 fois plus longues que les feuilles ; feuilles en glomérules distants, formant des épis interrompus.	**A. saxatilis 1244**
					Folioles 5-7, jamais exclusivement 5, lancéolées ou oblongues-lancéolées ; souches à stolons courts ; tiges environ 2 fois plus longues que les feuilles radicales ; fleurs à glomérules supérieurs confluents.	**A. alpina 1245**
				{5} Folioles 7-9, toutes ou quelques-unes soudées à la base, d'un vert mat ou glaucescentes en dessus sur le vif ; fleurs assez grandes, en glomérules plus ou moins lâches.
					Folioles les unes libres, les autres plus ou moins soudées à la base, oblongues ou oblongues-lancéolées, soyeuses et brillantes en dessous.	**A. Hoppeana 1246**
					Folioles toutes régulièrement soudées à la base sur un cinquième à un tiers de leur longueur, obovales, abondamment soyeuses-argentées en dessous.	**A. conjuncta 1247**
		{3} Feuilles suborbiculaires-palmatilobées, à 7-11 lobes superficiels ou atteignant au plus le milieu du limbe, glabres, velues ou un peu soyeuses en dessous.
			{4} Feuilles à lobes assez profonds, glabres ou presque glabres en dessus, pubescentes-soyeuses en dessous, surtout sur les nervures, à poils appliqués, longs, fins, brillants.	**A. splendens 1248**
			{4} Feuilles à lobes peu profonds, glabres, pubescentes ou velues, non soyeuses-argentées en dessous.
				Inflorescence, fleurs et pédicelles nettement velus ; fleurs en glomérules souvent confluents ; plante des lieux secs, peu élevée, toute couverte de poils longs et fins.	**A. pubescens 1249**
				Inflorescence, fleurs et pédicelles glabres ou parsemés de quelques poils ; fleurs en cymes corymbiformes diffuses ; plantes plus élevées, glabres ou modérément poilues.
					Feuilles glabres, papyracées, à lobes bien tranchés, dentés au sommet, à dents grosses, souvent digitiformes et divergentes ; stipules supérieures incisées-étoilées ; fleurs assez grandes, étalées en étoile après la floraison.	**A. glaberrima 1250**
					Feuilles glabres ou poilues, à lobes peu profonds, ordinairement dentés en scie sur tout leur pourtour ; stipules dentées ou un peu incisées-étoilées ; fleurs plus petites, moins nettement étoilées après la floraison.	**A. vulgaris 1251**