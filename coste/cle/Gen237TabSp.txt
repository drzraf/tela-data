TABLEAU DES ESPÈCES

Feuilles ovales-oblongues ou spatulées ; stipules et bractées argentées ; pétales émarginés ; plante annuelle.	**P. tetraphyllum 1330**
Feuilles ovales ou orbiculaires ; stipules et bractées grises, non argentées ; pétales entiers ; plante vivace.	**P. peploides 1331**