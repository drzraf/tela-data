TABLEAU DES ESPÈCES

Feuilles oblongues‑subspatulées ; silicules orbiculaires, aplaties ; graine occupant à peine le quart de la loge.	**C. Jonthiaspi 272**
Feuilles obovales‑spatulées ; silicules petites, obovales, un peu convexes ; graine remplissant au moins la moitié de la loge.	**C. microcarpa 273**