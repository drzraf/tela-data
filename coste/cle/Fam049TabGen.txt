TABLEAU DES GENRES

Plante dioïque, grimpante, munie de vrilles ; fruit globuleux dressé, lisse, rouge, ne s'ouvrant jamais pour lancer les graines.	**BRYONIA 232**
Plante monoïque, rampante, sans vrilles ; fruit oblong, penché, rude, hérissé, verdâtre, s'ouvrant avec élasticité et lançant ses graines à la maturité.	**ECBALLIUM 233**