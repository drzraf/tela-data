TABLEAU DES GENRES

{1} Calice à 5 lobes, égaux ou peu inégaux, toujours bien distincts ; anthères mutiques.
	{2} Corolle à tube prolongé à la base en bosse ou en éperon ; capsule s'ouvrant par plusieurs valves ou par des trous.
		Corolle grande, en masque, à tube large renflé-bossu à la base, à gorge complètement fermée par un palais saillant ; capsule s'ouvrant au sommet par 2-3 trous.	**ANTIRRHINUM 547**
		Corolle souvent petite, à tube atténué à la base en éperon linéaire ou conique ; capsule s'ouvrant par plusieurs valves, ou par 2 trous ou par un couvercle.
			Corolle irrégulière, à éperon très petit, à gorge ouverte et dépourvue de palais ; anthères à 1 loge ; feuilles caulinaires la plupart divisées dès la base en 3-7 lanières linéaires.	**ANARRHINUM 548**
			Corolle en masque, à éperon bien visible, à gorge fermée ou à peine ouverte munie d'un palais saillant ; anthères à deux loges ; feuilles caulinaires toutes simples.	**LINARIA 549**
	{2} Corolle à tube jamais bossu ni éperonné à la base ; capsule s'ouvrant par 2 valves entières ou bifides.
		{3} Feuilles caulinaires opposées ; corolle souvent à 2 lèvres.
			{4} Fleurs d'un brun rougeâtre ou verdâtre ; corolle à tube subglobuleux, à limbe petit et à 2 lèvres, la supérieure plus grande et munie en dedans d'une petite écaille ou staminode ; anthères à 1 loge ; stigmate en tète.	**SCROFULARIA 546**
			{4} Fleurs rosées ou jaunâtres ; corolle à tube cylindrique, à limbe à 5 lobes ou à 2 lèvres, la supérieure plus petite et sans écaille en dedans ; anthères à 2 loges ; stigmate à 2 lames.
				Corolle petite (2 à 8 mm.), rosée ou violacée, souvent incluse dans le calice profondément divisé en lobes linéaires ; plantes annuelles, glabres, de 5-25 cm.	**LINDERNIA 550**
				Corolle grande, dépassant 1 cm., toujours longuement saillante ; plantes vivaces de 10-50 cm.
					Corolle d'un blanc rosé, à 2 lèvres peu distinctes ; calice muni à la base de 2 bractées sépaloïdes, à 5 lobes profonds ; 2 étamines fertiles et 2 filets sans anthères ; plante très glabre.	**GRATIOLA 551**
					Corolle jaunâtre, à 2 lèvres inégales, la supérieure plus courte ; calice non entouré de bractées, tubuleux-pentagonal, à 5 lobes bien plus courts que le tube ; 4 étamines toutes fertiles ; plantes velues ou pubérulentes.	**MIMULUS 552**
		{3} Feuilles alternes ou toutes radicales ; corolle non distinctement bilabiée.
			{4} Fleurs purpurines ou jaunâtres, en grappes terminales bractéolées ; feuilles atténuées en court pétiole ou demi-embrassantes ; corolle tubuleuse en cloche ou en coupe ; stigmate bilobé ; plantes vivaces.
				Fleurs penchées, en grappes unilatérales ; corolle grande, à long tube ventru, à limbe oblique et à 4-5 lobes courts et très inégaux ; anthères à 2 loges ; plantes robustes d'environ 1 mètre.	**DIGITALIS 553**
				Fleurs dressées, en grappes corymbiformes ; corolle petite (5-8 mm.), à tube grêle dépassant à peine le calice, à limbe presque plan et à 5 lobes échancrés presque égaux ; anthères à 1 loge ; plante gazonnante de 5-13 cm.	**ERINUS 554**
			{4} Fleurs blanches ou rosées, très petites, solitaires sur des pédoncules bien plus courts que les pétioles très allongés des feuilles ; corolle presque en roue, plane, à tube court ; stigmate en tète ; plantes annuelles très grêles.
				Plante glabre, stolonifère, sans tiges ; feuilles toutes radicales, oblongues ou spatulées, entières ; calice glabre, en cloche, à 5 dents ; anthères à 1 loge ; capsule molle, glabre.	**LIMOSELLA 555**
				Plante velue, à tiges couchées-radicantes ; feuilles alternes, orbiculaires en cœur, crénelées ; calice velu, à 5 lobes profonds ; anthères à 2 loges ; capsule membraneuse, velue.	**SIBTHORPIA 556**
{1} Calice à 4 lobes presque égaux, rarement à 5 dont l'un très petit, ou à 2 lèvres.
	{2} Corolle en roue, à tube presque nul, à limbe plan à 4 lobes un peu inégaux ; 2 étamines ; anthères mutiques ; fleurs ordinairement bleues.	**VERONICA 557**
	{2} Corolle tubuleuse-bilabiée, à lèvre supérieure plus ou moins concave ou en casque, l'inférieure trilobée ; 4 étamines didynames ; fleurs rarement bleuâtres.
		{3} Calice renflé-ventru ; anthères à loges mutiques ; capsule à plusieurs graines grosses.
			{4} Feuilles alternes, rarement verticillées, 1 ou 2 fois pennatiséquées à divisions parallèles ; calice à 4-5 lobes irréguliers ou à 2 lèvres ; capsule ovoïde ou lancéolée, acuminée ; graines ovoïdes-trigones ; plantes vivaces ou bisannuelles	**PEDICULARIS 562**
			{4} Feuilles opposées, simplement dentées ou crénelées ; calice à 4 dents courtes et régulières ; capsule suborbiculaire comprimée ; graines en rein, aplaties, ailées tout autour ; plantes annuelles.	**RHINANTHUS 561**
		{3} Calice non ou à peine renflé ; anthères à loges mucronées ou aristées à la base ; capsule à 1-4 graines ou à graines nombreuses et très petites.
			{4} Capsule glabre, ne renfermant que 1-4 graines.
				Corolle bilabiée en masque, à lèvre supérieure eu casque, comprimée-carénée avec bords repliés en dessous ; capsule ovoïde-acuminée, comprimée-arquée, à 2-4 graines ovoïdes-subtrigones ; plantes annuelles, rigides.	**MELAMPYRUM 563**
				Corolle à 5 lobes presque égaux, à peine à 2 lèvres, la supérieure peu ou point concave ; capsule globuleuse, presque drupacée, à 1 seule graine subglobuleuse ; plante vivace, succulente.	**TOZZIA 564**
			{4} Capsule ordinairement pubescente, à graines nombreuses et très petites.
				Lèvre inférieure de la corolle à 3 lobes émarginés ou bilobés ; loge inférieure de l'anthère des 2 étamines courtes plus longuement aristée que la supérieure ; plantes grêles, peu élevées.	**EUPHRASIA 558**
				Lèvre inférieure de la corolle à 3 lobes entiers, obtus ; loges des anthères toutes également mucronées à la base ; plantes de 10 à 60 cm.
					Fleurs petites (4-8 mm. de long), nombreuses, en grappes unilatérales ; feuilles entières on à dents peu profondes ; plantes annuelles, ordinairement très rameuses.	**ODONTITES 559**
					Fleurs assez grandes, longues de plus de 8 mm., souvent peu nombreuses, en grappes non unilatérales ; feuilles fortement dentées ou incisées ; plantes annuelles ou vivaces, peu ou pas rameuses.	**BARTSIA 560**