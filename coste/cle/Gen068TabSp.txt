TABLEAU DES ESPÈCES

Feuilles lyrées‑pennatifides, à lobes obtus ; pétales inégaux ; 6 étamines ; plante pubérulente.	**T. nudicaulis 317**
Feuilles entières ou pennatifides, à lobes aigus ; pétales égaux ; 4 étamines ; plante glabre et luisante.	**T. Lepidium 318**