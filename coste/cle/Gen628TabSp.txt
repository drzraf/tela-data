TABLEAU DES ESPÈCES

Sous-arbrisseaux nains de 10-50 cm., à tiges tortueuses ou couchées-diffuses ; fleurs en tètes ou fascicules terminaux.
C Fleurs rouges ou roses, accompagnées de bractées ; feuilles étroites (3-S mm. de large), glabres, persistantes ; périanthe à tube 2-3 fois plus long que le limbe.
Tiges diffuses, grêles, subfiliformes, terminées par des rameaux pubescents ; tube du périanthe pubescent-grisâtre, non strié ; fleurs et fruits très brièvement pédicellés, les fruits tardivement nus, jaunâtres ou fauves.
D. Cneorum 31 CO
Tiges redressées, assez robustes, terminées par des rameaux glabres ; tube ilu périanthe glabre et strié ; fleurs et fruits tout à fait sessiles, les fruits DES de bonne heure, à la fin rouges	**D. striata 3167**
C Fleurs blanches ou d'un vert pourpre, dépourvues de bractées ; feuilles larges de 
3-10 mm., pubescentes dans leur jeunesse ; périanthe à tube pubescent à peine plus long que le limbe .
Feuilles épaisses, coriaces, persistantes, longues de 1-2 cm. et larges de 5-7 mm., luisantes en dessus, ponctuées-glanduleuses en dessous ; fleurs fasciculées par 2-4 ; tiges et rameaux droits ou peu tortueux, ni noueux ni fragiles.	**D. oleoides 3168**
Feuilles minces, molles, caduques, longues de 2-3 cm. et larges de 6-10 mm., non luisantes en dessus ni glanduleuses en dessous ; fleurs fasciculées par 
4-7 ; tiges et rameaux tortueux, noueux, fragiles . . .	**D. alpina 3169**
Arbrisseaux s'élevant de 50 cm. à 1 ou 2 mètres, à tiges dressées ; fleurs en panicule terminale ou en fascicules latéraux.
4» Feuilles lancéolées-linéaires, cuspidées, longues de 3-4 cm. sur 4-6 mm. de large, couvrant toute la longueur des rameaux ; fleurs blanches, en panicule rameuse terminale blanche- tomenteuse ; tiges cassantes.	**D. Gnidium 3170**
4> Feuilles obovales ou oblongues, non cuspidées, longues de 4-12 cm. sur 1-3 cm. de large, réunies en rosette au sommet des rameaux ; fleurs rouges ou jaunâtres, en grappes ou faisceaux latéraux, simples, non tomenteux ; tiges très souples.
Fleurs rouges ou roses, très odorantes, disposées 2-4 en petits faisceaux étalésdressés non feuilles ; feuilles naissant après ou avec les fleurs, minces, molles, caduques ; périanthe velu, à lobes égalant le tube ; fruit mûr rouge.	**D. Mezereum 3171**
Fleurs jaune verdâtre, peu odorantes, disposées 3-10 en petites grappes penchées axillaires ; feuilles précédant les fleurs, épaisses, coriaces, persistantes ; périanthe glabre, à lobes 2-3 fois plus courts que le tube ; fruit noir à la maturité D.Laureola 3172