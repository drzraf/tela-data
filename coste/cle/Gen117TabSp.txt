TABLEAU DES ESPÈCES

Feuilles superficiellement lobées ; pédoncules pins courts que les feuilles ; calice à lobes appliqués sur le fruit ; carpelles velus.
	Plante verte, mollement velue ; fleurs d'un rose vif, très grandes, en longue grappe, nue au sommet ; carpelles canaliculés sur le dos, à bords ailés,membraneux.	**A. pallida 626**
	Plante toute veloutée‑blanchâtre ; fleurs d'un blanc rosé, assez grandes, axillaires et terminales ; carpelles ni canaliculés, ni ailés, à bords obtus .	**A. officinalis 627**
Feuilles palmatipartites ou palmatiséquées, au moins les supérieures ; pédoncules plus longs que les feuilles ; calice à lobes dressés sur le fruit ; carpelles glabres.
	Plante vivace, dépassant 1 mètre, à pubescence étoilée ; feuilles inférieures palmati­partites ; corolle rose, 2‑3 fois plus longue que le calice ; carpelles à dos plan.	**A. cannabina 628**
	Plante annuelle, de 10‑40 cm., à longs poils étalés ; feuilles inférieures orbiculaires-­crénelées ; corolle lilacée, puis bleuâtre, dépassant peu le calice ; carpelles à dos arrondi.	**A. hirsuta 629**