TABLEAU DES ESPÈCES

{1} Sépales aigus, blancs‑scarieux, à carène verte ; plante entièrement glabre, dressée, annuelle.	**S. segetalis 578**
{1} Sépales obtus, concolores, non carénés ; plantes couchées‑étalées, ordinairement pubescentes‑glanduleuses dans le haut.
	{2} Graines toutes ou seulement les inférieures ailées.
		Graines inférieures seules ailées ; 5‑3 étamines ; plantes annuelles, bisannuelles ou pérennantes.	**S. Dillenii 579**
		Graines toutes largement ailées ; 10 étamines ; plantes vivaces.
			Pédicelles égalant la capsule ; sépales ovales, dépassant à peine la capsule ; stipules courtes, larges.	**S. azorica 580**
			Pédicelles 1‑2 fois plus longs que la capsule ; sépales lancéolés, bien plus courts que la capsule ; stipules longues.	**S. marginata 581**
	{2} Graines toutes sans ailes.
		{3} Plantes vivaces, toutes pubescentes‑glanduleuses.
			Sépales égalant les pétales et plus courts que la capsule ; feuilles étroites, longues, non imbriquées.	**S. Lebeliana 582**
			Sépales plus longs que les pétales et que la capsule ; feuilles larges, charnues, courtes, presque imbriquées.	**S. macrorhiza 583**
		{3} Plantes annuelles, bisannuelles ou pérennantes, glanduleuses seulement dans le haut ou glabres.
			{4} Fleurs assez grandes, longues de 3‑5 millimètres ; plantes assez robustes, pérennantes.
				Pédicelles inférieurs égalant à peine 2 fois les fleurs ; sépales un peu plus courts que la capsule.	**S. nicaensis 584**
				Pédicelles inférieurs 2‑4 fois plus longs que les fleurs ; sépales plus longs que la capsule.	**S. longipes 585**
			{4} Fleurs petites, de 2‑3 millimètres de long ; plantes grêles, annuelles ou bisannuelles.
				Pédicelles inférieurs 2‑4 fois plus longs que les fleurs ; fleurs en grappes non ou à peine feuillées à la base.	**S. salsuginea 586**
				Pédicelles inférieurs n'étant pas 2 fois plus longs que les fleurs ; fleurs en grappes feuillées et courtes.
					Pédicelles intérieurs 1‑2 fois plus longs que la capsule ; pétales blanchâtres ; 1‑3 étamines.	**S. insularis 587**
					Pédicelles plus courts que la capsule ou la dépassant peu ; pétales roses ; ordinairement 7‑10 étamines.	**S. rubra 588**