TABLEAU DES ESPÈCES

Petite plante de 2-8 cm., terrestre, jaunâtre ; cymes la plupart terminales ; graines tuberculeuses, ternes.	**M. minor 1327**
Plante longue de 10-30 cm., souvent flottante, verte ; cymes la plupart latérales ; graines presque lisses, luisantes.	**M. rivularis 1328**