TABLEAU DES ESPÈCES

Epillets solitaires, en tôte terminale dressée, sans bractées foliacées ; tige presque nue, les feuilles supérieures étant plus ou moins réduites à des gaines.
Tiges de 10-20 cm., très grêles, trigones, rudes de haut en bas ; épillet petit (6-8 mm.), à écailles roussâtres ; soies peu nombreuses (4-6 par écaille), flexueusescrépues, formant une houppe laineuse peu fournie. . . .	**E. alpinum 3762**
Tiges de 10 à 60 cm., un peu épaisses, subcylindriques, lisses ; épillet assez gros, à écailles noirâtres ; soies nombreuses, droites, non crépues, formant une houppe soyeuse très dense.
Souche longuement rampante-stolonifère ; tiges éparses, arrondies, munies de gaines peu renflées ; feuilles radicales peu nombreuses, molles et lisses.	**E. Scheuchzeri 3763**
Souche fibreuse gazonnante, sans stolons ; tiges en touffe compacte, munies de gaines renflées et lâches ; feuilles radicales nombreuses, raides, rudes aux bords	**E. vaginatum 3764**
Epillets en ombelle munie de bractées, inégalement pédoncules et plus ou moins penchés après la floraison ; tige portant de véritables feuilles.
Pédoncules lisses et glabres ; soies longues de 3-4 cm. ; akène brun, elliptiquelancéolé, apiculé ; feuilles caulinaircs allongées ; souche rampanle-stolonifère.	**E. angustifolium 3765**
Pédoncules scabres ou tomenleux ; soies longues seulement de 1-2 cm. ; akène fauve, arrondi et muliqueau sommet ; feuilles caulinaircs courtes.
Capitules nombreux (4-12), assez gros, tous penchés après la floraison, à pédoncules allongés, scabres et glabres ; soies longues de 2 cm. environ ; feuilles larges de 4-6 mm., planes ; souche courte, fibreuse, gazonuanle.	**E. latifolium 3766**
Capitules peu nombreux (2-6), petits, dressés ou à peine penchés, à pédoncules courts, rudes et tomenleux ; soies longues de 10-15 mm. ; fouilles linéairesétroites, canaliculces ; souche grêle, longuement rampante.
£. gracile 3767