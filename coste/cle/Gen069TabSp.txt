TABLEAU DES ESPÈCES

Silicules arrondies à la base, à graines nombreuses ; sépales à 3 nervures ; feuilles supérieures lancéolées‑aiguës.	**A. saxatile 319**
Silicules échancrées aux 2 bouts, à 1‑4 graines ; sépales à 6 nervures ; feuilles ovales ou obovales, obtuses.	**A. pyrenaicum 320**