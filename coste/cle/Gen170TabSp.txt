TABLEAU DES ESPÈCES

Fleurs jaunes ; pédoncule bien plus long que la feuille ; calice cylindrique, à dents plus courtes que le tube ; ailes de la gousse plus étroites que son diamètre ; plante vivace.	**T. siliquosus 925**
Fleurs d'un pourpre foncé ; pédoncule égalant la feuille ; calice en cloche, à dents plus longues que le tube ; ailes de la gousse aussi larges que son diamètre ; plante annuelle.	**T. purpureus 926**