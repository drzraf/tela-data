TABLEAU DES ESPÈCES

Fleurs jaunes ; pétales entiers ; silicules grandes, aplaties, bien plus longues que les pédoncules ; graines ailées.	**F. clypeata 256**
Fleurs blanches ; pétales bifides ; silicules petites, convexes, plus courtes que les pédoncules ; graines sans aile.	**F. incana 257**