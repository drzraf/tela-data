TABLEAU DES ESPÈCES

Plahtes annuelles, peu élevées, à racine fibreuse ; épis très grêles (1-2 mm. de large), tin peu lâches, souvent violacés.
Feuilles velues, surtout sur la gaine ; 3-10 épis allongés, d'abord dressés puis étalés ; épillets lancéolés, longs de 3 lùm. ; glume supérieure velue ciliée, de moitié plus courte que l'épillet	**D. sanguinalis 3964**
Feuilles et gaines glabres, avec quelqties pdils près de la ligule ; 2-0 épis très grêles, plus ou moins étalés ; épillets elliptiques-aigus, longs de 2 mm . ; glume supérieure pubérulente égalant l'épillet ; plante plus grôle dans toutes ses parties	**D. filiformis 3963**
Plantes vivacos, souvent allongées, à souche rampante ; épis un peu épais, larges au moins de 3 mm., denses, verdâtres avec anthères et stigmates violets-noirâtres 
(gen. Paspalum L).
Tige terminée par 2 épis d'abord dressés et appliqués l'une contre l'autre, longs de 
3-3 cm. sur 2-3 mm. de large, presque glabres ; épillets ovoïdes-aigus, disposés sur 2 rangs, à glumes et glumelles brièvement pubescentes ; ligule très courte et obtuse	**D. vaginata 3966**
Tige munie au sommet de 3-7 épis alternes, écartés, étalés, dressés, longs de 
3-12 cm. et larges de 3-6 mm., velus-laineux ; épillets suborbiculaircs-apiculés, disposés sur 3-4 rangs, à glumes et glumelles longuement ciliées ; ligule lancéolée-aiguë	**D. dilatata 3967**