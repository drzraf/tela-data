TABLEAU DES ESPÈCES

Épi floral très grêle, pauciflore, court, d'abord penché, terminé par des fleurs ; fleurs jaunâtres, alternes, les inférieures pourvues de feuilles, les autres à l'aisselle de bractées plus courtes qu'elles.	**M. alterniflorum 1303**
Épi floral multiflore, allongé, droit, terminé par des fleurs ; fleurs rosées, verticillées, en épis terminaux munis de bractées ne dépassant pas les fleurs, nullement feuillés.	**M. spicatum 1304**
Épi floral multiflore, très allongé, feuillé au sommet ; fleurs rosées, toutes en verticilles espacés à l'aisselle des feuilles qui les dépassent longuement.	**M. verticillatum 1305**