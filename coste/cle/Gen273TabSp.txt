TABLEAU DES ESPÈCES

Plante de 30-60 cm. ; feuilles vertes, ovales dans leur pourtour, les inférieures à pétiole triangulaire, les supérieures à gaine courte ; ombelle centrale à 5-10 rayons ; involucre à folioles réfléchies ; fruit oblong, atténué à la base.	**F. Ferulago 1518**
Plantes hautes de 1-2 mètres ; feuilles triangulaires dans leur pourtour, les inférieures a pétiole cylindrique, les supérieures à gaine grande ; ombelle centrale à 20-40 rayons ; pas d'involucre ; fruit arrondi aux deux bouts.
	Feuilles molles, vertes sur les deux faces, à lanières étroitement linéaires ; fruit gros, ovale ou elliptique, non glauque.	**F. communis 1519**
	Feuilles fermes, un peu charnues, vertes et luisantes en dessus, très glauques en dessous, à lanières assez largement linéaires ; fruit moins gros, oblong ou elliptique-allongé, d'un glauque pruineux.	**F. glauca 1520**