CREATE TABLE `nva_v2_03` (
  `num_nom_vernaculaire` int(7) NOT NULL,
  `nom_vernaculaire` varchar(40) NOT NULL,
  `code_langue` varchar(10) NOT NULL,
  `num_genre` int(1) NOT NULL,
  `num_nombre` int(1) NOT NULL,
  `notes` varchar(40) NOT NULL,
PRIMARY KEY ( `num_nom_vernaculaire` ) 
) ENGINE = MYISAM DEFAULT CHARSET = utf8;

CREATE TABLE IF NOT EXISTS `nva_index_v2_03` (
  `num_taxon` int(11) NOT NULL DEFAULT '0',
  `num_nom` int(11) NOT NULL DEFAULT '0',
  `num_nom_vernaculaire` int(7) NOT NULL DEFAULT '0',
  `code_pays` char(2) NOT NULL DEFAULT '',
  `code_emploi` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`num_nom`,`num_nom_vernaculaire`,`code_pays`,`num_taxon`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


 CREATE  TABLE IF NOT EXISTS  `nva_meta` (  `guid` varchar( 255  )  NOT  NULL DEFAULT  '',
 `langue_meta` varchar( 2  )  NOT  NULL DEFAULT  '',
 `code` varchar( 20  )  DEFAULT NULL ,
 `version` varchar( 20  )  DEFAULT NULL ,
 `titre` varchar( 255  )  DEFAULT NULL ,
 `description` text,
 `mots_cles` varchar( 510  )  DEFAULT NULL ,
 `citation` varchar( 255  )  DEFAULT NULL ,
 `url_tech` varchar( 510  )  DEFAULT NULL ,
 `url_projet` varchar( 510  )  DEFAULT NULL ,
 `source` text,
 `createurs` text,
 `editeur` text,
 `contributeurs` text,
 `droits` text,
 `url_droits` varchar( 510  )  DEFAULT NULL ,
 `langue` varchar( 255  )  DEFAULT NULL ,
 `date_creation` varchar( 30  )  DEFAULT NULL ,
 `date_validite` varchar( 255  )  DEFAULT NULL ,
 `couverture_spatiale` varchar( 510  )  DEFAULT NULL ,
 `couverture_temporelle` varchar( 510  )  DEFAULT NULL ,
 `web_services` varchar( 255  )  NOT  NULL ,
 PRIMARY  KEY (  `guid` ,  `langue_meta`  )  ) ENGINE  =  MyISAM  DEFAULT CHARSET  = utf8;
 
 INSERT INTO `nva_meta` (`guid`, `langue_meta`, `code`, `version`, `titre`, `description`, `mots_cles`, `citation`, `url_tech`, `url_projet`, `source`, `createurs`, `editeur`, `contributeurs`, `droits`, `url_droits`, `langue`, `date_creation`, `date_validite`, `couverture_spatiale`, `couverture_temporelle`, `web_services`) VALUES
('urn:lsid:tela-botanica.org:nva:2_03', 'fr', 'nva', '2_03', 'Noms vernaculaires des Antilles', '', 'eFlore, ontologies, flore, antilles', NULL, 'http://www.tela-botanica.org/wikini/isfgm/wakka.php?wiki=PagePrincipale', 'http://www.tela-botanica.org/page:liste_projets?id_projet=23', '', 'p.nom=Fournet,p.prenom=Jacques,p.courriel=fournet.j@wanadoo.fr', 'nom=Tela Botanica,guid=urn:lsid:tela-botanica.org,courriel=accueil@tela-botanica.org,telephone=+334 67 52 41 22,url.info=http://www.tela-botanica.org/page:association_tela_botanica,url.logo=http://www.tela-botanica.org/sites/accueil/generique/images/graphisme/logo_tela_fond_blanc.png,type=organisation,acronyme=TB,adresse=Association Tela Botanica - Institut de Botanique - 167 rue Auguste Broussonnet 34090 MONTPELLIER FRANCE,latitude.wgs84=43.615892,longitude.wgs84=3.871736,contact.prenom=Jean-Pascal,contact.nom=MILCENT,contact.courriel=jpm@tela-botanica.org,contact.role=administrateur des données', 'p.nom=Feldmann,p.prenom=Phillipe,o.nom=SFO;p.nom=Descourvières,p.prenom=Pascal,o.nom=SFO;p.nom=Jérémie,p.prenom=Joël,o.nom=MNHN;p.nom=Lavocat-Bernard,
p.prenom=Élisabeth;p.nom=Lurel,p.prenom=Félix;p.nom=Rollet,p.prenom=Bernard;p.nom=Sastre,p.prenom=Claude,o.nom=MNHN;p.nom=Flower,p.prenom=Jean-Marie,o.nom=CBAF;p.nom=Bernard,p.prenom=Jean-François;p.nom=Hoff,p.prenom=Michel,o.nom=Université de Strasbourg;p.nom=Imbert,p.prenom=Daniel,o.nom=Université Antilles Guyane;p.nom=Joseph,p.prenom=Philippe,o.nom=CBAF;p.nom=Rousteau,p.prenom=Alain,o.nom=Université Antilles Guyane;', NULL, NULL, 'fr', '2007-10-29', NULL, NULL, NULL, 'noms-vernaculaires:0.1;Meta-donnees:0.1');

CREATE TABLE IF NOT EXISTS `nva_ontologies` (
  `id` int(5) NOT NULL,
  `classe_id` int(5) NOT NULL,
  `nom` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `code` varchar(100) NOT NULL,
  `complements` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



