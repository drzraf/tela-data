--
-- Structure de la table `eflore_meta`
--
CREATE TABLE IF NOT EXISTS `eflore_meta` (
  `guid` varchar(255) NOT NULL DEFAULT '',
  `langue_meta` varchar(2) NOT NULL DEFAULT '',
  `code` varchar(20) DEFAULT NULL,
  `version` varchar(20) DEFAULT NULL,
  `titre` varchar(255) DEFAULT NULL,
  `description` text,
  `mots_cles` varchar(510) DEFAULT NULL,
  `citation` varchar(255) DEFAULT NULL,
  `url_tech` varchar(510) DEFAULT NULL,
  `url_projet` varchar(510) DEFAULT NULL,
  `source` text,
  `createurs` text,
  `editeur` text,
  `contributeurs` text,
  `droits` text,
  `url_droits` varchar(510) DEFAULT NULL,
  `langue` varchar(255) DEFAULT NULL,
  `date_creation` varchar(30) DEFAULT NULL,
  `date_validite` varchar(255) DEFAULT NULL,
  `couverture_spatiale` varchar(510) DEFAULT NULL,
  `couverture_temporelle` varchar(510) DEFAULT NULL,
  `web_services` varchar(255) NOT NULL,
  PRIMARY KEY (`guid`,`langue_meta`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `eflore_meta`
--
INSERT INTO `eflore_meta` (`guid`, `langue_meta`, `code`, `version`, `titre`, `description`, `mots_cles`, `citation`, `url_tech`, `url_projet`, `source`, `createurs`, `editeur`, `contributeurs`, `droits`, `url_droits`, `langue`, `date_creation`, `date_validite`, `couverture_spatiale`, `couverture_temporelle`, `web_services`) VALUES
('urn:lsid:tela-botanica.org:eflore:2011', 'fr', 'commun', '2011', '', 'Le projet commun (ou eflore) contient toute les informations commune à tous les projets d''eflore. Notamment les ontologies utilisées dans les table des méta-données', 'eFlore, ontologies, flore, france', NULL, '', '', '', 'p.prenom=Jean-Pascal,p.nom=Milcent,p.courriel=jpm@tela-botanica.org', 'nom=Tela Botanica,guid=urn:lsid:tela-botanica.org,courriel=accueil@tela-botanica.org,telephone=+334 67 52 41 22,url.info=http://www.tela-botanica.org/page:association_tela_botanica,url.logo=http://www.tela-botanica.org/sites/accueil/generique/images/graphisme/logo_tela_fond_blanc.png,type=organisation,acronyme=TB,adresse=Association Tela Botanica - Institut de Botanique - 167 rue Auguste Broussonnet 34090 MONTPELLIER FRANCE,latitude.wgs84=43.615892,longitude.wgs84=3.871736,contact.prenom=Jean-Pascal,contact.nom=MILCENT,contact.courriel=jpm@tela-botanica.org,contact.role=administrateur des données', NULL, NULL, NULL, 'fr', '2011-08-09', NULL, 'iso-3166-1.id=FX;', NULL, 'Ontologies:0.1;Meta-donnees:0.1');

--
-- Structure de la table `eflore_ontologies_v2011`
--
CREATE TABLE IF NOT EXISTS `eflore_ontologies` (
  `id` int(1) NOT NULL DEFAULT '0',
  `classe_id` int(1) DEFAULT NULL,
  `nom` varchar(50) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `code` varchar(100) DEFAULT NULL,
  `complements` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
