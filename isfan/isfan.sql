-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le : Lun 10 Juin 2013 à 16:42
-- Version du serveur: 5.5.16
-- Version de PHP: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `tb_eflore`
--

-- --------------------------------------------------------

--
-- Structure de la table `isfan_v2013`
--

CREATE TABLE IF NOT EXISTS `isfan_v2013` (
  `num_nom` int(10) NOT NULL,
  `presence` varchar(5) NOT NULL,
  `statut_introduction` varchar(5) NOT NULL,
  `statut_origine` varchar(5) NOT NULL,
  `nom_addendum` varchar(50) NOT NULL,
  `basionyme` int(10) NOT NULL,
  `rang` int(7) NOT NULL,
  `auteur` varchar(100) NOT NULL,
  `annee` int(4) NOT NULL,
  `type_epithete` varchar(10) NOT NULL,
  `syn_mal_applique` int(2) NOT NULL,
  `nom_sci_au` text NOT NULL,
  `nom_sci` varchar(500) NOT NULL,
  `num_tax_sup` int(10) NOT NULL,
  `statut_syn` varchar(2) NOT NULL,
  `num_nom_retenu` int(10) NOT NULL,
  `genre` varchar(50) NOT NULL,
  `notes` varchar(50) NOT NULL,
  `epithete_sp` varchar(50) NOT NULL,
  `epithete_infra_sp` varchar(50) NOT NULL,
  `hybride_parent_01` int(10) NOT NULL,
  `hybride_parent_02` int(10) NOT NULL,
  `biblio_origine` varchar(50) NOT NULL,
  `exclure_taxref` int(11) DEFAULT '0',
  `infra_bas` int(1) NOT NULL,
  `nom_sci_html` varchar(500) DEFAULT NULL,
  `famille` varchar(255) DEFAULT NULL,
  `nom_complet` varchar(500) DEFAULT NULL,
  `num_taxonomique` int(9) DEFAULT NULL,
  `hierarchie` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`num_nom`),
  KEY `hybride_parent_01` (`hybride_parent_01`),
  KEY `hybride_parent_02` (`hybride_parent_02`),
  KEY `num_tax_sup` (`num_tax_sup`),
  KEY `rang` (`rang`),
  KEY `num_nom_retenu` (`num_nom_retenu`),
  KEY `nom_sci` (`nom_sci`(255))
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `isfan_meta`
--

CREATE TABLE IF NOT EXISTS `isfan_meta` (
  `guid` varchar(255) NOT NULL DEFAULT 'urn:lsid:tela-botanica.org:#projet:#version',
  `langue_meta` varchar(2) NOT NULL DEFAULT 'fr',
  `code` varchar(20) NOT NULL,
  `version` varchar(20) NOT NULL,
  `titre` varchar(255) DEFAULT NULL,
  `description` text,
  `mots_cles` varchar(510) DEFAULT NULL,
  `citation` varchar(255) DEFAULT NULL,
  `url_tech` varchar(510) DEFAULT NULL,
  `url_projet` varchar(510) DEFAULT NULL,
  `source` text,
  `createurs` text,
  `editeur` varchar(1000) NOT NULL DEFAULT 'nom=Tela Botanica;guid=urn:lsid:tela-botanica.org;courriel=accueil@tela-botanica.org;telephone=+334 67 52 41 22;url.info=http://www.tela-botanica.org/page:association_tela_botanica;url.logo=http://www.tela-botanica.org/sites/accueil/generique/images/graphisme/logo_tela_fond_blanc.png;type=organisation;acronyme=TB;adresse=Association Tela Botanica - Institut de Botanique - 167 rue Auguste Broussonnet 34090 MONTPELLIER, FRANCE;latitude.wgs84=43.615892;longitude.wgs84=3.871736;contact.prenom=Jean-Pascal;contact.nom=MILCENT;contact.courriel=jpm@tela-botanica.org;contact.role=administrateur des données;',
  `contributeurs` text,
  `droits` text,
  `url_droits` varchar(510) DEFAULT NULL,
  `langue` varchar(255) NOT NULL DEFAULT 'fr',
  `date_creation` varchar(30) NOT NULL DEFAULT 'YYYY-MM-DD',
  `date_validite` varchar(255) DEFAULT NULL,
  `couverture_spatiale` varchar(510) NOT NULL DEFAULT 'iso-3166-1.id=FX;',
  `couverture_temporelle` varchar(510) DEFAULT NULL,
  `web_services` varchar(510) DEFAULT NULL COMMENT 'ontologies,meta-donnees,noms,taxons,aide,textes,images,observations,noms-vernaculaires',
  PRIMARY KEY (`guid`,`langue_meta`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `isfan_meta`
--

INSERT INTO `isfan_meta` (`guid`, `langue_meta`, `code`, `version`, `titre`, `description`, `mots_cles`, `citation`, `url_tech`, `url_projet`, `source`, `createurs`, `editeur`, `contributeurs`, `droits`, `url_droits`, `langue`, `date_creation`, `date_validite`, `couverture_spatiale`, `couverture_temporelle`, `web_services`) VALUES
('urn:lsid:tela-botanica.org:isfan:2013', 'fr', 'ISFAN', '2013', 'Index synonymique flore d''Afrique du Nord', 'Fruit d''une collaboration entre les Conservatoire et Jardin botaniques de la Ville de Genève (CJB) et le South African National Biodiversity Institute (SANBI), cette base de données regroupe de l''information sur les espèces de plantes à fleurs d''Afrique ', 'référentiel, nom, flore, afrique', 'DOBIGNARD, A. & C. CHATELAIN (2010-2013) Index synonymique et bibliographique de la flore d''Afrique du Nord.', '', 'http://www.ville-ge.ch/musinfo/bd/cjb/africa/', 'DOBIGNARD, A. & C. CHATELAIN (2010-2013) Index synonymique et bibliographique de la flore d''Afrique du Nord. vol. 1-5. ', 'p.prenom=Cyrille,p.nom=Chatelain,p.courriel=Cyrille.Chatelain@ville-ge.ch,o.nom=Conservatoire et jardin botaniques ville de Genéve;p.prenom=Alain,p.nom=Dobignard', 'nom=Tela Botanica,guid=urn:lsid:tela-botanica.org,courriel=accueil@tela-botanica.org,telephone=+334 67 52 41 22,url.info=http://www.tela-botanica.org/page:association_tela_botanica,url.logo=http://www.tela-botanica.org/sites/accueil/generique/images/graphisme/logo_tela_fond_blanc.png,type=organisation;acronyme=TB,adresse=Association Tela Botanica - Institut de Botanique - 167 rue Auguste Broussonnet 34090 MONTPELLIER FRANCE,latitude.wgs84=43.615892,longitude.wgs84=3.871736,contact.prenom=Jean-Pascal,contact.nom=MILCENT,contact.courriel=jpm@tela-botanica.org,contact.role=administrateur des données', '', 'Copyright © Conservatoire et jardin botaniques de la ville de Genéve 2010', NULL, 'fr', '2013-06-01', NULL, 'iso-3166-1.id=DZ;iso-3166-1.id=EG;iso-3166-1.id=LY;iso-3166-1.id=MA;iso-3166-1.id=EH;iso-3166-1.id=SD;iso-3166-1.id=TN;', NULL, 'metaDonnees:0.1;noms:0.1;taxons:0.1');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
