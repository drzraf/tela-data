--
-- Structure de la table `sptb_meta`
--
CREATE TABLE IF NOT EXISTS `sptb_meta` (
  `guid` varchar(255) NOT NULL DEFAULT '',
  `langue_meta` varchar(2) NOT NULL DEFAULT '',
  `code` varchar(20) DEFAULT NULL,
  `version` varchar(20) DEFAULT NULL,
  `titre` varchar(255) DEFAULT NULL,
  `description` text,
  `mots_cles` varchar(510) DEFAULT NULL,
  `citation` varchar(255) DEFAULT NULL,
  `url_tech` varchar(510) DEFAULT NULL,
  `url_projet` varchar(510) DEFAULT NULL,
  `source` text,
  `createurs` text,
  `editeur` text,
  `contributeurs` text,
  `droits` text,
  `url_droits` varchar(510) DEFAULT NULL,
  `langue` varchar(255) DEFAULT NULL,
  `date_creation` varchar(30) DEFAULT NULL,
  `date_validite` varchar(255) DEFAULT NULL,
  `couverture_spatiale` varchar(510) DEFAULT NULL,
  `couverture_temporelle` varchar(510) DEFAULT NULL,
  `web_services` varchar(255) NOT NULL,
  PRIMARY KEY (`guid`,`langue_meta`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sptb_meta`
--
INSERT INTO `sptb_meta` (`guid`, `langue_meta`, `code`, `version`, `titre`, `description`, `mots_cles`, `citation`, `url_tech`, `url_projet`, `source`, `createurs`, `editeur`, `contributeurs`, `droits`, `url_droits`, `langue`, `date_creation`, `date_validite`, `couverture_spatiale`, `couverture_temporelle`, `web_services`) VALUES
('urn:lsid:tela-botanica.org:sptb:2012', 'fr', 'sptb', '2012', 'Statuts de protection synthétisés par Tela botanica', '', 'eFlore, statut, protection, france', NULL, '', '', '', 'p.prenom=Jean-Pascal,p.nom=Milcent,p.courriel=jpm@tela-botanica.org', 'nom=Tela Botanica,guid=urn:lsid:tela-botanica.org,courriel=accueil@tela-botanica.org,telephone=+334 67 52 41 22,url.info=http://www.tela-botanica.org/page:association_tela_botanica,url.logo=http://www.tela-botanica.org/sites/accueil/generique/images/graphisme/logo_tela_fond_blanc.png,type=organisation,acronyme=TB,adresse=Association Tela Botanica - Institut de Botanique - 167 rue Auguste Broussonnet 34090 MONTPELLIER FRANCE,latitude.wgs84=43.615892,longitude.wgs84=3.871736,contact.prenom=Jean-Pascal,contact.nom=MILCENT,contact.courriel=jpm@tela-botanica.org,contact.role=administrateur des données', NULL, NULL, NULL, 'fr', '2012-07-10', NULL, 'iso-3166-1.id=FX;', NULL, 'Ontologies:0.1;Meta-donnees:0.1');

--
-- Structure de la table `sptb_especes_v2012`
--

CREATE TABLE IF NOT EXISTS `sptb_especes_v2012` (
  `nom_sci` text NOT NULL,
  `num_nom` int(10) NOT NULL,
  `1` int(1) NOT NULL,
  `2` int(1) NOT NULL,
  `3` int(1) NOT NULL,
  `4` int(1) NOT NULL,
  `5` int(1) NOT NULL,
  `6` int(1) NOT NULL,
  `7` int(1) NOT NULL,
  `8` int(1) NOT NULL,
  `9` int(1) NOT NULL,
  `10` int(1) NOT NULL,
  `11` int(1) NOT NULL,
  `12` int(1) NOT NULL,
  `13` int(1) NOT NULL,
  `14` int(1) NOT NULL,
  KEY `num_nom` (`num_nom`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Structure de la table `sptb_lois_v2012`
--

CREATE TABLE IF NOT EXISTS `sptb_lois_v2012` (
  `id` int(10) NOT NULL,
  `texte` text NOT NULL,
  `date_mise_a_jour` varchar(50) NOT NULL,
  `hyperlien_legifrance` text NOT NULL,
  `statut` text NOT NULL COMMENT 'sous section = « statut »',
  `regle` text NOT NULL,
  `zone_application` text NOT NULL,
  `code_zone_application` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

