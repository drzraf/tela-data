1.	Créer la structure de la table à l'aide du fichier bdtfx_vx_xx.sql correspondant à votre version en utilisant
	l'onglet "Importer" de PhpMyAdmin
2.	Vérifier la présence du fichier tsv contenant les données à l'emplacement :
	http://www.tela-botanica.org/eflore/donnees/bdtfx/x.xx/bdtfx_vx_xx_ref.txt
3.	Se connecter sur le serveur en root. Puis lancer les commandes suivantes :
	/usr/local/mysql/bin/mysql -u telabotap -p --local-infile=1
	use tb_eflore
	LOAD DATA LOCAL INFILE '/home/telabotap/www/eflore/donnees/bdtfx/x.xx/bdtfx_vx_xx_ref.txt'
	REPLACE INTO TABLE bdtfx_vx_xx 
	CHARACTER SET utf8 
	FIELDS TERMINATED BY '\t'  
	ESCAPED BY '\\' 
	IGNORE 1 LINES;
4.	Générer (ou compléter) la table bdtfx_meta avec les informations de la nouvelle version si la requête SQL
	d'insertion n'était pas présente dans le fichier bdtfx_vx_xx.sql
5.	Lancer le script de création des noms scientifiques au format HTML :
	/usr/loca/bin/php cli.php creationcol -a nomSciHtml -t bdtfx_vx_xx -v 3