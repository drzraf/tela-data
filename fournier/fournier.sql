--
-- Structure de la table `fournier_meta`
--

DROP TABLE IF EXISTS `fournier_meta`;
CREATE TABLE IF NOT EXISTS `fournier_meta` (
  `guid` varchar(255) NOT NULL DEFAULT 'urn:lsid:tela-botanica.org:#projet:#version',
  `langue_meta` varchar(2) NOT NULL DEFAULT 'fr',
  `code` varchar(20) NOT NULL,
  `version` varchar(20) NOT NULL,
  `titre` varchar(255) DEFAULT NULL,
  `description` text,
  `mots_cles` varchar(510) DEFAULT NULL,
  `citation` varchar(255) DEFAULT NULL,
  `url_tech` varchar(510) DEFAULT NULL,
  `url_projet` varchar(510) DEFAULT NULL,
  `source` text,
  `createurs` text,
  `editeur` varchar(1000) NOT NULL DEFAULT 'nom=Tela Botanica;guid=urn:lsid:tela-botanica.org;courriel=accueil@tela-botanica.org;telephone=+334 67 52 41 22;url.info=http://www.tela-botanica.org/page:association_tela_botanica;url.logo=http://www.tela-botanica.org/sites/accueil/generique/images/graphisme/logo_tela_fond_blanc.png;type=organisation;acronyme=TB;adresse=Association Tela Botanica - Institut de Botanique - 167 rue Auguste Broussonnet 34090 MONTPELLIER, FRANCE;latitude.wgs84=43.615892;longitude.wgs84=3.871736;contact.prenom=Jean-Pascal;contact.nom=MILCENT;contact.courriel=jpm@tela-botanica.org;contact.role=administrateur des données;',
  `contributeurs` text,
  `droits` text,
  `url_droits` varchar(510) DEFAULT NULL,
  `langue` varchar(255) NOT NULL DEFAULT 'fr',
  `date_creation` varchar(30) NOT NULL DEFAULT 'YYYY-MM-DD',
  `date_validite` varchar(255) DEFAULT NULL,
  `couverture_spatiale` varchar(510) NOT NULL DEFAULT 'iso-3166-1.id=FX;',
  `couverture_temporelle` varchar(510) DEFAULT NULL,
  `web_services` varchar(510) DEFAULT NULL COMMENT 'ontologies,meta-donnees,noms,taxons,aide,textes,images,observations,noms-vernaculaires',
  PRIMARY KEY (`guid`,`langue_meta`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `fournier_meta`
--

INSERT INTO `fournier_meta` (`guid`, `langue_meta`, `code`, `version`, `titre`, `description`, `mots_cles`, `citation`, `url_tech`, `url_projet`, `source`, `createurs`, `editeur`, `contributeurs`, `droits`, `url_droits`, `langue`, `date_creation`, `date_validite`, `couverture_spatiale`, `couverture_temporelle`, `web_services`) VALUES
('urn:lsid:tela-botanica.org:bdtfx:1.00', 'fr', 'FOURNIER', '1.00', 'Base de données des noms de Fournier', 'Intégration des noms scientifiques de Fournier.', 'référentiel, nom, flore, france', NULL, 'http://www.tela-botanica.org/wikini/eflore/wakka.php?wiki=IntegrationFournier', NULL, NULL, NULL, '', NULL, 'Copyright © Clapas.org (2011). Tout droits réservés.', NULL, 'fr', '2011-07-29', NULL, 'iso-3166-1.id=FX;', NULL, 'metaDonnees:0.1;noms:0.1;taxons:0.1');

--
-- Structure de la table `fournier_v1_00`
--

DROP TABLE IF EXISTS `fournier_v1_00`;
CREATE TABLE IF NOT EXISTS `fournier_v1_00` (
  `num_nom` bigint(20) NOT NULL COMMENT 'Identifiant numérique unique du nom scientifique',
  `num_nom_retenu` varchar(9) NOT NULL COMMENT 'Identifiant numérique du nom scientifique (num_nom) retenu pour désigner le taxon',
  `num_tax_sup` varchar(9) DEFAULT NULL COMMENT 'Identifiant numérique du nom (num_nom) de rang supérieur dans la classification par défaut de l''index',
  `rang` int(4) NOT NULL,
  `nom_sci` varchar(500) DEFAULT NULL COMMENT 'Nom scientifique complet sans auteur, année ou référence bibliographique',
  `nom_supra_generique` varchar(100) DEFAULT NULL,
  `genre` varchar(100) DEFAULT NULL,
  `epithete_infra_generique` varchar(100) DEFAULT NULL,
  `epithete_sp` varchar(100) DEFAULT NULL,
  `type_epithete` varchar(100) DEFAULT NULL,
  `epithete_infra_sp` varchar(100) DEFAULT NULL,
  `cultivar_groupe` varchar(100) DEFAULT NULL,
  `cultivar` varchar(100) DEFAULT NULL,
  `nom_commercial` varchar(100) DEFAULT NULL,
  `auteur` varchar(100) DEFAULT NULL COMMENT '	Intitulé de l''auteur du nom (en accord avec le code de nomenclature dont il dépend)',
  `annee` varchar(4) DEFAULT NULL,
  `biblio_origine` varchar(500) DEFAULT NULL,
  `nom_addendum` varchar(500) DEFAULT NULL,
  `homonyme` char(1) DEFAULT NULL,
  `basionyme` varchar(9) DEFAULT NULL,
  `synonyme_proparte` char(1) DEFAULT NULL,
  `synonyme_douteux` char(1) DEFAULT NULL,
  `synonyme_mal_applique` char(1) DEFAULT NULL,
  `hybride_parent_01` varchar(9) DEFAULT NULL COMMENT 'Identifiant du nom (num_nom) du parent numéro 1 (dans le cas d''un hybride)',
  `hybride_parent_01_notes` text,
  `hybride_parent_02` varchar(9) DEFAULT NULL COMMENT 'Identifiant du nom (num_nom) du parent numéro 2 (dans le cas d''un hybride)',
  `hybride_parent_02_notes` text,
  `synonyme_orthographique` varchar(9) DEFAULT NULL COMMENT '	Contient l''identifiant numérique du nom (num_nom) correctement orthographié, si le nom est un synonyme orthographique. Dans le cas contraire, le champ reste vide. ',
  `notes` text COMMENT '	Notes complémentaires, remarques, état d''avancement des recherches concernant le nom...',
  `champs_supplementaires` text COMMENT 'Contient les champs supplémentaires sous forme XML.',
  `nom_scientifique` varchar(255) DEFAULT NULL,
  `nom_vernaculaire` varchar(255) DEFAULT NULL,
  `page` varchar(45) DEFAULT NULL,
  `code_taxon` varchar(45) DEFAULT NULL,
  `milieu` varchar(255) DEFAULT NULL,
  `floraison` varchar(45) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `sol` varchar(45) DEFAULT NULL,
  `rarete_region_alt` varchar(255) DEFAULT NULL,
  `region_bota_monde` varchar(100) DEFAULT NULL,
  `etymologie` varchar(255) DEFAULT NULL,
  `taille` varchar(45) DEFAULT NULL,
  `formule_hybridite` varchar(255) DEFAULT NULL,
  `culture` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`num_nom`),
  KEY `num_nom_retenu` (`num_nom_retenu`),
  KEY `num_tax_sup` (`num_tax_sup`),
  KEY `nom_sci` (`nom_sci`),
  KEY `hybride_parent_01` (`hybride_parent_01`),
  KEY `hybride_parent_02` (`hybride_parent_02`),
  KEY `synonyme_orthographique` (`synonyme_orthographique`),
  KEY `genre` (`genre`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;