-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le : Jeu 26 Avril 2012 à 12:12
-- Version du serveur: 5.5.16
-- Version de PHP: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `tb_eflore`
--

-- --------------------------------------------------------

--
-- Structure de la table `baseflor_rang_sup_ecologie_v2013_09_23`
--

CREATE TABLE IF NOT EXISTS `baseflor_rang_sup_ecologie_v2013_09_23` (
  `cle` int(11) NOT NULL,
  `num_nomen` int(10) NOT NULL,
  `bdnt` varchar(5) NOT NULL,
  `ve_lumiere_min` varchar(1) DEFAULT NULL,
  `ve_lumiere_max` varchar(1) DEFAULT NULL,
  `ve_temperature_min` varchar(1) DEFAULT NULL,
  `ve_temperature_max` varchar(1) DEFAULT NULL,
  `ve_continentalite_min` varchar(1) DEFAULT NULL,
  `ve_continentalite_max` varchar(1) DEFAULT NULL,
  `ve_humidite_atmos_min` varchar(1) DEFAULT NULL,
  `ve_humidite_atmos_max` varchar(1) DEFAULT NULL,
  `ve_humidite_edaph_min` varchar(2) DEFAULT NULL,
  `ve_humidite_edaph_max` varchar(2) DEFAULT NULL,
  `ve_reaction_sol_min` varchar(1) DEFAULT NULL,
  `ve_reaction_sol_max` varchar(1) DEFAULT NULL,
  `ve_nutriments_sol_min` varchar(1) DEFAULT NULL,
  `ve_nutriments_sol_max` varchar(1) DEFAULT NULL,
  `ve_salinite_min` varchar(1) DEFAULT NULL,
  `ve_salinite_max` varchar(1) DEFAULT NULL,
  `ve_texture_sol_min` varchar(1) DEFAULT NULL,
  `ve_texture_sol_max` varchar(1) DEFAULT NULL,
  `ve_mat_org_sol_min` varchar(1) DEFAULT NULL,
  `ve_mat_org_sol_max` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`cle`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
