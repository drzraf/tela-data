
--
-- insertion table metadonnees pour baseflor v2013-09-23

INSERT INTO `baseflor_meta` (`guid`, `langue_meta`, `code`, `version`, `titre`, `description`, `mots_cles`, `citation`, `url_tech`, `url_projet`, `source`, `createurs`, `editeur`, `contributeurs`, `droits`, `url_droits`, `langue`, `date_creation`, `date_validite`, `couverture_spatiale`, `couverture_temporelle`, `web_services`) VALUES
('urn:lsid:tela-botanica.org:baseflor:2013_09_23',
 'fr',
 'baseflor', 
'2013_09_23',
'baseflor',
'Index botanique, écologique et chorologique de la flore de France [Extrait du programme Catminat, programme personnel de Ph. Julve]. (Phytosociological, Ecological and botanical data upon French flora, with statistics). ', 
'données biologiques, données écologiques, catminat, france, BDTFX, BDBFX, BDAFX',
'Julve, Ph., 2013 ff. - Baseflor. Index botanique, écologique et chorologique de la flore de France. Version : 23 septembre 2013. http://perso.wanadoo.fr/philippe.julve/catminat.htm',
 'http://philippe.julve.pagesperso-orange.fr/catminat.htm#INDEXFLORE', 
'http://www.tela-botanica.org/page:liste_projets?id_projet=18&act=documents&id_repertoire=98',
NULL , 
 'p.nom=JULVE,p.prenom=Philippe,p.courriel=philippe.julve@wanadoo.fr', 
'nom=Tela Botanica,guid=urn:lsid:tela-botanica.org,courriel=accueil@tela-botanica.org,telephone=+334 67 52 41 22,url.info=http://www.tela-botanica.org/page:association_tela_botanica,url.logo=http://www.tela-botanica.org/sites/accueil/generique/images/graphisme/logo_tela_fond_blanc.png,type=organisation,acronyme=TB,adresse=Association Tela Botanica - Institut de Botanique - 167 rue Auguste Broussonnet 34090 MONTPELLIER FRANCE,latitude.wgs84=43.615892,longitude.wgs84=3.871736,contact.prenom=Jean-Pascal,contact.nom=MILCENT,contact.courriel=jpm@tela-botanica.org,contact.role=administrateur des données;',
 NULL,
NULL, 
NULL, 
'fr',
 '2013-09-23',
 NULL,
 'iso-3166-1.id=FR;', 
NULL, 
'informations;ontologies'
);
