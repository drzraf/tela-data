-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le : Mer 21 Mars 2012 à 13:27
-- Version du serveur: 5.5.16
-- Version de PHP: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `tb_eflore`
--

-- --------------------------------------------------------

--
-- Structure de la table `baseflor_v2013_09_23`
--

CREATE TABLE IF NOT EXISTS `baseflor_v2013_09_23` (
  `cle` int(11) NOT NULL AUTO_INCREMENT,
  `rang` varchar(11) DEFAULT NULL,
  `catminat_code` varchar(15) DEFAULT 'inconnu',
  `num_taxon_originel` varchar(10) DEFAULT NULL,
  `num_nomen_originel` varchar(10) DEFAULT NULL,
  `nom_sci` text NOT NULL,
  `chorologie` varchar(100) DEFAULT NULL,
  `inflorescence` varchar(100) DEFAULT NULL,
  `sexualite` varchar(25) DEFAULT NULL,
  `ordre_maturation` varchar(25) DEFAULT NULL,
  `pollinisation` varchar(70) DEFAULT NULL,
  `fruit` varchar(25) DEFAULT NULL,
  `dissemination` varchar(30) DEFAULT NULL,
  `couleur_fleur` varchar(70) DEFAULT NULL,
  `macule` varchar(70) DEFAULT NULL,
  `floraison` varchar(5) DEFAULT NULL,
  `type_ligneux` varchar(30) DEFAULT NULL,
hauteur_veg_max varchar(30) DEFAULT NULL,
  `type_bio` varchar(30) DEFAULT NULL,
  `form_vegetale` varchar(100) DEFAULT NULL,
  `carac_ecolo` text,
  `ind_phytosocio` text,
  `ind_carac_trans` text,
  `ind_diff_1` text,
  `ind_diff_2` text,
  `ind_diff_3` text,
  `ve_lumiere` int(1) DEFAULT NULL COMMENT "valence ecologique",
  `ve_temperature` int(1) DEFAULT NULL,
  `ve_continentalite` int(1) DEFAULT NULL,
  `ve_humidite_atmos` int(1) DEFAULT NULL,
  `ve_humidite_edaph` int(2) DEFAULT NULL,
  `ve_reaction_sol` int(1) DEFAULT NULL,
  `ve_nutriments_sol` int(1) DEFAULT NULL,
  `ve_salinite` int(1) DEFAULT NULL,
  `ve_texture_sol` int(1) DEFAULT NULL,
  `ve_mat_org_sol` int(1) DEFAULT NULL,
  `vi_lumiere` char(1) DEFAULT NULL,
  `vi_temperature` char(1) DEFAULT NULL,
  `vi_continentalite` char(1) DEFAULT NULL,
  `vi_humidite_edaph` varchar(2) DEFAULT NULL,
  `vi_reaction_sol` char(1) DEFAULT NULL,
  `vi_nutriments_sol` char(1) DEFAULT NULL,
  `vi_salinite` char(1) DEFAULT NULL,
  `royaume` varchar(50) DEFAULT NULL,
  `embranchement` varchar(50) DEFAULT NULL,
  `subembranchement` varchar(50) DEFAULT NULL,
  `classe` varchar(50) DEFAULT NULL,
  `subclasse` varchar(50) DEFAULT NULL,
  `sb_clade_intermediaire_1` varchar(50) DEFAULT NULL,
  `sb_clade_intermediaire_2` varchar(50) DEFAULT NULL,
  `sb_clade_intermediaire_3` varchar(50) DEFAULT NULL,
  `superordre` varchar(50) DEFAULT NULL,
  `so_clade_intermediaire` varchar(50) DEFAULT NULL,
  `ordre` varchar(50) DEFAULT NULL,
  `famille` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`cle`),
  KEY `num_taxon_idx` (`num_taxon_originel`),
  KEY `num_nomen_idx` (`num_nomen_originel`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `baseflor_ontologies`
--

CREATE TABLE IF NOT EXISTS `baseflor_ontologies` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `classe_id` int(10) NOT NULL,
  `nom` text NOT NULL,
  `description` text DEFAULT NULL,
  `code` varchar(100) NOT NULL,
  `complements` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `code_idx` (`code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------
--
-- Structure de la table `baseflor_meta`
--
CREATE TABLE IF NOT EXISTS `baseflor_meta` (
`guid` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT 'urn:lsid:tela-botanica.org:#baseflor:#v2013_09_23',
`langue_meta` varchar(2) CHARACTER SET utf8 NOT NULL DEFAULT 'fr',
`code` varchar(20) CHARACTER SET utf8 NOT NULL,
`version` varchar(20) CHARACTER SET utf8 NOT NULL,
`titre` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
`description` text CHARACTER SET utf8,
`mots_cles` varchar(510) CHARACTER SET utf8 DEFAULT NULL,
`citation` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
`url_tech` varchar(510) CHARACTER SET utf8 DEFAULT NULL,
`url_projet` varchar(510) CHARACTER SET utf8 DEFAULT NULL,
`source` text CHARACTER SET utf8,
`createurs` text CHARACTER SET utf8,
`editeur` varchar(1000) CHARACTER SET utf8 NOT NULL DEFAULT 'nom=Tela Botanica,guid=urn:lsid:tela-botanica.org,courriel=accueil@tela-botanica.org,telephone=+334 67 52 41 22,url.info=http://www.tela-botanica.org/page:association_tela_botanica,url.logo=http://www.tela-botanica.org/sites/accueil/generique/images/graphisme/logo_tela_fond_blanc.png,type=organisation,acronyme=TB,adresse=Association Tela Botanica - Institut de Botanique - 167 rue Auguste Broussonnet 34090 MONTPELLIER FRANCE,latitude.wgs84=43.615892,longitude.wgs84=3.871736,contact.prenom=Jean-Pascal,contact.nom=MILCENT,contact.courriel=jpm@tela-botanica.org,contact.role=administrateur des données;',
`contributeurs` text CHARACTER SET utf8,
`droits` text CHARACTER SET utf8,
`url_droits` varchar(510) CHARACTER SET utf8 DEFAULT NULL,
`langue` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT 'fr',
`date_creation` varchar(30) NOT NULL DEFAULT 'YYYY-MM-DD',
`date_validite` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
`couverture_spatiale` varchar(510) NOT NULL DEFAULT 'iso-3166-1.id=FX;',
`couverture_temporelle` varchar(510) CHARACTER SET utf8 DEFAULT NULL,
`web_services` varchar(510) CHARACTER SET utf8 DEFAULT NULL COMMENT 'ontologies,meta-donnees,noms,taxons,aide,textes,images,observations,noms-vernaculaires',
PRIMARY KEY (`guid`,`langue_meta`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
