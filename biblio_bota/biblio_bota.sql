-- phpMyAdmin SQL Dump
-- version 3.4.7.1
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le : Mer 01 Février 2012 à 11:35
-- Version du serveur: 5.1.32
-- Version de PHP: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `tb_eflore`
--
-- --------------------------------------------------------
--
-- Structure de la table `biblio_meta`
--

CREATE TABLE IF NOT EXISTS `biblio_meta` (
  `guid` varchar(255) NOT NULL DEFAULT 'urn:lsid:tela-botanica.org:chorodep:#version',
  `langue_meta` varchar(2) NOT NULL DEFAULT 'fr',
  `code` varchar(20) NOT NULL,
  `version` varchar(20) NOT NULL,
  `titre` varchar(255) DEFAULT NULL,
  `description` text,
  `mots_cles` varchar(510) DEFAULT NULL,
  `citation` varchar(255) DEFAULT NULL,
  `url_tech` varchar(510) DEFAULT NULL,
  `url_projet` varchar(510) DEFAULT NULL,
  `source` text,
  `createurs` text,
  `editeur` varchar(1000) NOT NULL DEFAULT 'nom=Tela Botanica;guid=urn:lsid:tela-botanica.org;courriel=accueil@tela-botanica.org;telephone=+334 67 52 41 22;url.info=http://www.tela-botanica.org/page:association_tela_botanica;url.logo=http://www.tela-botanica.org/sites/accueil/generique/images/graphisme/logo_tela_fond_blanc.png;type=organisation;acronyme=TB;adresse=Association Tela Botanica - Institut de Botanique - 167 rue Auguste Broussonnet 34090 MONTPELLIER, FRANCE;latitude.wgs84=43.615892;longitude.wgs84=3.871736;contact.prenom=Jean-Pascal;contact.nom=MILCENT;contact.courriel=jpm@tela-botanica.org;contact.role=administrateur des données;',
  `contributeurs` text,
  `droits` text,
  `url_droits` varchar(510) DEFAULT NULL,
  `langue` varchar(255) NOT NULL DEFAULT 'fr',
  `date_creation` varchar(30) NOT NULL DEFAULT 'YYYY-MM-DD',
  `date_validite` varchar(255) DEFAULT NULL,
  `couverture_spatiale` varchar(510) NOT NULL DEFAULT 'iso-3166-1.id=FX;',
  `couverture_temporelle` varchar(510) DEFAULT NULL,
  `web_services` varchar(510) DEFAULT NULL COMMENT 'ontologies,meta-donnees,noms,taxons,aide,textes,images,observations,noms-vernaculaires',
  PRIMARY KEY (`guid`,`langue_meta`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `biblio_meta`
--

INSERT INTO `biblio_meta` (`guid`, `langue_meta`, `code`, `version`, `titre`, `description`, `mots_cles`, `citation`, `url_tech`, `url_projet`, `source`, `createurs`, `editeur`, `contributeurs`, `droits`, `url_droits`, `langue`, `date_creation`, `date_validite`, `couverture_spatiale`, `couverture_temporelle`, `web_services`) VALUES
('urn:lsid:tela-botanica.org:biblio-bota:2012', 'fr', 'biblio-bota', '2012-03', 'Bibliographie botanique.', 'Ce projet à pour objet de constituer une base de données bibliographiques de la botanique contenant les index des articles parus dans les publications botaniques et naturalistes francophones.\r\nCette base de données intégre également des informations sur les périodiques, les asssociations et les sites Web de la botanique francophone.\r\nToutes ces informations sont consultables librement sur le site Internet de Tela Botanica.\r\nVous pouvez participer à ce projet en nous aidant à collecter des données déjà existantes, mais non publiées, ou en réalisant la saisie des sommaires des revues non encore indexées.', 'bibliographie, botanique, francophone', 'Bibliographie botanique par les membres du réseau Tela Botanica..', 'http://www.tela-botanica.org/page:liste_projets?id_projet=6', 'http://www.tela-botanica.org/page:accueil_biblio', NULL, NULL, 'nom=Tela Botanica,guid=urn:lsid:tela-botanica.org,courriel=accueil@tela-botanica.org,telephone=+334 67 52 41 22,url.info=http://www.tela-botanica.org/page:association_tela_botanica,url.logo=http://www.tela-botanica.org/sites/accueil/generique/images/graphisme/logo_tela_fond_blanc.png,type=organisation,acronyme=TB,adresse=Association Tela Botanica - Institut de Botanique - 167 rue Auguste Broussonnet 34090 MONTPELLIER FRANCE,latitude.wgs84=43.615892,longitude.wgs84=3.871736,contact.prenom=Jean-Pascal,contact.nom=MILCENT,contact.courriel=jpm@tela-botanica.org,contact.role=administrateur des données;', NULL, 'licence libre Creative Commons (by-sa)', 'http://www.tela-botanica.org/page:licence?langue=fr', 'fr', '', NULL, 'iso-3166-1.id=FR;', NULL, 'meta-donnees:0.1;publications:0.1');

-- --------------------------------------------------------

--
-- Structure de la table `biblio_article`
--

CREATE TABLE IF NOT EXISTS `biblio_article` (
  `B_A_IDART` int(11) NOT NULL AUTO_INCREMENT,
  `B_A_CRAICOLL` varchar(200) NOT NULL DEFAULT '',
  `B_A_CRAISERIE` int(11) NOT NULL DEFAULT '1',
  `B_A_CRAIFASC` varchar(200) NOT NULL DEFAULT '',
  `B_A_PAGEDEBUT` int(11) DEFAULT NULL,
  `B_A_PAGEFIN` int(11) DEFAULT NULL,
  PRIMARY KEY (`B_A_IDART`,`B_A_CRAICOLL`,`B_A_CRAISERIE`,`B_A_CRAIFASC`),
  KEY `B_A_IDART_2` (`B_A_IDART`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 PACK_KEYS=0 AUTO_INCREMENT=31866 ;

-- --------------------------------------------------------

--
-- Structure de la table `biblio_article_sauv`
--

CREATE TABLE IF NOT EXISTS `biblio_article_sauv` (
  `B_A_IDART` int(11) NOT NULL AUTO_INCREMENT,
  `B_A_CRAICOLL` varchar(200) NOT NULL DEFAULT '',
  `B_A_CRAISERIE` int(11) NOT NULL DEFAULT '1',
  `B_A_CRAIFASC` varchar(200) NOT NULL DEFAULT '',
  `B_A_PAGEDEBUT` int(11) DEFAULT NULL,
  `B_A_PAGEFIN` int(11) DEFAULT NULL,
  PRIMARY KEY (`B_A_IDART`,`B_A_CRAICOLL`,`B_A_CRAISERIE`,`B_A_CRAIFASC`),
  KEY `B_A_IDART_2` (`B_A_IDART`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 PACK_KEYS=0 AUTO_INCREMENT=31838 ;

-- --------------------------------------------------------

--
-- Structure de la table `biblio_aut_saisie`
--

CREATE TABLE IF NOT EXISTS `biblio_aut_saisie` (
  `B_AS_ID` varchar(15) NOT NULL DEFAULT 'tela',
  `B_AS_LIBELLE` varchar(255) NOT NULL DEFAULT '',
  `B_AS_MAIL` varchar(255) DEFAULT NULL,
  `B_AS_REM` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`B_AS_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `biblio_collection`
--

CREATE TABLE IF NOT EXISTS `biblio_collection` (
  `B_C_CRAI` varchar(200) NOT NULL DEFAULT '0',
  `B_C_LKSTR` int(11) NOT NULL DEFAULT '0',
  `B_C_NOMCOMPLET` varchar(255) NOT NULL DEFAULT '',
  `B_C_ABREGE` varchar(255) DEFAULT NULL,
  `B_C_ISSN` varchar(9) DEFAULT NULL,
  `B_C_DATECREATION` varchar(10) DEFAULT NULL,
  `B_C_FAISSUITE` varchar(255) DEFAULT NULL,
  `B_C_DATEFIN` varchar(10) DEFAULT NULL,
  `B_C_DEVIENT` varchar(255) DEFAULT NULL,
  `B_C_PERIODICITE` varchar(255) DEFAULT NULL,
  `B_C_CONTACTNOM` varchar(255) DEFAULT NULL,
  `B_C_CONTACTMAIL` varchar(255) DEFAULT NULL,
  `B_C_IMAGE` varchar(255) DEFAULT NULL,
  `B_C_CACHER` int(1) NOT NULL DEFAULT '1',
  `B_C_COMMENT` text,
  `B_C_REM` text,
  `B_C_MAJFICHE` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`B_C_CRAI`),
  UNIQUE KEY `B_C_NOMCOMPLET` (`B_C_NOMCOMPLET`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Structure de la table `biblio_domaine`
--

CREATE TABLE IF NOT EXISTS `biblio_domaine` (
  `B_D_ID` int(2) NOT NULL AUTO_INCREMENT,
  `B_D_LABEL` varchar(50) NOT NULL DEFAULT '',
  `B_D_COMMENT` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`B_D_ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

-- --------------------------------------------------------

--
-- Structure de la table `biblio_domaine_lier`
--

CREATE TABLE IF NOT EXISTS `biblio_domaine_lier` (
  `B_DL_IDITEM` int(11) NOT NULL DEFAULT '0',
  `B_DL_IDDOM` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`B_DL_IDITEM`,`B_DL_IDDOM`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `biblio_domaine_lier_sauv`
--

CREATE TABLE IF NOT EXISTS `biblio_domaine_lier_sauv` (
  `B_DL_IDITEM` int(11) NOT NULL DEFAULT '0',
  `B_DL_IDDOM` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`B_DL_IDITEM`,`B_DL_IDDOM`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `biblio_fasc`
--

CREATE TABLE IF NOT EXISTS `biblio_fasc` (
  `B_F_CRAICOLL` varchar(200) NOT NULL DEFAULT '',
  `B_F_CRAISERIE` int(11) NOT NULL DEFAULT '0',
  `B_F_NUMERO` varchar(200) NOT NULL DEFAULT '',
  `B_F_TITRE` varchar(255) DEFAULT NULL,
  `B_F_DATE` varchar(10) DEFAULT NULL,
  `B_F_NBPAGES` int(11) DEFAULT NULL,
  `B_F_IMAGE` varchar(255) DEFAULT NULL,
  `B_F_CACHER` int(1) NOT NULL DEFAULT '1',
  `B_F_COMMENT` text,
  `B_F_REM` text,
  `B_F_MAJFICHE` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`B_F_NUMERO`,`B_F_CRAICOLL`,`B_F_CRAISERIE`),
  FULLTEXT KEY `B_F_TITRE` (`B_F_TITRE`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Structure de la table `biblio_fasc_sauv`
--

CREATE TABLE IF NOT EXISTS `biblio_fasc_sauv` (
  `B_F_CRAICOLL` varchar(200) NOT NULL DEFAULT '',
  `B_F_CRAISERIE` int(11) NOT NULL DEFAULT '0',
  `B_F_NUMERO` varchar(200) NOT NULL DEFAULT '',
  `B_F_TITRE` varchar(255) DEFAULT NULL,
  `B_F_DATE` varchar(10) DEFAULT NULL,
  `B_F_NBPAGES` int(11) DEFAULT NULL,
  `B_F_IMAGE` varchar(255) DEFAULT NULL,
  `B_F_CACHER` int(1) NOT NULL DEFAULT '1',
  `B_F_COMMENT` text,
  `B_F_REM` text,
  `B_F_MAJFICHE` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`B_F_NUMERO`,`B_F_CRAICOLL`,`B_F_CRAISERIE`),
  FULLTEXT KEY `B_F_TITRE` (`B_F_TITRE`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Structure de la table `biblio_item`
--

CREATE TABLE IF NOT EXISTS `biblio_item` (
  `B_I_IDITEM` int(11) NOT NULL AUTO_INCREMENT,
  `B_I_TITRE` varchar(255) NOT NULL DEFAULT '',
  `B_I_AUTEURS` varchar(255) NOT NULL DEFAULT '',
  `B_I_TYPLOG` int(2) NOT NULL DEFAULT '0',
  `B_I_TYPPHY` int(2) NOT NULL DEFAULT '0',
  `B_I_GEO` varchar(255) DEFAULT NULL,
  `B_I_LANGUE` varchar(255) DEFAULT NULL,
  `B_I_RESUMCLE` text,
  `B_I_IMAGE` varchar(255) DEFAULT NULL,
  `B_I_FOURDONN` varchar(255) DEFAULT NULL,
  `B_I_AUTEURSAISIE` varchar(255) DEFAULT NULL,
  `B_I_CACHER` int(1) NOT NULL DEFAULT '1',
  `B_I_COMMENT` text,
  `B_I_REM` text,
  `B_I_MAJFICHE` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`B_I_IDITEM`),
  KEY `B_I_IDITEM` (`B_I_IDITEM`),
  FULLTEXT KEY `B_I_TITRE` (`B_I_TITRE`),
  FULLTEXT KEY `B_I_AUTEURS` (`B_I_AUTEURS`),
  FULLTEXT KEY `B_I_RESUMCLE` (`B_I_RESUMCLE`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 PACK_KEYS=0 AUTO_INCREMENT=31870 ;

-- --------------------------------------------------------

--
-- Structure de la table `biblio_item_sauv`
--

CREATE TABLE IF NOT EXISTS `biblio_item_sauv` (
  `B_I_IDITEM` int(11) NOT NULL AUTO_INCREMENT,
  `B_I_TITRE` varchar(255) NOT NULL DEFAULT '',
  `B_I_AUTEURS` varchar(255) NOT NULL DEFAULT '',
  `B_I_TYPLOG` int(2) NOT NULL DEFAULT '0',
  `B_I_TYPPHY` int(2) NOT NULL DEFAULT '0',
  `B_I_GEO` varchar(255) DEFAULT NULL,
  `B_I_LANGUE` varchar(255) DEFAULT NULL,
  `B_I_RESUMCLE` text,
  `B_I_IMAGE` varchar(255) DEFAULT NULL,
  `B_I_FOURDONN` varchar(255) DEFAULT NULL,
  `B_I_AUTEURSAISIE` varchar(255) DEFAULT NULL,
  `B_I_CACHER` int(1) NOT NULL DEFAULT '1',
  `B_I_COMMENT` text,
  `B_I_REM` text,
  `B_I_MAJFICHE` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`B_I_IDITEM`),
  KEY `B_I_IDITEM` (`B_I_IDITEM`),
  FULLTEXT KEY `B_I_TITRE` (`B_I_TITRE`),
  FULLTEXT KEY `B_I_AUTEURS` (`B_I_AUTEURS`),
  FULLTEXT KEY `B_I_RESUMCLE` (`B_I_RESUMCLE`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 PACK_KEYS=0 AUTO_INCREMENT=31838 ;

-- --------------------------------------------------------

--
-- Structure de la table `biblio_item_typlog`
--

CREATE TABLE IF NOT EXISTS `biblio_item_typlog` (
  `B_IL_ID` int(2) NOT NULL AUTO_INCREMENT,
  `B_IL_LABEL` varchar(100) NOT NULL DEFAULT '',
  `B_IL_IMAGE` varchar(100) DEFAULT NULL,
  `B_IL_REM` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`B_IL_ID`),
  UNIQUE KEY `B_TM_IDMEDIA` (`B_IL_ID`),
  KEY `B_TM_IDMEDIA_2` (`B_IL_ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Structure de la table `biblio_item_typphy`
--

CREATE TABLE IF NOT EXISTS `biblio_item_typphy` (
  `B_IP_ID` int(2) NOT NULL AUTO_INCREMENT,
  `B_IP_LABEL` varchar(100) NOT NULL DEFAULT '',
  `B_IP_IMAGE` varchar(100) DEFAULT NULL,
  `B_IP_REM` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`B_IP_ID`),
  UNIQUE KEY `B_TM_IDMEDIA` (`B_IP_ID`),
  KEY `B_TM_IDMEDIA_2` (`B_IP_ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Structure de la table `biblio_link`
--

CREATE TABLE IF NOT EXISTS `biblio_link` (
  `B_L_IDLINK` int(11) NOT NULL AUTO_INCREMENT,
  `B_L_LKSTR` int(11) NOT NULL DEFAULT '0',
  `B_L_TITRE` varchar(255) NOT NULL DEFAULT '',
  `B_L_URL` varchar(255) NOT NULL DEFAULT '',
  `B_L_RESUMCLE` text,
  `B_L_PARTENAIRE` int(1) NOT NULL DEFAULT '0',
  `B_L_REFERENCE` int(1) NOT NULL DEFAULT '0',
  `B_L_IMAGE` varchar(255) DEFAULT NULL,
  `B_L_CACHER` int(1) NOT NULL DEFAULT '1',
  `B_L_COMMENT` text,
  `B_L_REM` text,
  `B_L_MAJFICHE` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`B_L_IDLINK`),
  UNIQUE KEY `B_L_IDLINK` (`B_L_IDLINK`),
  KEY `B_L_IDLINK_2` (`B_L_IDLINK`),
  FULLTEXT KEY `B_L_RESUMCLE` (`B_L_RESUMCLE`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 PACK_KEYS=0 AUTO_INCREMENT=297 ;

-- --------------------------------------------------------

--
-- Structure de la table `biblio_link_categ`
--

CREATE TABLE IF NOT EXISTS `biblio_link_categ` (
  `B_CAT_IDCAT` int(11) NOT NULL AUTO_INCREMENT,
  `B_CAT_LABEL` varchar(50) NOT NULL DEFAULT '',
  `B_CAT_IMAGE` varchar(255) DEFAULT NULL,
  `B_CAT_REM` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`B_CAT_IDCAT`),
  UNIQUE KEY `B_CAT_IDCAT` (`B_CAT_IDCAT`),
  KEY `B_CAT_IDCAT_2` (`B_CAT_IDCAT`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Structure de la table `biblio_link_categoriser`
--

CREATE TABLE IF NOT EXISTS `biblio_link_categoriser` (
  `B_LC_IDLINK` int(11) NOT NULL DEFAULT '0',
  `B_LC_IDCAT` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`B_LC_IDLINK`,`B_LC_IDCAT`),
  KEY `B_L_IDLINK` (`B_LC_IDLINK`,`B_LC_IDCAT`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `biblio_link_sauv`
--

CREATE TABLE IF NOT EXISTS `biblio_link_sauv` (
  `B_L_IDLINK` int(11) NOT NULL AUTO_INCREMENT,
  `B_L_LKSTR` int(11) NOT NULL DEFAULT '0',
  `B_L_TITRE` varchar(255) NOT NULL DEFAULT '',
  `B_L_URL` varchar(255) NOT NULL DEFAULT '',
  `B_L_RESUMCLE` text,
  `B_L_PARTENAIRE` int(1) NOT NULL DEFAULT '0',
  `B_L_REFERENCE` int(1) NOT NULL DEFAULT '0',
  `B_L_IMAGE` varchar(255) DEFAULT NULL,
  `B_L_CACHER` int(1) NOT NULL DEFAULT '1',
  `B_L_COMMENT` text,
  `B_L_REM` text,
  `B_L_MAJFICHE` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`B_L_IDLINK`),
  UNIQUE KEY `B_L_IDLINK` (`B_L_IDLINK`),
  KEY `B_L_IDLINK_2` (`B_L_IDLINK`),
  FULLTEXT KEY `B_L_RESUMCLE` (`B_L_RESUMCLE`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 PACK_KEYS=0 AUTO_INCREMENT=210 ;

-- --------------------------------------------------------

--
-- Structure de la table `biblio_media`
--

CREATE TABLE IF NOT EXISTS `biblio_media` (
  `B_M_IDMEDIA` int(11) NOT NULL DEFAULT '0',
  `B_M_LKSTR` int(11) NOT NULL DEFAULT '0',
  `B_M_ISBN` varchar(20) DEFAULT NULL,
  `B_M_COLLECTION` varchar(255) DEFAULT NULL,
  `B_M_NUMCOLL` varchar(100) DEFAULT NULL,
  `B_M_EDITEUR` varchar(255) DEFAULT NULL,
  `B_M_DATE` varchar(10) DEFAULT NULL,
  `B_M_PRIX` float(7,2) DEFAULT NULL,
  `B_M_LKMONNAIE` int(2) NOT NULL DEFAULT '0',
  `B_M_VOLUME` varchar(255) DEFAULT NULL,
  `B_M_EDITE` int(1) NOT NULL DEFAULT '1',
  `B_M_VEND` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`B_M_IDMEDIA`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Structure de la table `biblio_modif`
--

CREATE TABLE IF NOT EXISTS `biblio_modif` (
  `B_MOD_ID` int(11) NOT NULL AUTO_INCREMENT,
  `B_MOD_TABLESRC` varchar(50) NOT NULL DEFAULT '',
  `B_MOD_FICHESRC` text NOT NULL,
  `B_MOD_FICHEDEST` text,
  `B_MOD_STATUT` varchar(20) NOT NULL DEFAULT '',
  `B_MOD_MAIL` varchar(255) DEFAULT NULL,
  `B_MOD_REM` text,
  `B_MOD_MAJFICHE` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`B_MOD_ID`),
  UNIQUE KEY `B_MOD_ID` (`B_MOD_ID`),
  KEY `B_MOD_ID_2` (`B_MOD_ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

-- --------------------------------------------------------

--
-- Structure de la table `biblio_serie`
--

CREATE TABLE IF NOT EXISTS `biblio_serie` (
  `B_SER_CRAICOLL` varchar(200) NOT NULL DEFAULT '',
  `B_SER_IDSERIE` int(11) NOT NULL DEFAULT '1',
  `B_SER_SOUSTITRE` varchar(255) DEFAULT NULL,
  `B_SER_DATEDEBUT` varchar(10) DEFAULT NULL,
  `B_SER_DATEFIN` varchar(10) DEFAULT NULL,
  `B_SER_INTOBIBLIO` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`B_SER_CRAICOLL`,`B_SER_IDSERIE`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Structure de la table `biblio_spy`
--

CREATE TABLE IF NOT EXISTS `biblio_spy` (
  `B_SPY_ID` int(11) NOT NULL AUTO_INCREMENT,
  `B_SPY_DOC` varchar(255) DEFAULT NULL,
  `B_SPY_MOTEUR` varchar(25) DEFAULT NULL,
  `B_SPY_IP` varchar(255) DEFAULT NULL,
  `B_SPY_CHAINE` varchar(255) DEFAULT NULL,
  `B_SPY_GEO` varchar(255) DEFAULT NULL,
  `B_SPY_AUTEUR` varchar(255) DEFAULT NULL,
  `B_SPY_CATEG` varchar(255) DEFAULT NULL,
  `B_SPY_CATEG2` varchar(255) DEFAULT NULL,
  `B_SPY_CATEG3` varchar(255) DEFAULT NULL,
  `B_SPY_TYPQUE` varchar(255) DEFAULT NULL,
  `B_SPY_STEP` varchar(255) DEFAULT NULL,
  `B_SPY_SORT` varchar(255) DEFAULT NULL,
  `B_SPY_SINCE` varchar(255) DEFAULT NULL,
  `B_SPY_NBRES` varchar(255) DEFAULT NULL,
  `B_SPY_DATE` int(11) DEFAULT NULL,
  PRIMARY KEY (`B_SPY_ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=276259 ;

-- --------------------------------------------------------

--
-- Structure de la table `biblio_str`
--

CREATE TABLE IF NOT EXISTS `biblio_str` (
  `B_S_IDSTR` int(11) NOT NULL AUTO_INCREMENT,
  `B_S_NOM` varchar(255) NOT NULL DEFAULT '',
  `B_S_SIGLE` varchar(75) DEFAULT NULL,
  `B_S_SERVICE` varchar(255) DEFAULT NULL,
  `B_S_TYPESTR` int(11) NOT NULL DEFAULT '0',
  `B_S_DATECREATION` varchar(10) DEFAULT NULL,
  `B_S_NBRPERSO` int(11) NOT NULL DEFAULT '0',
  `B_S_VOCACTIV` varchar(255) DEFAULT NULL,
  `B_S_ADRESSE1` varchar(255) DEFAULT NULL,
  `B_S_ADRESSE2` varchar(255) DEFAULT NULL,
  `B_S_CODEPOSTAL` varchar(20) DEFAULT NULL,
  `B_S_VILLE` varchar(255) DEFAULT NULL,
  `B_S_PAYS` varchar(5) NOT NULL DEFAULT 'fr',
  `B_S_TEL` varchar(50) DEFAULT NULL,
  `B_S_FAX` varchar(50) DEFAULT NULL,
  `B_S_MAIL` varchar(255) DEFAULT NULL,
  `B_S_IMAGE` varchar(255) DEFAULT NULL,
  `B_S_CACHER` int(1) NOT NULL DEFAULT '1',
  `B_S_COMMENT` text,
  `B_S_REM` text,
  `B_S_MAJFICHE` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`B_S_IDSTR`),
  UNIQUE KEY `B_S_IDSTR` (`B_S_IDSTR`),
  KEY `B_S_IDSTR_2` (`B_S_IDSTR`),
  KEY `B_S_SIGLE` (`B_S_SIGLE`),
  KEY `B_S_VOCATION` (`B_S_VOCACTIV`),
  KEY `B_S_NOM` (`B_S_NOM`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 PACK_KEYS=0 AUTO_INCREMENT=583 ;

-- --------------------------------------------------------

--
-- Structure de la table `biblio_str_sauve`
--

CREATE TABLE IF NOT EXISTS `biblio_str_sauve` (
  `B_S_IDSTR` int(11) NOT NULL AUTO_INCREMENT,
  `B_S_NOM` varchar(255) NOT NULL DEFAULT '',
  `B_S_SIGLE` varchar(75) DEFAULT NULL,
  `B_S_SERVICE` varchar(255) DEFAULT NULL,
  `B_S_TYPESTR` int(11) NOT NULL DEFAULT '0',
  `B_S_DATECREATION` varchar(10) DEFAULT NULL,
  `B_S_NBRPERSO` int(11) NOT NULL DEFAULT '0',
  `B_S_VOCACTIV` varchar(255) DEFAULT NULL,
  `B_S_ADRESSE1` varchar(255) DEFAULT NULL,
  `B_S_ADRESSE2` varchar(255) DEFAULT NULL,
  `B_S_CODEPOSTAL` varchar(20) DEFAULT NULL,
  `B_S_VILLE` varchar(255) DEFAULT NULL,
  `B_S_PAYS` varchar(5) NOT NULL DEFAULT 'fr',
  `B_S_TEL` varchar(50) DEFAULT NULL,
  `B_S_FAX` varchar(50) DEFAULT NULL,
  `B_S_MAIL` varchar(255) DEFAULT NULL,
  `B_S_IMAGE` varchar(255) DEFAULT NULL,
  `B_S_CACHER` int(1) NOT NULL DEFAULT '1',
  `B_S_COMMENT` text,
  `B_S_REM` text,
  `B_S_MAJFICHE` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`B_S_IDSTR`),
  UNIQUE KEY `B_S_IDSTR` (`B_S_IDSTR`),
  KEY `B_S_IDSTR_2` (`B_S_IDSTR`),
  KEY `B_S_SIGLE` (`B_S_SIGLE`),
  KEY `B_S_VOCATION` (`B_S_VOCACTIV`),
  KEY `B_S_NOM` (`B_S_NOM`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 PACK_KEYS=0 AUTO_INCREMENT=412 ;

-- --------------------------------------------------------

--
-- Structure de la table `biblio_str_type`
--

CREATE TABLE IF NOT EXISTS `biblio_str_type` (
  `B_TYPSTR_ID` int(11) NOT NULL AUTO_INCREMENT,
  `B_TYPSTR_LABEL` varchar(255) NOT NULL DEFAULT '',
  `B_TYPSTR_REM` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`B_TYPSTR_ID`),
  UNIQUE KEY `B_TYP_ID` (`B_TYPSTR_ID`),
  KEY `B_TYP_ID_2` (`B_TYPSTR_ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
