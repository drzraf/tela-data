-- phpMyAdmin SQL Dump
-- version 3.5.5
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Lun 11 Février 2013 à 11:57
-- Version du serveur: 5.1.61-log
-- Version de PHP: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `tb_eflore`
--

--
-- Contenu de la table `baseveg_meta`
--

INSERT INTO `baseveg_meta` (`guid`, `langue_meta`, `code`, `version`, `titre`, `description`, `mots_cles`, `citation`, `url_tech`, `url_projet`, `source`, `createurs`, `editeur`, `contributeurs`, `droits`, `url_droits`, `langue`, `date_creation`, `date_validite`, `couverture_spatiale`, `couverture_temporelle`, `web_services`) VALUES
('urn:lsid:tela-botanica.org:baseveg:2013_08_05', 'fr', 'baseveg', '2013_08_05', 'baseveg', 'Répertoire synonymique des groupements végétaux de France. [Extrait du programme Catminat, programme personnel de Ph. Julve]. ', 'phytosociologie, syntaxons, catminat, végétation ', 'Julve, Ph., 2013 ff. - Baseveg. Répertoire synonymique des groupements végétaux de France. Version : 05 aout 2013. http://perso.wanadoo.fr/philippe.julve/catminat.htm', 'http://philippe.julve.pagesperso-orange.fr/catminat.htm##REPERTOIRE', 'http://www.tela-botanica.org/page:liste_projets?id_projet=18&act=documents&id_repertoire=98', NULL, 'p.nom=JULVE,p.prenom=Philippe,p.courriel=philippe.julve@wanadoo.fr', 'nom=Tela Botanica,guid=urn:lsid:tela-botanica.org,courriel=accueil@tela-botanica.org,telephone=+334 67 52 41 22,url.info=http://www.tela-botanica.org/page:association_tela_botanica,url.logo=http://www.tela-botanica.org/sites/accueil/generique/images/graphisme/logo_tela_fond_blanc.png,type=organisation,acronyme=TB,adresse=Association Tela Botanica - Institut de Botanique - 167 rue Auguste Broussonnet 34090 MONTPELLIER FRANCE,latitude.wgs84=43.615892,longitude.wgs84=3.871736,contact.prenom=Jean-Pascal,contact.nom=MILCENT,contact.courriel=jpm@tela-botanica.org,contact.role=administrateur des données;', NULL, NULL, NULL, 'fr', '2013-08-05', NULL, 'iso-3166-1.id=FR;', NULL, 'syntaxons;metadonnees;ontologies;');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
