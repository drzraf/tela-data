-- phpMyAdmin SQL Dump
-- version 3.5.5
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Lun 11 Février 2013 à 11:52
-- Version du serveur: 5.1.61-log
-- Version de PHP: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `tb_eflore`
--

-- --------------------------------------------------------

--
-- Structure de la table `baseveg_meta`
--

CREATE TABLE IF NOT EXISTS `baseveg_meta` (
  `guid` varchar(255) NOT NULL DEFAULT 'urn:lsid:tela-botanica.org:baseveg:v2013_08_05',
  `langue_meta` varchar(2) NOT NULL DEFAULT 'fr',
  `code` varchar(20) NOT NULL,
  `version` varchar(20) NOT NULL,
  `titre` varchar(255) DEFAULT NULL,
  `description` text,
  `mots_cles` varchar(510) DEFAULT NULL,
  `citation` varchar(255) DEFAULT NULL,
  `url_tech` varchar(510) DEFAULT NULL,
  `url_projet` varchar(510) DEFAULT NULL,
  `source` text,
  `createurs` text,
  `editeur` varchar(1000) NOT NULL DEFAULT 'nom=Tela Botanica,guid=urn:lsid:tela-botanica.org,courriel=accueil@tela-botanica.org,telephone=+334 67 52 41 22,url.info=http://www.tela-botanica.org/page:association_tela_botanica,url.logo=http://www.tela-botanica.org/sites/accueil/generique/images/graphisme/logo_tela_fond_blanc.png,type=organisation,acronyme=TB,adresse=Association Tela Botanica - Institut de Botanique - 167 rue Auguste Broussonnet 34090 MONTPELLIER FRANCE,latitude.wgs84=43.615892,longitude.wgs84=3.871736,contact.prenom=Jean-Pascal,contact.nom=MILCENT,contact.courriel=jpm@tela-botanica.org,contact.role=administrateur des données;',
  `contributeurs` text,
  `droits` text,
  `url_droits` varchar(510) DEFAULT NULL,
  `langue` varchar(255) NOT NULL DEFAULT 'fr',
  `date_creation` varchar(30) NOT NULL DEFAULT 'YYYY-MM-DD',
  `date_validite` varchar(255) DEFAULT NULL,
  `couverture_spatiale` varchar(510) NOT NULL DEFAULT 'iso-3166-1.id=FX;',
  `couverture_temporelle` varchar(510) DEFAULT NULL,
  `web_services` varchar(510) DEFAULT NULL COMMENT 'ontologies,meta-donnees,noms,taxons,aide,textes,images,observations,noms-vernaculaires',
  PRIMARY KEY (`guid`,`langue_meta`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
